import http from './base/http-base';

const getElasticsSearchAPI = async () => {
	const { data } = await http.get('/elasticsSearch');
	return data;
};

export default getElasticsSearchAPI;
