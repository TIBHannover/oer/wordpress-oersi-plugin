import axios from 'axios';

const http = axios.create({
	baseURL:
		import.meta.env.MODE === 'production'
			? window?.OERSI?.apiUrl
			: import.meta.env.OERSI_API_URL,
	headers: {
		'Content-Type': 'application/json',
		// 'Access-Control-Allow-Origin': 'localhost:3000',
		// 'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, OPTIONS',
		// origin: 'localhost:3000',
	},
});

export default http;
