/**
 *
 * @package OERSI
 * @license: MIT
 *
 * @author Edmond Kacaj <Edmond.Kacaj@tib.eu>
 *
 * Copyright (c) 2022 TIB <https://www.tib.eu/en>
 */

import HttpApi from 'i18next-http-backend';
import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import Backend from 'i18next-chained-backend';
import LocalStorageBackend from 'i18next-localstorage-backend';
i18n.use(Backend)
	.use(initReactI18next)
	.init({
		fallbackLng: window?.OERSI?.fallbackLng || ['de'],
		debug: window?.OERSI?.debug || false,
		lang: window?.OERSI?.language || 'de',
		// supportedLngs: window?.OERSI?.supportedLngs || ['de'],
		nsSeparator: '.',
		defaultNS: [
			'cards',
			'translation',
			'languages',
			'labels',
			//'collections',
		],
		namespaces: [
			'cards',
			'translation',
			'languages',
			'labels',
		],
		fallbackNS: [
			'cards',
			'translation',
			'languages',
			'labels',
		],
		backend: {
			backends: [LocalStorageBackend, HttpApi],
			backendOptions: [
				{
					// expirationTime: window?.OERSI?.cacheExpirationTime || 1000 * 60 * 60 * 24, // 24 hours
					// expirationTime: 10 seconds,
					expirationTime: 1000 * 10,
				},
				{
					// for all available options read the backend's repository readme file
					loadPath: (lng, namespaces) => {
						return `${window?.OERSI?.apiUrl ||
							import.meta.env.OERSI_API_URL
							}/locales/{{ns}}?lang={{lng}}`;
					},
				},
			],
		},
		interpolation: {
			escapeValue: false,
		},
		react: {
			bindI18n: 'languageChanged',
			bindI18nStore: '',
			transEmptyNodeValue: '',
			transSupportBasicHtmlNodes: true,
			transKeepBasicHtmlNodesFor: ['br', 'strong', 'i'],
			useSuspense: true,
		},
	});
export default i18n;
