import { useTheme } from '@mui/material';
import GlobalStyles from '@mui/material/GlobalStyles';
import { ReactiveBase } from '@appbaseio/reactivesearch';
import PropTypes from 'prop-types';
import Layout from '@components/Layout';
import NotFound from '@components/NotFound';
const App = ({ data }) => {
	const theme = useTheme();
	return (
		data &&
		(data?.url && data?.app_name ? (
			<ReactiveBase
				app={data.app_name}
				url={data.url}
				className={theme.classPrefix + 'app-reactive-base'}
			>
				<GlobalStyles />
				<Layout data={data} />
			</ReactiveBase>
		) : (
			<NotFound error={"Please add the URL and app_name, in admin page/ElasticsSearch"} theme={theme} />
		))
	);
};

App.propTypes = {
	data: PropTypes.object.isRequired,
};

App.defaultProps = {};

export default App;
