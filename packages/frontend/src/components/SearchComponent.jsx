/**
 *
 * @package OERSI
 * @license: MIT
 *
 * @author Edmond Kacaj <Edmond.Kacaj@tib.eu>
 *
 * Copyright (c) 2022 TIB <https://www.tib.eu/en>
 */

import { DataSearch } from '@appbaseio/reactivesearch';
import {
	Alert,
	Avatar,
	Box,
	Button,
	List,
	ListItem,
	ListItemButton,
	ListItemText,
	ListItemAvatar,
	Typography,
	useTheme,
} from '@mui/material';
import React, { useState } from 'react';
import { css } from '@emotion/core';
import SearchIcon from '@mui/icons-material/Search';
import ClearIcon from '@mui/icons-material/Clear';
import PropTypes from 'prop-types';
import { useTranslation } from 'react-i18next';
import Sanitize from './Sanitize';
import { checkProviderForMaterial } from "@helpers";

const SearchComponent = ({ data, providerName = undefined, providerImageLink = undefined }) => {
	const { components, classPrefix, breakpoints, palette } = useTheme();
	const { t } = useTranslation();
	const backendURL =
		window?.OERSI?.assetURL || import.meta.env.OERSI_ASSET_URL;
	const shortDescription =
		window?.OERSI?.configuration?.searchTextLength || 200;
	const [mediaQuery, setMediaQuery] = React.useState(window.innerWidth);
	const stylesOverride = css`
		& .${classPrefix}search-component-input-title {
			${{ ...components.searchComponent?.dataSearchTitle }}
		}
		& .${classPrefix}search-component-input-group {
			${{ ...components.searchComponent?.dataSearchInputGroup }}
		}
		& .input-addon {
			${{ ...components.searchComponent?.dataSearchInputAddon }}
		}
		& .input-group {
			${{ ...components.searchComponent?.searchInputGroup }}
		}
	`;

	// state for search
	const [searchText, setSearchText] = useState('');
	const searchInputRef = React.useRef(null);

	// handler for text input
	const handleSearchInput = (value) => {
		setSearchText(value);
	};

	// handler for enter key
	const handleKeyPress = (event) => {
		if (event.key === 'Enter' && !data?.executeSearch) {  // only execute if executeSearch false
			event.preventDefault();
			if (searchInputRef.current) {
				searchInputRef.current.triggerQuery();
			}
		}
	};

	React.useEffect(() => {
		const handleResize = () => {
			setMediaQuery(window.innerWidth);
		};
		window.addEventListener('resize', handleResize);
		return () => {
			window.removeEventListener('resize', handleResize);
		};
	}, []);
	return (
		data && (
			<Box
				className={classPrefix + 'search-component-container'}
				sx={{
					width: '100%',
					maxWidth: '1000px',
					margin: {
						xs: '0 auto',
						md: '0 auto 3em'
					},
				}}
			>
				<DataSearch
					ref={searchInputRef}
					css={stylesOverride}
					className={classPrefix + 'search-component-data-search'}
					// title='search'
					componentId={data?.componentId}
					dataField={data?.dataField}
					placeholder={
						data?.placeholder.indexOf('translation.') > -1
							? t(data?.placeholder)
							: data?.placeholder
					}
					autosuggest={data?.autosuggest}
					highlight={data?.highlight}
					fieldWeights={data?.fieldWeights}
					highlightField={data?.highlightField}
					queryFormat={data?.queryFormat}
					fuzziness={data?.fuzziness}
					size={data?.size}
					showIcon={data?.showIcon}
					iconPosition={data?.iconPosition}
					showFilter={data?.showFilter}
					debounce={data?.debounce}
					filterLabel={data?.filterLabel}
					enablePopularSuggestions={data?.enablePopularSuggestions}
					expandSuggestionsContainer={
						data?.expandSuggestionsContainer
					}
					customHighlight={(props) => ({
						highlight: data?.customHighlight
					})}
					showClear={data?.showClear}  // clear x-icon
					searchAsYouType={data?.executeSearch}  // executeSearch if true
					value={data?.executeSearch ? undefined : searchText}  // Controlled input if executeSearch false
					onChange={handleSearchInput}
					onValueSelected={(value, cause) => {
						if (data?.executeSearch) {
							return value;  // executeSearch true
						}
						// executeSearch false use button
						return (cause === 'ENTER_PRESS' || cause === 'SUGGESTION_SELECT') ? value : undefined;
					}}
					icon={
						data?.showButton ? (
							// Text-Button Variante
							<div className={classPrefix + 'search-component-input-icon'} style={{ height: '100%' }}>
								<Button
									sx={{
										...components.searchComponent?.dataSearchInputIconButton,
										[breakpoints.down(300)]: { fontSize: '1em' },
									}}
									variant='dataSearch'
									className={classPrefix + 'search-component-custom-icon'}
									aria-label={t('translation.SEARCH_BUTTON_TEXT')}
								>
									{mediaQuery > 300 ? (
										data?.translateText?.indexOf('translation.') > -1
											? t(data?.translateText)
											: data?.translateText
									) : (
										<SearchIcon
											style={{
												color: palette.grey[700],
												fontSize: '7.5em',
											}}
										/>
									)}
								</Button>
							</div>
						) : (
							// Seach-Icon Button
							<div className={classPrefix + 'search-component-input-icon search-icon'}>
								<Button
									sx={{
										minWidth: 'auto',
										padding: '8px',
										borderRadius: '50%',
										'&:hover': {
											backgroundColor: 'rgba(0, 0, 0, 0.04)'
										}
									}}
									aria-label={t('translation.SEARCH_BUTTON_TEXT')} // Search-Button Translation Accessibility
								>
									<SearchIcon className={'search-icon-svg'} />
								</Button>
							</div>
						)
					}
					clearIcon={
						// Clear-Icon Button
						<div className={'clear-icon'}>
							<Button
								sx={{
									minWidth: 'auto',
									padding: '8px',
									borderRadius: '50%',
									'&:hover': {
										backgroundColor: 'rgba(0, 0, 0, 0.04)'
									}
								}}
								aria-label={t('translation.CLEAR_SEARCH')}  // Clear-Button Translation Accessibility
							>
								<ClearIcon className={'clear-icon-svg'} />
							</Button>
						</div>
					}
					URLParams={data?.URLParams}
					innerClass={{
						input: classPrefix + 'search-component-input-group',
						title: classPrefix + 'search-component-input-title',
						list: classPrefix + 'search-component-input-list',
						'recent-search-icon':
							classPrefix +
							'search-component-input-recent-search-icon',
						'popular-search-icon':
							classPrefix +
							'search-component-input-popular-search-icon',
					}}
					render={({
						loading,
						error,
						data,
						value,
						downshiftProps: {
							isOpen,
							getItemProps,
							highlightedIndex
						},
					}) => {
						if (loading) {
							return (
								<Alert severity='info'> {'Loading...'} </Alert>
							);
						}
						if (error) {
							return (
								<div>
									<Alert severity='error'>
										{'Something went wrong'}
									</Alert>
								</div>
							);
						}
						return isOpen && Boolean(value.length) ? (
							<Box
								sx={{
									width: 'calc(100% - 40px)',
									height: '400px',
									overflowY: 'auto',
									position: 'absolute',
									backgroundColor: 'white',
									border: '0px',
									borderBottom: '10px solid #cccccc5e',
									borderColor: palette.grey[300],
									left: '20px',
									zIndex: '1',
									boxShadow: '0 30px 30px 20px rgba(0,0,0,0.3)',
									borderRadius: '0 0 10px 10px',
								}}
								className={classPrefix + 'search-component-box'}
							>
								{data.map((suggestion, index) => (
									<List
										key={index}
										{...getItemProps({
											item: suggestion,
											key: suggestion.source.id,
											style: {
												backgroundColor:
													highlightedIndex === index
														? palette.grey[100]
														: 'white',
											},
										})}
										sx={{
											...components.searchComponent
												?.dataSearchListItem,
										}}
										className={
											classPrefix +
											'search-component-list-item'
										}
									>
										<ListItem>
											<ListItemButton
												alignItems='flex-start'
												sx={{
													width: '100%',
													maxWidth: '100%',
													'&:hover': {
														backgroundColor: 'transparent'
													}
												}}
												className={
													classPrefix +
													'search-component-list-item-button'
												}
											>
												<ListItemAvatar
													className={
														classPrefix +
														'search-component-list-item-avatar'
													}
													sx={{
														minWidth: '64px',
														display: 'flex',
														alignSelf: 'stretch',
														alignItems: 'center',
														justifyContent: 'center',
														border: '1px solid #e7e7e7',
														margin: '0 1em 0 0'
													}}
												>
													<Avatar
														className={
															classPrefix +
															'search-component-list-item-avatar-image'
														}
														sx={{
															width: 24,
															height: 24,
														}}
														alt={
															suggestion?.source
																?.mainEntityOfPage?.[0]
																.provider.name
														}
														src={providerName && checkProviderForMaterial(suggestion?.source, providerName)?.found
															? (providerImageLink || (backendURL + '/images/oer-oersi-logo.svg'))
															: backendURL + '/images/oer-oersi-logo.svg'
														}
													/>
												</ListItemAvatar>
												<ListItemText
													primary={
														<Typography
															sx={{
																display: 'inline',
																fontSize: '0.85em',
															}}
															component='span'
															variant='body2'
															color='#141414'
															className={
																classPrefix +
																'search-component-list-item-text-primary'
															}
														>
															<b>
																<Sanitize>
																	{
																		suggestion
																			?.source
																			?.highlight
																			?.name ?
																			suggestion
																				?.source
																				?.highlight
																				?.name
																			:
																			suggestion
																				?.source
																				?.name
																	}
																</Sanitize>
															</b>
														</Typography>
													}
													secondary={
														<React.Fragment>
															<Typography
																sx={{
																	display: 'inline',
																	fontSize: '0.85em',
																}}
																component='span'
																variant='body2'
																color='#141414'
																className={
																	classPrefix +
																	'search-component-list-item-text-secondary'
																}
															>
																<b>
																	{
																		suggestion
																			?.source
																			?.mainEntityOfPage?.[0]
																			?.provider
																			?.name
																	}
																</b>

																{suggestion?.source
																	?.description
																	? ' - '
																	: ''}
															</Typography>
															<Typography
																sx={{
																	fontSize: '0.85em',
																	color: '#141414',
																}}>
																<Sanitize>
																	{
																		suggestion?.source?.highlight?.description
																			? suggestion?.source?.highlight?.description?.[0]?.oersiSubStr(
																				shortDescription,
																			)
																			: suggestion?.source?.description?.oersiSubStr(
																				shortDescription,
																			)
																	}
																</Sanitize>
															</Typography>
														</React.Fragment>
													}
												/>
											</ListItemButton>
										</ListItem>
									</List>
								))}
							</Box>
						) : null;
					}}
					executeSearch={data?.executeSearch}
					onKeyPress={!data?.executeSearch ? handleKeyPress : undefined}  // only if executeSearch false
				></DataSearch>
			</Box>
		)
	);
};

SearchComponent.propTypes = {
	data: PropTypes.object.isRequired,
	providerName: PropTypes.string,
	providerImageLink: PropTypes.string
};

SearchComponent.defaultProps = {};

export default SearchComponent;
