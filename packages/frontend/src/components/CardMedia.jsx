/**
 *
 * @package OERSI
 * @license: MIT
 *
 * @author Edmond Kacaj <Edmond.Kacaj@tib.eu>
 *
 * Copyright (c) 2022 TIB <https://www.tib.eu/en>
 */

import { Link, CardMedia as CardMediaMUI, Box } from '@mui/material';
import React, { useState } from 'react';
import { getSafeUrl, getThumbnailUrl } from '@helpers';
import { LazyLoadComponent } from 'react-lazy-load-image-component';
import PropTypes from 'prop-types';
import DatasetLinkedOutlinedIcon from '@mui/icons-material/DatasetLinkedOutlined';
import { alpha } from '@mui/system';

const CardMedia = ({
	data,
	isImageLinked = false,
	classPrefix,
	thumbnailUrl = getThumbnailUrl(data?._id),
	theme,
}) => {
	const [hasError, setHasError] = useState(false);
	const imageUrl = thumbnailUrl || data?.image;

	const imageStyles = {
		height: '38%',
		width: '100%',
		objectFit: 'cover',
		[theme.breakpoints.down('sm')]: {
			height: '40%',
		},
		display: hasError ? 'none' : 'block',
	};

	const handleError = (e) => {
		console.error('Error loading image: ', e);
		setHasError(true);
	};

	const renderImageOrIcon = () => {
		if (hasError) {
			return (
				<Box
					sx={{
						height: '38%',
						width: '100%',
						display: 'flex',
						justifyContent: 'center',
						alignItems: 'center',
						border: '3px solid rgba(191,191,191,0.1)',
						borderBottomWidth: '0px',
						borderRadius: '5px 5px 0 0',
						backgroundColor: theme.palette.grey[200], // Optional: Hintergrundfarbe für das Icon
						transition: 'all 0.6s',
						[theme.breakpoints.down('sm')]: {
							height: '40%',
						},
						'&:hover': {
							backgroundColor: alpha(theme.palette.grey[200], 0.9),
						},
					}}
					title={data?.name}
					role="img"
					aria-label={`Failed to load image: ${data?.name}`}
				>
					<DatasetLinkedOutlinedIcon
						sx={{
							width: '110px',
							borderRadius: '50%',
							height: '110px',
							background: alpha(theme.palette.grey[300], 0.7),
							color: theme.palette.grey[100],
							padding: '1em',
							transition: 'all 0.6s',
							[theme.breakpoints.down('sm')]: {
								width: '80px',
								height: '80px',
							},
						}} />
				</Box>
			);
		}
		return (
			<CardMediaMUI
				sx={imageStyles}
				className={`${classPrefix}card-media-image`}
				component='img'
				title={data?.name}
				image={imageUrl}
				alt={data?.name}
				loading='lazy'
				onError={handleError}
			/>
		);
	};

	return (
		<LazyLoadComponent
			delayTime={100}
			className={theme.classPrefix + 'card-media-lazy-load-component'}
		>
			{isImageLinked ? (
				<Link
					href={getSafeUrl(data?.id)}
					className={theme.classPrefix + 'card-media-link-image'}
				>
					{renderImageOrIcon()}
				</Link>
			) : (
				renderImageOrIcon()
			)}
		</LazyLoadComponent>
	);
};

CardMedia.propTypes = {
	data: PropTypes.any,
	isImageLinked: PropTypes.bool,
	classPrefix: PropTypes.string,
	backendURL: PropTypes.string,
	theme: PropTypes.object.isRequired,
};

export default CardMedia;
