/**
 *
 * @package OERSI
 * @license: MIT
 *
 * @author Edmond Kacaj <Edmond.Kacaj@tib.eu>
 *
 * Copyright (c) 2022 TIB <https://www.tib.eu/en>
 */

import React from 'react';
import { CardContent as MuiCardContent, Grid, Divider, Typography } from '@mui/material';
import PropTypes from 'prop-types';
import { useSpring, useElementScroll, motion } from 'framer-motion';
import Content from '@components/Content';
import { joinArrayField } from '@helpers';
import Sanitize from './Sanitize';

const CardContent = ({ data, t, theme }) => {
	const ref = React.useRef(null);
	const { scrollYProgress } = useElementScroll(ref);
	const scaleX = useSpring(scrollYProgress, {
		stiffness: 100,
		damping: 30,
		restDelta: 0.001,
		duration: 0.5, // seconds to complete the motion
	});

	return (
		<MuiCardContent
			className={theme.classPrefix + 'card-content'}
			sx={{
				height: '401px',
				width: '100%',
				transition: 'all 0.5s ease',
				'&:hover': {
					transform: 'translateY(-222px)',
				},
				[theme.breakpoints.down('sm')]: {
					'&:hover': {
						transform: 'translateY(-170px)',
					},
				},
			}}
		>
			<Grid
				container
				spacing={1}
				className={theme.classPrefix + 'card-content-container'}
				sx={{
					marginTop: '0px',
					marginLeft: '0px',
					width: '100%',
				}}
			>
				<Grid
					item
					xs={12}
					className={
						theme.classPrefix + 'card-content-item-grid-first'
					}
					sx={{
						width: '100%',
						position: 'sticky',
						top: '0px',
						zIndex: '1',
						backgroundColor: 'transparent',
						'&.MuiGrid-item': { paddingTop: '0' },
					}}
				>
					{window?.OERSI?.configuration?.showProgressBar &&
						window?.OERSI?.configuration?.showProgressBar ===
						true && (
							<motion.div
								className={
									'progress-bar ' +
									theme.classPrefix +
									'card-content-motion-progress-bar'
								}
								style={Object.assign(
									{
										position: 'absolute',
										top: '0px',
										left: '-3px',
										width: '101%',
										height: '5px',
										backgroundColor:
											theme.palette.twilloColor.teal,
										borderRadius: '0px',
										transformOrigin: 'left',
									},
									{ scaleX },
								)}
							/>
						)}
				</Grid>
				<Grid
					item
					xs={12}
					className={
						theme.classPrefix + 'card-content-item-grid-second'
					}
					sx={{
						height: '400px',
						overflowY: 'scroll',
						'&.MuiGrid-item': { padding: '6% 7%' },
						[theme.breakpoints.down('sm')]: {
							height: '240px',
						}
					}}
					ref={ref}
				>
					<Grid
						sx={{
							maxHeight: '90px',
							mb: '16px',
						}}>
						{data?.creator && (
							<>
								{/*
							 <Content
								data={joinArrayField(
									data.creator,
									item => item.name,
									null,
								)}
								translation={t('cards.AUTHOR')}
								format={'html'}
								theme={theme}
							/> */}
								<Typography
									theme={theme}
									sx={{
										fontSize: '0.85em',
										fontStyle: 'italic',
										fontWeight: '400',
										display: '-webkit-box',
										'-webkit-box-orient': 'vertical',
										'-webkit-line-clamp': '1',
										overflow: 'hidden',
										textOverflow: 'ellipsis',
										marginBottom: '7px',
									}}
								>
									<Sanitize>
										{joinArrayField(
											data.creator,
											item => item.name,
											null
										)}
									</Sanitize>
								</Typography>
							</>
						)}
						{data?.name && (
							<>
								{/* <Content
								data={data?.name}
								translation={t('cards.NAME')}
								format={'html'}
								theme={theme}
							/> */}
								<Typography
									variant='h3'
									theme={theme}
								>
									<Sanitize>
										{data?.name}
									</Sanitize>
								</Typography>
							</>
						)}
					</Grid>
					{data?.description && (
						<>
							{/*
							<Content
								data={data?.description}
								translation={t('cards.DESCRIPTION')}
								format={'html'}
								theme={theme}
							/> */}
							<Typography
								theme={theme}
								sx={{
									fontSize: '0.9em',
									hyphens: 'auto'
								}}
							>
								<Sanitize>
									{data?.description}
								</Sanitize>
							</Typography>
						</>
					)}

					{data?.about &&
						data?.about?.length > 0 && (
							<>
								<Divider
									style={{
										margin: '16px 0'
									}}
								/>
								{/* <Content
									data={joinArrayField(
										data?.about,
										item => item.id,
										label => t('labels.' + label),
									)}
									translation={t('cards.SUBJECT')}
									format={'text'}
									theme={theme}
								/> */}
								<Typography
									theme={theme}
									sx={{
										variant: 'h4',
										fontSize: '1em',
										fontWeight: '600'
									}}
								>
									{t('cards.SUBJECT')}
								</Typography>
								<Typography
									theme={theme}
									sx={{
										fontSize: '0.9em',
									}}
								>
									<Sanitize>
										{joinArrayField(
											data?.about,
											item => item.id,
											label => t('labels.' + label),
										)}
									</Sanitize>
								</Typography>
							</>
						)}
					{/* {data?.learningResourceType && (
							<Content
								data={joinArrayField(
									data?.learningResourceType,
									item => item.id,
									label => t('labels.' + label),
								)}
								translation={t('cards.MATERIAL_TYPE')}
							/>
						)} */}
					{/* {data?.license?.id && (
							<Content
								data={
									<License
										license={data?.license}
										sx={{
											color: theme.palette.grey[600],
											backgroundColor: theme.palette.grey[300],
											borderColor: theme.palette.grey[300],
											borderRadius: '10px',
											padding: '5px',
										}}
									/>
								}
								transition={t('cards.LICENSE')}
								isReactElement={true}
							/>
						)} */}
					{/* {data?.dateCreated && (
						<>
							<Divider
								style={{
									margin: '16px 0'
								}}
							/>
							<Content
								data={data?.dateCreated}
								translation={t('cards.DATE_PUBLISHED')}
								format={'date'}
								theme={theme}
							/>
						</>
					)} */}
					{data?.keywords?.length > 0 && (
						<>
							<Divider
								style={{
									margin: '16px 0'
								}}
							/>
							<Typography
								theme={theme}
								sx={{
									variant: 'h4',
									fontSize: '1em',
									fontWeight: '600'
								}}
							>{t('cards.KEYWORDS')}</Typography>
							<Content
								data={data?.keywords}
								//translation={t('cards.KEYWORDS')}
								format={'array'}
								theme={theme}
								sx={{
									p: 1,
									fontSize: '0.8em',
								}}
							/>
						</>
					)}
				</Grid>
			</Grid>
		</MuiCardContent>
	);
};


CardContent.propTypes = {
	data: PropTypes.any,
	providerName: PropTypes.string,
	providerImageLink: PropTypes.string
};

export default CardContent;
