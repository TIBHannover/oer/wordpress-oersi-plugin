import React from 'react';
import PropTypes from 'prop-types';
import { FixedSizeList } from 'react-window';
import {
	Box,
	Checkbox,
	Chip,
	FormControlLabel,
	Grid,
	useTheme,
} from '@mui/material';

const MultiListRender = ({ data, value, onSelectionChange }) => {
	const theme = useTheme();
	const ITEM_SIZE = 35;
	const itemCount = data ? data.length : 0;
	const listHeight = Math.min(350, itemCount * ITEM_SIZE);
	return (
		<Grid
			item
			xs={12}
			sx={{
				position: 'relative',
				overflowX: 'hidden',
			}}>
			<FixedSizeList
				height={listHeight}
				itemSize={ITEM_SIZE}
				itemCount={itemCount}
				className={
					theme.classPrefix + 'multi-list-render-fixed-size-list'
				}
			>
				{({ index, style }) => (
					<FormControlLabel
						control={
							<Checkbox
								className={
									theme.classPrefix +
									'multi-list-render-checkbox'
								}
								key={index}
								checked={data[index].key in value}
								onChange={onSelectionChange}
								value={data[index].key}
								style={{ height: ITEM_SIZE + 'px' }}
								sx={{
									'& .MuiSvgIcon-root': {
										fontSize: '1.5em',
									},
								}}
							/>
						}
						label={onItemRender(
							data[index].label,
							data[index].doc_count,
							theme,
						)}
						className={
							'full-width ' +
							theme.classPrefix +
							'multi-list-render-checkbox-full-width'
						}
						sx={{
							width: '100%',
							padding: '0 5%',
							overflow: 'hidden',
							margin: 0,
							transition: 'all .3s',
							'&:hover': {
								backgroundColor: theme.palette.grey[100],
							},
						}}
						style={
							delete style.width && style
						}
						classes={{
							label:
								theme.classPrefix +
								'filter-item-label full-width',
						}}
					/>
				)}
			</FixedSizeList>
		</Grid>
	);
};

/**
 *
 * @param {String} label  - label of the item to be rendered
 * @param {String} count - count of the item to be rendered (if any)
 * @param {Object} theme - theme object to be used for rendering
 * @returns  {React.Component} - returns a component with the label and count
 */
const onItemRender = (label, count, theme) => {
	return (
		<Box className={theme.classPrefix + 'filter-item-label'}
			sx={{
				width: '100% !important',
				overflowX: 'hidden'
			}}
		>
			<Grid
				container
				spacing={1}
				className={theme.classPrefix + 'filter-item-label-grid'}
			>
				<Grid
					item
					xs={8}
					md={9}
					className={
						theme.classPrefix + 'filter-item-label-grid-item'
					}
				>
					<Box
						className={theme.classPrefix + 'filter-item-label-text'}
						component='div'
						sx={{
							textOverflow: 'ellipsis',
							overflow: 'hidden',
							whiteSpace: 'nowrap',
							ml: '3px'
						}}
					>
						{label}
					</Box>
				</Grid>
				<Grid
					item
					xs={4}
					md={3}
					className={
						theme.classPrefix + 'filter-item-label-grid-item-count'
					}
					sx={{
						position: 'absolute',
						right: '3%',
						pt: '6px !important',
					}}
				>
					<Chip
						className={
							theme.classPrefix + 'filter-item-label-count-chip'
						}
						label={count}
						size='small'
						sx={{
							fontSize: '0.85em',
							backgroundColor: '#a5dce5'
						}}
					/>
				</Grid>
			</Grid>
		</Box>
	);
};

MultiListRender.propTypes = {
	data: PropTypes.array.isRequired,
	isHierarchicalFilter: PropTypes.bool.isRequired,
	onSelectionChange: PropTypes.func.isRequired,
	value: PropTypes.object.isRequired,
};

MultiListRender.defaultProps = {};

export default MultiListRender;
export { onItemRender };
