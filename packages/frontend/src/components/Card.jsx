/**
 *
 * @package OERSI
 * @license: MIT
 *
 * @author Edmond Kacaj <Edmond.Kacaj@tib.eu>
 *
 * Copyright (c) 2022 TIB <https://www.tib.eu/en>
 */

import React from 'react';
import PropTypes from 'prop-types';
import { Card as MuiCard, useTheme } from '@mui/material';
import { useTranslation } from 'react-i18next';
import { areEqual, getSafeUrl, joinArrayField, checkProviderForMaterial } from '@helpers';
import CardMedia from '@components/CardMedia';
import CardHeader from '@components/CardHeader';
import CardAction from '@components/CardAction';
import CardContent from '@components/CardContent';

const Card = ({ data, providerName = undefined, providerImageLink = undefined }) => {
	const theme = useTheme();
	const { t } = useTranslation(['cards']);
	const backendURL = window?.OERSI?.assetURL || import.meta.env.OERSI_ASSET_URL;
	const canLinkImage = window?.OERSI?.canLinkImage || true;

	return (
		data && (
			<MuiCard
				className={theme.classPrefix + 'card-root'}
				role="region"
				aria-labelledby={`card-media-title-${data?.id}`}
			>
				<CardMedia
					data={data}
					isImageLinked={canLinkImage}
					classPrefix={theme.classPrefix}
					backendURL={backendURL}
					theme={theme}
					className={theme.classPrefix + 'card-root-card-media'}
				/>
				<CardHeader
					material={joinArrayField(
						data?.learningResourceType,
						item => item.id,
						label => t('labels.' + label),
						true,
					)}
					providerName={providerName && checkProviderForMaterial(data, providerName)?.found ? providerName : 'oersi'}
					theme={theme}
					imgSrc={providerName && checkProviderForMaterial(data, providerName)?.found
						? (providerImageLink || (backendURL + '/images/oer-oersi-logo.svg'))
						: backendURL + '/images/oer-oersi-logo.svg'
					}
					translate={t}
					className={theme.classPrefix + 'card-root-card-header'}
					id={`card-media-title-${data?.id}`}
				/>
				<CardContent
					data={data}
					theme={theme}
					t={t}
					className={theme.classPrefix + 'card-root-card-content'}
				/>
				<CardAction
					t={t}
					theme={theme}
					license={data?.license}
					url={
						providerName && checkProviderForMaterial(data, providerName)?.found
							? getSafeUrl(data.mainEntityOfPage?.[checkProviderForMaterial(data, providerName)?.index].id)
							: getSafeUrl(data?.id)
					}
				/>
			</MuiCard>
		)
	);
};

Card.propTypes = {
	data: PropTypes.any,
	providerName: PropTypes.string,
	providerImageLink: PropTypes.string
};

export default React.memo(Card, areEqual);
