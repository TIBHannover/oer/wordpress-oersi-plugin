/**
 *
 * @package OERSI
 * @license: MIT
 *
 * @author Edmond Kacaj <Edmond.Kacaj@tib.eu>
 *
 * Copyright (c) 2022 TIB <https://www.tib.eu/en>
 */

import { ToggleButton, ToggleButtonGroup, Tooltip } from '@mui/material';
import React from 'react';
import { useTranslation } from 'react-i18next';
import SortByAlphaIcon from '@mui/icons-material/SortByAlpha';
import FormatListNumberedIcon from '@mui/icons-material/FormatListNumbered';
import PropTypes from 'prop-types';

const SortMultiList = ({ sortBy, onChangeSortBy, theme }) => {
	const { t } = useTranslation();
	return (
		<div>
			<ToggleButtonGroup
				value={sortBy}
				exclusive
				onChange={(e, value) => {
					// onChangeSortBy(value) change only the value of the sortBy
					onChangeSortBy({
						label: value.label,
						value: value.value === 'asc' ? 'desc' : 'asc',
					});
				}}
				aria-label='text alignment'
				className={theme.classPrefix + 'sort-multi-list'}
				sx={{
					mt: '6px',
					mb: '12px',
				}}
			>
				<ToggleButton
					value={{ label: 'doc_count', value: sortBy.value }}
					aria-label='left aligned'
					className={theme.classPrefix + 'sort-multi-list-doc-count'}
					sx={{
						width: '32px',
						height: '32px',
						fontSize: '20px',
						borderTopRightRadius: '4px !important',
						borderBottomRightRadius: '4px !important',
						mr: 0.5,
						border: `1px solid ${theme.palette.grey[200]}`,
						backgroundColor:
							theme.palette.grey[200],
						color: theme.palette.grey[600],
						'&:hover': {
							backgroundColor:
								theme.palette.grey[400],
							color: theme.palette.grey[100],
						},
					}}
				>
					<Tooltip
						arrow
						placement='top-end'
						className={theme.classPrefix + 'sort-multi-list-tooltip-doc-count'}
						title={t('translation.SORT_BY_COUNTER', {
							what: sortBy.value,
						})}
						sx={{
							'& .MuiTooltip-tooltip': {
								fontSize: '2.2em',
							},
						}}
					>
						<FormatListNumberedIcon
							className={theme.classPrefix + 'sort-multi-list-doc-count-icon'}
							fontSize="inherit"
						/>
					</Tooltip>
				</ToggleButton>
				<ToggleButton
					value={{ label: 'label', value: sortBy.value }}
					aria-label='right aligned'
					className={theme.classPrefix + '-sort-multi-list-label'}
					sx={{
						width: '32px',
						height: '32px',
						fontSize: '20px',
						borderTopLeftRadius: '4px !important',
						borderBottomLeftRadius: '4px !important',
						border: `1px solid ${theme.palette.grey[200]}`,
						backgroundColor:
							theme.palette.grey[200],
						color: theme.palette.grey[600],
						'&:hover': {
							backgroundColor:
								theme.palette.grey[400],
							color: theme.palette.grey[100],
						},
					}}
				>
					<Tooltip
						arrow
						placement="top-end"
						className={theme.classPrefix + 'sort-multi-list-tooltip-label'}
						title={t('translation.SORT_BY_LABEL', {
							what: sortBy.value,
						})}
						sx={{
							'& .MuiTooltip-tooltip': {
								fontSize: '2.2em',
							},
						}}
					>
						<SortByAlphaIcon
							className={theme.classPrefix + 'sort-multi-list-label-icon'}
							fontSize="inherit"
						/>
					</Tooltip>
				</ToggleButton>
			</ToggleButtonGroup>
		</div>
	);
};

SortMultiList.propTypes = {
	sortBy: PropTypes.object.isRequired,
	onChangeSortBy: PropTypes.func.isRequired,
};

SortMultiList.defaultProps = {
	sortBy: {
		label: 'doc_count',
		value: 'desc',
	},
	onChangeSortBy: () => { },
};

export default SortMultiList;
