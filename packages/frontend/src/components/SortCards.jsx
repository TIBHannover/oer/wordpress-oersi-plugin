/**
 *
 * @package OERSI
 * @license: MIT
 *
 * Copyright (c) 2022 TIB <https://www.tib.eu/en>
 */
import React from 'react';
import {
	Grid,
	IconButton,
	Tooltip,
	useTheme
} from '@mui/material';
import { useTranslation } from 'react-i18next';
import FormatListBulletedOutlinedIcon from '@mui/icons-material/FormatListBulletedOutlined';
import AppsOutlinedIcon from '@mui/icons-material/AppsOutlined';

const SortCards = ({ setViewType }) => {
	const theme = useTheme();
	const { t } = useTranslation();
	return (
		<Grid
			sx={{
				display: 'flex',
				gap: 0.5,
				justifyContent: 'flex-end',
			}}
		>
			<Tooltip
				arrow
				placement="top-start"
				className={theme.classPrefix + 'view-type-list-tooltip'}
				title={t('translation.LIST_VIEW')}
				sx={{
					'& .MuiTooltip-tooltip': {
						fontSize: '2.2em',
					},
				}}
			>
				<IconButton
					aria-label="list"
					size="large"
					className={theme.classPrefix + 'list-view-button'}
					onClick={() => setViewType('list')}
					sx={{
						border: `1px solid ${theme.palette.grey[200]}`,
						backgroundColor:
							theme.palette.grey[200],
						color: theme.palette.grey[600],
						'&:hover': {
							backgroundColor:
								theme.palette.grey[400],
							color: theme.palette.grey[100],
						},
					}}
				>
					<FormatListBulletedOutlinedIcon fontSize="inherit" />
				</IconButton>
			</Tooltip>
			<Tooltip
				arrow
				placement="top-start"
				className={theme.classPrefix + 'view-type-card-tooltip'}
				title={t('translation.CARD_VIEW')}
			>
				<IconButton
					aria-label="cards"
					size="large"
					className={theme.classPrefix + 'card-view-button'}
					onClick={() => setViewType('card')}
					sx={{
						border: `1px solid ${theme.palette.grey[200]}`,
						backgroundColor:
							theme.palette.grey[200],
						color: theme.palette.grey[600],
						'&:hover': {
							backgroundColor:
								theme.palette.grey[400],
							color: theme.palette.grey[100],
						},
					}}
				>
					<AppsOutlinedIcon fontSize="inherit" />
				</IconButton>
			</Tooltip>
		</Grid>
	);
};

export default SortCards;
