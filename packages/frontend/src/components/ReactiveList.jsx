/**
 *
 * @package OERSI
 * @license: MIT
 *
 * @author Edmond Kacaj <Edmond.Kacaj@tib.eu>
 *
 * Copyright (c) 2022 TIB <https://www.tib.eu/en>
 */

import React, { useRef, useState } from 'react';
import {
	ReactiveList as ReactiveListComponent,
	StateProvider,
} from '@appbaseio/reactivesearch';
import { Grid, useTheme, useMediaQuery } from '@mui/material';
import Card from '@components/Card';
import ListView from '@components/ListView';
import Pagination from '@components/Pagination';
import SelectedFilters from '@components/SelectedFilters';
import SortCards from '@components/SortCards';
import ResultStats from '@components/ResultStats';
import { determineInitialPageSize } from '@helpers';
import { useTranslation } from 'react-i18next';


const ReactiveList = ({ reactiveList, providerName, providerImageLink }) => {
	const [viewType, setViewType] = useState(window?.OERSI?.configuration?.viewTypeFrontEnd || 'card');
	const { sortOptions, defaultSortOption, showSortByOption, ...reactiveListProp } = reactiveList;
	const defaultValue = sortOptions?.find(({ dataField }) => dataField === defaultSortOption);
	const [sort] = useState(defaultValue || sortOptions?.[0]);
	const queryParameters = new URLSearchParams(window.location.search)
	const type = queryParameters.get("search")
	const [displaySearch, setDisplaySearch] = useState(false);
	const theme = useTheme();
	const { t } = useTranslation();
	const [pageSize, setPageSize] = React.useState(
		determineInitialPageSize(
			window?.OERSI?.configuration?.pageSize || 12,
			window?.OERSI?.configuration?.pageSizeOptions || [12, 24, 48],
		),
	);
	const scrollToSection = useRef(null);
	const matches = useMediaQuery('(max-width: 1024px)');

	return (
		reactiveListProp && (
			<>
				{matches && type !== null ? (
					<div style={{ display: 'block' }} ref={scrollToSection} />) : ''
				}

				{matches && displaySearch ? (
					<div style={{ display: 'block' }} ref={scrollToSection} />) : (
					<div style={{ display: 'none' }} />)
				}

				<ReactiveListComponent
					className={theme.classPrefix + 'results-list-component'}
					{...reactiveListProp}
					// URLParams={false}
					size={pageSize}
					react={{ and: reactiveListProp?.and }}
					defaultQuery={() => ({
						...reactiveListProp.defaultQuery,
						size: pageSize,
						sort: [
							sort ? { [sort?.dataField]: { order: sort?.sortBy } } : {}
						]
					})}
					innerClass={{
						resultsInfo:
							theme.classPrefix +
							'results-list-component-inner-class-resultsInfo',
						sortOptions:
							theme.classPrefix +
							'results-list-component-inner-class-sortOptions',
						resultStats:
							theme.classPrefix +
							'results-list-component-inner-class-resultStats',
						noResults:
							theme.classPrefix +
							'results-list-component-inner-class-noResults',
						pagination:
							theme.classPrefix +
							'results-list-component-inner-class-pagination',
						active:
							theme.classPrefix +
							'results-list-component-inner-class-active',
						list:
							theme.classPrefix +
							'results-list-component-inner-class-list',
						poweredBy:
							theme.classPrefix +
							'results-list-component-inner-class-poweredBy',
					}}
					renderPagination={({
						pages,
						currentPage,
						totalPages,
						setPage,
						...props
					}) => (

						<StateProvider
							componentIds={reactiveListProp.componentId}
							includeKeys={['hits']}
							strict={false}
							onChange={() => {
								setDisplaySearch(true);
								scrollToSection.current?.scrollIntoView()
							}}
							render={({ searchState }) =>
								searchState.results?.hits?.total > 0 && (
									<Pagination
										page={currentPage + 1}
										total={
											searchState.results?.hits?.total
												? searchState.results?.hits
													?.total
												: 0
										}
										pageSizeOptions={
											window.OERSI.configuration
												.pageSizeOptions || [
												12, 24, 48, 96,
											]
										}
										pageSize={pageSize}
										onChangePage={page => {
											setPage(page - 1);
										}}
										onChangePageSize={size => {
											setPageSize(parseInt(size));
											// update params
											const params = new URLSearchParams(
												location.search,
											);
											params.set('size', size);
											params.set(
												reactiveListProp.componentId,
												'1',
											);
											if (history.pushState) {
												window.history.pushState(
													{},
													document.title,
													`${location.pathname
													}?${params.toString()}`,
												);
											}
										}}
										theme={theme}
									/>
								)
							}
						/>
					)}
					renderResultStats={data =>
						data && (
							<>
								<Grid
									container
									direction="column">
									<Grid
										container
										sx={{
											justifyContent: "space-between"
										}}>
										<ResultStats
											theme={theme}
											sx={{
												color: theme.palette.grey[700],
												backgroundColor: theme.palette.grey[200],
												display: 'grid',
												width: 'fit-content',
												padding: '10px 18px',
												borderRadius: 1,
											}}
											className={
												theme.classPrefix +
												'reactive-list-result-stats'
											}
										/>
										<SortCards theme={theme} t={t} setViewType={setViewType} />
									</Grid>
									<Grid
										container
									>
										<SelectedFilters theme={theme} />
									</Grid>
								</Grid>
							</>
						)

					}
				>
					{({ data }) => (
						<Grid
							container
							direction='row'
							mt={0}
							spacing={{
								xs: 5,
								sm: 3,
								xl: 3,
								xxxl: 4
							}}
							className={
								theme.classPrefix +
								'reactive-list-component-container'
							}
						>
							{viewType === 'card'
								? data.length > 0 && data.map(item => (
									<Grid
										key={item._id}
										item
										xs={12}
										sm={6}
										xl={4}
										xxl={3}
										xxxl={2}
										p={0}
										className={
											theme.classPrefix +
											'reactive-card-component-grid-item'
										}
									>
										<Card key={item._id}
											data={item}
											providerName={providerName || undefined}
											providerImageLink={providerImageLink || undefined}
										/>
									</Grid>
								))
								: data.length > 0 && data.map(item => (
									<Grid container
										key={item._id}
										item
										xs={12}
										mx={1}
										my={2}
										className={
											theme.classPrefix +
											'reactive-list-component-grid-item'
										}
										sx={{
											borderBottom: '1px solid #ededed',
											margin: {
												xs: '10px 0px',
												md: '10px 30px'
											},
											padding: '2em 0em',
											'&:first-of-type': {
												mt: {
													xs: '1em',
													md: '3em'
												},
											}
										}}
									>
										<ListView key={item._id}
											data={item}
											providerName={providerName || undefined}
											providerImageLink={providerImageLink || undefined}

										/>
									</Grid>
								))}
						</Grid>
					)}
				</ReactiveListComponent>
			</>
		)
	);
};

ReactiveList.propTypes = {
	// reactiveList: propTypes.object.isRequired,
};

export default ReactiveList;
