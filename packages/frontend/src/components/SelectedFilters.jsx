/**
 *
 * @package OERSI
 * @license: MIT
 *
 * @author Edmond Kacaj <Edmond.Kacaj@tib.eu>
 *
 * Copyright (c) 2022 TIB <https://www.tib.eu/en>
 */
import React from 'react';
import { SelectedFilters as ReactiveSelectedFilters } from '@appbaseio/reactivesearch';
import { Box, Button, useTheme } from '@mui/material';
import CancelIcon from '@mui/icons-material/Cancel';
import { getLabelForStandardComponent } from '@helpers';
import PropTypes from 'prop-types';
import { useTranslation } from 'react-i18next';

const SelectedFilters = ({ theme }) => {
	const { t } = useTranslation();
	return (
		<ReactiveSelectedFilters
			showClearAll={true}
			clearAllLabel={t('translation.CLEAR_ALL')}
			render={data => render(data, t, theme)}
			className={
				theme.classPrefix + 'selected-filters-component-container'
			}
		/>
	);
};

const render = (data, t, theme) => {
	let hasValues = false;
	const selectedValues = data?.selectedValues;
	const appliedFilters = Object.keys(data.selectedValues || {});
	const shortDescription = window?.OERSI?.configuration?.searchTextLength || 100;
	return (
		<Box
			my={1}
			className={theme.classPrefix + 'selected-filters-component-box'}
		>
			{appliedFilters
				.filter(
					id =>
						data.components.includes(id) &&
						selectedValues[id].showFilter,
				)
				.map(component => {
					const { label, value } = selectedValues[component];
					const isArray = Array.isArray(value);
					if (isArray && value.length) {
						hasValues = true;
						return value?.map((val, index) => (
							<Button
								variant='contained'
								disableElevation
								className={
									theme.classPrefix +
									'selected-filters-component-button'
								}
								sx={{
									mr: 0.5,
									mb: 0.5,
									fontSize: '0.8em',
									backgroundColor:
										theme.palette.grey[200],
									color: theme.palette.grey[700],
									'&:hover': {
										backgroundColor:
											theme.palette.grey[500],
										color: theme.palette.grey[200],
									},
								}}
								key={index}
								onClick={() => {
									const newValues = selectedValues[
										component
									].value.filter(v => v !== val);
									data.setValue(component, newValues);
								}}
								endIcon={
									<CancelIcon
										className={
											theme.classPrefix +
											'selected-filters-component-close-icon'
										}
										sx={{
											color: theme.palette.grey[400],
											'&:nth-of-type(1)': {
												fontSize: '1em'
											}
										}}
									/>
								}
							>
								{label?.indexOf('translation.') > -1 ? t(label) : label}:{' '}
								{getLabelForStandardComponent(
									val,
									component,
									t,
								)}
							</Button>
						));
					} else if (value && value.length) {
						hasValues = true;
						return (
							<Button
								variant='contained'
								className={
									theme.classPrefix +
									'selected-filter-component-button'
								}
								disableElevation
								sx={{
									mr: 0.5,
									mb: 0.5,
									fontSize: '0.8em',
									backgroundColor:
										theme.palette.grey[200],
									color: theme.palette.grey[700],
									'&:hover': {
										backgroundColor:
											theme.palette.grey[500],
										color: theme.palette.grey[200],
									},
								}}
								key={component}
								onClick={() => {
									data.setValue(component, null);
								}}
								endIcon={<CancelIcon />}
							>
								{label?.indexOf('translation.') > -1 ? t(label) : label}:{' '}
								{getLabelForStandardComponent(
									value,
									component,
								)?.oersiSubStr(shortDescription)}
							</Button>
						);
					} else {
						return null;
					}
				})}
			{hasValues ? (
				<Button
					variant='contained'
					disableElevation
					className={
						theme.classPrefix + 'selected-filter-button-clear-all'
					}
					sx={{
						mr: 0.5,
						mb: 0.5,
						fontSize: '0.8em',
						backgroundColor: theme.palette.grey[300],
						color: theme.palette.grey[700],
						'&:hover': {
							backgroundColor: theme.palette.grey[500],
							color: theme.palette.grey[200],
						},
					}}
					onClick={data.clearValues}
				>
					{data.clearAllLabel}
				</Button>
			) : null}
		</Box>
	);
};

SelectedFilters.propTypes = {
	theme: PropTypes.object,
};

SelectedFilters.defaultProps = {};

export default SelectedFilters;
