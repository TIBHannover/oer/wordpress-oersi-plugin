/**
 *
 * @package OERSI
 * @license: MIT
 *
 * @author Edmond Kacaj <Edmond.Kacaj@tib.eu>
 *
 * Copyright (c) 2022 TIB <https://www.tib.eu/en>
 */

import { Button, CardActions, Grid } from '@mui/material';
import React from 'react';
import License from '@components/License';
import LaunchIcon from '@mui/icons-material/Launch';
import PropTypes from 'prop-types';

const CardAction = ({ theme, url, license, t }) => {
	return (
		<CardActions
			className='card-actions'
			sx={{
				marginTop: 'auto',
				p: '0px',
			}}
			disableSpacing
		>
			<Grid
				container
				spacing={1}
				sx={{
					marginTop: '0',
					marginLeft: '0',
					width: '100%',
					justifyContent: 'space-between',
				}}
			>
				<Grid
					item
					xs={4}
					sx={{
						display: 'flex',
						alignItems: 'center',
						justifyContent: 'start',
						'&.MuiGrid-item': { padding: '0px' },
					}}
				>
					{(license && license?.id) && (
						<License
							license={license}
							sx={{
								color: theme.palette.twilloColor.darkBlue,
								width: 'fit-content',
								height: '100%',
								justifyContent: 'start',
								backgroundColor: theme.palette.twilloColor.white,
								borderColor: theme.palette.twilloColor.white,
								borderRadius: '0px',
								padding: '5px 16px',
								'&:hover': {
									backgroundColor:
										theme.palette.twilloColor.darkBlue,
									borderColor: theme.palette.twilloColor.darkBlue,
									color: theme.palette.twilloColor.white,
								},
							}}
						/>
					)}
				</Grid>
				<Grid
					item
					xs={8}
					sx={{
						display: 'flex',
						justifyContent: 'end',
						textAlign: 'right',
						'&.MuiGrid-item': { padding: '0' },
					}}
				>
					<Button
						sx={{
							width: 'fit-content',
							textDecoration: 'none',
							height: '50px',
							justifyContent: 'end',
							color: theme.palette.twilloColor.darkBlue,
							borderRadius: '0px',
							padding: '5px 16px',
							fontWeight: 'bold',
							fontSize: '1em',
							'&:hover': {
								backgroundColor:
									theme.palette.twilloColor.darkBlue,
								borderColor: theme.palette.twilloColor.darkBlue,
								color: theme.palette.twilloColor.white,
							},
						}}
						className='button-details'
						target='_blank'
						href={url}
						key={'button-details' + 'data._id'}
					>
						{t('cards.DETAILS')}
						<LaunchIcon
							sx={{
								marginLeft: '5px',
								fontSize: '110%',
							}}
							className='button-details-icon' />
					</Button>
				</Grid>
			</Grid>
		</CardActions>
	);
};

CardAction.propTypes = {
	theme: PropTypes.object,
	url: PropTypes.string,
	license: PropTypes.object,
	t: PropTypes.func,
};

export default CardAction;
