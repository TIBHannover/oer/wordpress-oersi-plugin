/**
 *
 * @package OERSI
 * @license: MIT
 *
 * @author Edmond Kacaj <Edmond.Kacaj@tib.eu>
 *
 * Copyright (c) 2022 TIB <https://www.tib.eu/en>
 */

import React from 'react';
import { Box, Chip, Grid, Typography } from '@mui/material';
import PropTypes from 'prop-types';
import { formatDate } from '@helpers';
import Sanitize from '@components/Sanitize';
const Content = ({
	data,
	translation,
	isReactElement = false,
	format = '',
	theme,
}) => {
	return (
		data && (

			<Grid
				container
				spacing={1}
				className={theme.classPrefix + 'card-content-container'}
			>
				<Grid
					item
					xs={12}
					sm={12}
					md={12}
					lg={12}
					className={
						theme.classPrefix +
						'card-content-item-title  ' +
						theme.classPrefix +
						'card-content-item-title-' +
						translation?.replaceAll(' ', '-')
					}
				>
					<Typography
						variant='h3'
						className={
							theme.classPrefix +
							'card-content-h5-title  ' +
							theme.classPrefix +
							'card-content-h5-title-' +
							translation?.replaceAll(' ', '-')
						}
					>
						{translation}
					</Typography>
				</Grid>
				<Grid
					item
					xs={12}
					sm={12}
					md={12}
					lg={12}
					className={
						theme.classPrefix +
						'card-content-item-body  ' +
						theme.classPrefix +
						'card-content-item-body-' +
						translation?.replaceAll(' ', '-')
					}
				>
					{isReactElement ? (
						<div
							className={
								theme.classPrefix +
								'card-content-item-body-element  ' +
								theme.classPrefix +
								'card-content-item-body-element-' +
								data?.replaceAll(' ', '-')
							}
						>
							{data}
						</div>
					) : Array.isArray(data) ? (
						<Box
							sx={{
								display: 'flex',
								flexWrap: 'wrap',
								gap: 0.5,
							}}
						>
							{data.map((item, index) => (
								<Chip
									className={
										theme.classPrefix +
										'card-content-chip-chip-item  ' +
										theme.classPrefix +
										'card-content-chip-chip-item-' +
										item
											.toLowerCase()
											.replace(/\s/g, '-')
									}
									key={index}
									label={item}
									sx={{
										fontSize: '0.8em',
										padding: '1px 8px 3px',
										borderRadius: 6,
										backgroundColor:
											theme.palette.twilloColor.teal,
										'.MuiChip-label': {
											color: theme.palette.twilloColor
												.white,
											textOverflow: 'ellipsis',
											whiteSpace: 'nowrap',
											overflow: 'hidden',
											padding: '0',
										},
									}}
								/>
							))}
						</Box>
					) : (
						<Typography
							component='div'
							variant='body1'
							className={
								theme.classPrefix + 'card-content-body-text '
							}
						>
							{' '}
							{format === 'date' &&
								formatDate(
									data,
									window?.OERSI?.date_format_js?.toMoment() ??
									'DD.MM.YYYY',
								)}
							{format === 'html' && (
								<Sanitize>{data}</Sanitize>
							)}
							{format === 'text' && data}
						</Typography>
					)}
				</Grid>
			</Grid>
		)
	);
};

Content.propTypes = {
	data: PropTypes.oneOfType([
		PropTypes.string,
		PropTypes.array,
		PropTypes.object,
	]),
	translation: PropTypes.string.isRequired,
	isReactElement: PropTypes.bool,
	theme: PropTypes.object.isRequired,
	format: PropTypes.string,
};

Content.defaultProps = {
	isReactElement: false,
	translation: '',
	format: '',
};

export default Content;
