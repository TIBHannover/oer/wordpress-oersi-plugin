/**
 *
 * @package OERSI
 * @license: MIT
 *
 * @author Edmond Kacaj <Edmond.Kacaj@tib.eu>
 *
 * Copyright (c) 2022 TIB <https://www.tib.eu/en>
 */
import { StateProvider } from '@appbaseio/reactivesearch';
import { CircularProgress, Fade, Grid, Typography } from '@mui/material';
import React from 'react';
import { useTranslation } from 'react-i18next';
import PropTypes from 'prop-types';

const ResultStats = ({ theme, sx }) => {
	const { t } = useTranslation();
	return (
		<StateProvider
			componentIds='results'
			includeKeys={['hits', 'isLoading']}
			strict={false}
			render={({ searchState }) => (
				<Grid item>
					<Typography
						aria-label='total-result'
						className={theme.classPrefix + 'result-stats-total-result'}
						component='div'
						variant='h3'
						sx={{
							fontWeight: 'normal',
							textAlign: 'center',
							padding: '5px',
							border: `1px solid ${theme.palette.grey[200]}`,
							...sx,
						}}
					>
						{searchState.results?.isLoading ? (
							<Fade
								in={searchState.results?.isLoading}
								className={
									theme.classPrefix + 'result-stats-loading'
								}
								mountOnEnter
								unmountOnExit
							>
								<CircularProgress
									aria-label='loading-progress'
									className={
										theme.classPrefix +
										'result-stats-loading-progress'
									}
									color='inherit'
									size={16}
								/>
							</Fade>
						) : (
							t('translation.SHOW_RESULT_STATS', {
								total: searchState.results?.hits?.total,
							})
						)}{' '}
					</Typography>
				</Grid>
			)}
		/>
	);
};

ResultStats.propTypes = {
	theme: PropTypes.object.isRequired,
	sx: PropTypes.object,
};

export default ResultStats;
