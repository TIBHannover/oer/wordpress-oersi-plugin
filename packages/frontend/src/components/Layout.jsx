/**
 *
 * @package OERSI
 * @license: MIT
 *
 * @author Edmond Kacaj <Edmond.Kacaj@tib.eu>
 *
 * Copyright (c) 2022 TIB <https://www.tib.eu/en>
 */
import React from 'react';
import {
	Box,
	Grid,
	useTheme,
	CssBaseline,
	Container,
} from '@mui/material';

import MultiList from '@components/MultiList';
import ReactiveList from './ReactiveList';
import SearchComponent from '@components/SearchComponent';
import PropTypes from 'prop-types';

const Layout = ({ data }) => {
	const { searchComponent, multiList, resultList } = JSON.parse(
		data?.fields || '{}',
	);
	const theme = useTheme();
	return (
		<Box
			sx={{ top: 0, right: 0, display: 'flex', width: '100%' }}
			className={theme.classPrefix + 'layout-container'}
		>
			<CssBaseline enableColorScheme />
			<Box
				className={theme.classPrefix + 'layout-box-container'}
				component='main'
				sx={{
					flexGrow: 1,
					overflow: 'auto',
					width: `calc(100% - 10px)`,
				}}
			>
				<Container
					className={theme.classPrefix + 'layout-content-container'}
					sx={{
						my: 4,
						maxWidth: '100% !important'
					}}
				>
					<Grid
						container
						spacing={{
							xs: 3,
							xxl: 5
						}}
						className={
							theme.classPrefix + 'layout-grid-search-container'
						}
					>
						<Grid
							item
							xs={12}
							className={
								theme.classPrefix + 'layout-grid-search-item'
							}
						>
							<SearchComponent
								data={searchComponent}
								providerName={data?.providerName}
								providerImageLink={data?.providerImageLink || undefined}
							/>
						</Grid>
						<Grid
							item
							xs={12}
							md={4}
							xl={3}
							xxl={3}
							xxxl={2}
							className={
								theme.classPrefix + 'layout-grid-multilist-item'
							}
						>
							{multiList != null && (
								<Grid
									container
									sx={{
										p: { xs: 0, md: 1 },
										pb: 0,
										borderRadius: '4px',
										mt: '-7px'
									}}> {
										multiList.map((list, index) => (
											<MultiList multiList={list} key={index} />
										))}
								</Grid>
							)}
						</Grid>
						<Grid
							className={
								theme.classPrefix +
								'layout-grid-result-list-item'
							}
							item
							xs={12}
							md={8}
							xl={9}
							xxl={9}
							xxxl={10}
							id='reactive-list-id'
						>
							<ReactiveList
								reactiveList={resultList}
								providerName={data?.providerName}
								providerImageLink={data?.providerImageLink || undefined}
							/>
						</Grid>
					</Grid>
				</Container>
			</Box>
		</Box>
	);
};

Layout.propTypes = {
	data: PropTypes.string
};

export default Layout;
