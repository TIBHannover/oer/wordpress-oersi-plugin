/**
 *
 * @package OERSI
 * @license: MIT
 *
 * @author Edmond Kacaj <Edmond.Kacaj@tib.eu>
 *
 * Copyright (c) 2022 TIB <https://www.tib.eu/en>
 */

import React from 'react';
import PropTypes from 'prop-types';
import { IconButton, TextField, useTheme } from '@mui/material';
import ClearIcon from '@mui/icons-material/Clear';

const InputField = ({
	label,
	value,
	onChange,
	placeholder,
	inputProps,
}) => {
	const theme = useTheme();
	return (
		<TextField
			label={label}
			className={theme.classPrefix + 'input-field'}
			InputProps={{
				endAdornment: (
					<IconButton
						className={
							theme.classPrefix + 'input-field-clear-button'
						}
						sx={{
							visibility: value !== '' ? 'visible' : 'hidden',
						}}
						onClick={() => onChange('')}
					>
						<ClearIcon />
					</IconButton>
				),
			}}
			value={value}
			inputProps={inputProps}
			variant='outlined'
			placeholder={placeholder}
			onChange={e => onChange(e.target.value)}
		/>
	);
};

InputField.propTypes = {
	label: PropTypes.string,
	value: PropTypes.string.isRequired,
	onChange: PropTypes.func.isRequired,
	placeholder: PropTypes.string,
	inputProps: PropTypes.object,
	sx: PropTypes.object,
};

InputField.defaultProps = {
	label: '',
	placeholder: '',
};

export default InputField;
