/**
 *
 * @package OERSI
 * @license: MIT
 *
 * @author Edmond Kacaj <Edmond.Kacaj@tib.eu>
 *
 * Copyright (c) 2022 TIB <https://www.tib.eu/en>
 */

import React from 'react';
import {
	Box,
	Grid,
	MenuItem,
	Pagination as PaginationComponent,
	PaginationItem,
	Paper,
	Select,
	Typography,
} from '@mui/material';
import ExpandMoreOutlinedIcon from '@mui/icons-material/ExpandMoreOutlined';

import { useTranslation } from 'react-i18next';
import PropTypes from 'prop-types';
const Pagination = ({
	page,
	total,
	pageSizeOptions,
	pageSize,
	onChangePage = () => { },
	onChangePageSize,
	theme,
}) => {
	const { t } = useTranslation();
	const pageCount = Math.ceil(total / pageSize);
	const currentRangeStart = Math.min((page - 1) * pageSize + 1, total);
	const currentRangeEnd = Math.min(page * pageSize, total);
	const maxScrollableResults = 10000;
	const maxPage = Math.floor(maxScrollableResults / pageSize);

	return (
		<Grid
			container
			className={theme.classPrefix + 'pagination-grid-container'}
		>
			<Grid
				item
				xs={12}
				className={theme.classPrefix + 'pagination-grid-container-item'}
			>
				<Paper
					className={
						theme.classPrefix +
						'pagination-grid-container-item-paper'
					}
					sx={{
						display: 'flex',
						flexDirection: 'column',
						my: 5,
						mx: 'auto',
						py: 4,
						width: '100%',
						boxShadow: 0,
						backgroundColor: 'transparent',
					}}
				>
					<Box
						sx={{ alignSelf: 'center' }}
						mb={1}
						className={
							theme.classPrefix +
							'pagination-grid-container-item-paper-box'
						}
					>
						<Typography
							className={
								theme.classPrefix +
								'pagination-grid-container-item-paper-box-typography'
							}
							variant='div'
						>
							<span className="range-start">{currentRangeStart}</span> - <span className="range-end">{currentRangeEnd}</span> <span className="total">{t('translation.SHOW_TOTAL_OF')} <b>{total}</b> {t('translation.SHOW_TOTAL_RESULTS')}</span>
						</Typography>
					</Box>
					<Box
						className={
							theme.classPrefix +
							'pagination-grid-container-item-paper-box-first-siblings'
						}
						sx={{
							display: 'flex',
							flexWrap: 'wrap',
							alignSelf: 'center',
							justifyContent: 'center',
							padding: theme.spacing(1),
							[theme.breakpoints.down('sm')]: {
								flexDirection: 'column',
								gap: '10px',
							},
						}}
					>
						<PaginationComponent
							count={pageCount > 0 ? pageCount : 1}
							page={page}
							onChange={(_event, value) => onChangePage(value)}
							className={
								theme.classPrefix +
								'pagination-grid-container-item-paper-box-first-siblings-pagination-component'
							}
							shape='circular'
							renderItem={item => (
								<PaginationItem type='last' {...item} disabled={item.page > maxPage} />
							)}
						/>
						<Select
							value={pageSize}
							IconComponent={ExpandMoreOutlinedIcon}
							SelectDisplayProps={{ "aria-label": "page size selection" }}
							displayEmpty
							onChange={event =>
								onChangePageSize(event.target.value)
							}
							className={
								theme.classPrefix +
								'pagination-grid-container-item-paper-box-first-siblings-select'
							}
						>
							{pageSizeOptions.map(v => (
								<MenuItem
									key={v}
									value={v}
									className={
										theme.classPrefix +
										'pagination-grid-container-item-paper-box-first-siblings-select-menu-item'
									}
								>
									{t('translation.PAGE_SIZE_SELECTION', {
										size: v,
									})}
								</MenuItem>
							))}
						</Select>
					</Box>
				</Paper>
			</Grid>
		</Grid>
	);
};

Pagination.propTypes = {
	page: PropTypes.number.isRequired,
	total: PropTypes.number.isRequired,
	pageSizeOptions: PropTypes.arrayOf(PropTypes.number).isRequired,
	pageSize: PropTypes.number.isRequired,
	onChangePage: PropTypes.func.isRequired,
	onChangePageSize: PropTypes.func.isRequired,
	theme: PropTypes.object,
};

Pagination.defaultProps = {
	page: 1,
	pageSizeOptions: [12, 24, 36],
	pageSize: 12,
	theme: {},
	onChangePage: () => { },
	onChangePageSize: () => { },
};

export default Pagination;
