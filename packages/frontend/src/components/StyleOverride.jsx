/**
 *
 * @package OERSI
 * @license: MIT
 *
 * @author Edmond Kacaj <Edmond.Kacaj@tib.eu>
 *
 * Copyright (c) 2022 TIB <https://www.tib.eu/en>
 */

import { getStyleOverrideAPI } from '@api';
import { useFetch } from '@hooks';
import React from 'react';
import { Helmet } from 'react-helmet';
const StyleOverride = () => {
	const { data, isError, isLoading } = useFetch(getStyleOverrideAPI);
	return !isError && !isLoading && data ? (
		<Helmet>
			<style data-emotion='css' id='style-override'>
				{data || ''}
			</style>
		</Helmet>
	) : null;
};

export default StyleOverride;
