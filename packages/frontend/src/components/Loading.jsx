/**
 *
 * @package OERSI
 * @license: MIT
 *
 * @author Edmond Kacaj <Edmond.Kacaj@tib.eu>
 *
 * Copyright (c) 2022 TIB <https://www.tib.eu/en>
 */

import React from 'react';
import Backdrop from '@mui/material/Backdrop';
import CircularProgress from '@mui/material/CircularProgress';
import PropTypes from 'prop-types';
import { useTheme } from '@mui/system';
const Loading = ({
	isLoading = true,
	handleClose = () => { /* default empty */ }
}) => {
	const theme = useTheme()
	return (
		<>
			<Backdrop
				data-testid='backdrop'
				sx={{ backgroundColor: '#ffffff99', color: '#fff', zIndex: theme => theme.zIndex.drawer + 1 }}
				open={isLoading}
				onClick={handleClose}
				className={theme.classPrefix + 'loading-full-backdrop'}
			>
				<CircularProgress
					color='secondary'
					data-testid='circular-progress'
					className={theme.classPrefix + 'loading-full-circular-progress'}
				/>
			</Backdrop>
		</>
	);
};

Loading.propTypes = {
	isLoading: PropTypes.bool.isRequired,
	handleClose: PropTypes.func.isRequired
};

Loading.defaultProps = {
	isLoading: true,
	handleClose: () => { /* default empty */ }
};

export default Loading;
