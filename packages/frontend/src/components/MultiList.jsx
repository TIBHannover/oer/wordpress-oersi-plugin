/**
 *
 * @package OERSI
 * @license: MIT
 *
 * @author Edmond Kacaj <Edmond.Kacaj@tib.eu>
 *
 * Copyright (c) 2022 TIB <https://www.tib.eu/en>
 */

import React, { useEffect, useState } from 'react';
import { MultiList as MultiAppSearchList } from '@appbaseio/reactivesearch';
import {
	Accordion,
	AccordionDetails,
	AccordionSummary,
	Box,
	Grid,
	Typography,
	useTheme,
} from '@mui/material';
import { ExpandMore } from '@mui/icons-material';
import { useAggregation } from '@hooks';
import InputField from '@components/InputField';
import { PropTypes } from 'prop-types';
import { getLabelForStandardComponent, customQuery } from '@helpers/helpers';
import { useTranslation } from 'react-i18next';
import SortMultiList from '@components/SortMultiList';
import HierarchicalMultiSelect from './HierarchicalMultiSelect';
import {
	HierarchicalDataPreparer,
	modifyAllParents,
	modifyAll,
	findAllChildNodes,
	getSiblings,
	sortDataBy,
} from '@helpers';
import { getMultilistParentData } from '@api';
import MultiListRender from './MultiListRender';


const MultiList = ({ multiList }) => {
	const theme = useTheme();
	const [vocabScheme, setVocabScheme] = useState(null);
	const [itemStates, setItemStates] = useState({});
	const [expandItemsDefault, setExpandItemsDefault] = useState(false);
	const { t } = useTranslation();
	const [sortLabelBy, setSortLabelBy] = useState({
		label: 'doc_count',
		value: 'desc',
	});
	const [values, setValues] = useState([]);
	const [textFieldValue, setTextFieldValue] = useState('');
	const [isExpanded, setExpanded] = useState(false);
	const onChangeExpanded = (expanded) => {
		setExpanded(expanded);
	};

	const isHierarchicalFilter = multiList?.hierarchical_filter !== undefined;

	useEffect(() => {
		async function loadScheme() {
			if (isHierarchicalFilter) {
				const schemeResponse = await getMultilistParentData(
					multiList.hierarchical_filter.schemaPath,
				);
				if (schemeResponse.status === 200) {
					setVocabScheme((await schemeResponse.data) || []);
				}
			}
		}
		loadScheme();
	}, [multiList?.hierarchical_filter]);

	const handleClearClick = value => {
		setTextFieldValue(value);
		onUpdateSearchTerm(value);
		expandAllItems();
	};
	const { defaultQuery, onUpdateSearchTerm, searchTerm } = useAggregation({
		dataField: multiList?.dataField,
		debounce: multiList?.debounce,
		minSearchLength: multiList?.minSearchLength,
		reloadOnSearch: multiList?.reloadOnSearch,
		allowedSearchRegex: multiList?.allowedSearchRegex,
		defaultQuery: multiList?.defaultQuery
			? eval(multiList?.defaultQuery)
			: null,
		size: multiList?.size,
	});
	if (multiList.componentId === 'license') {
		multiList['customQuery'] = customQuery;
	}

	const onToggleExpandItem = itemKey => {
		const updatedItemState =
			itemKey in itemStates
				? {
					...itemStates[itemKey],
					expanded: !itemStates[itemKey].expanded,
				}
				: { expanded: !expandItemsDefault };
		setItemStates({ ...itemStates, [itemKey]: updatedItemState });
	};

	const expandAllItems = () => {
		const updatedItemStates = {};
		for (let s in itemStates) {
			updatedItemStates[s] = { ...s, expanded: true };
		}
		setItemStates(updatedItemStates);
		setExpandItemsDefault(true);
	};

	function addSelectedFlag(data, value) {
		const addSelected = d => {
			d.selected = d.key in value;
			if (d.children?.length) {
				d.children = d.selected
					? modifyAll(d.children, e => (e.selected = true))
					: d.children.map(addSelected);
			}
			if (d.selected) {
				modifyAllParents(d, e => {
					e.hasSelectedChild = true;
				});
			}
			return d;
		};
		return modifyAll(data, d => (d.hasSelectedChild = false)).map(
			addSelected,
		);
	}

	const transformData = (data, value) => {
		const matchesSearchTerm = d =>
			d.label?.match(new RegExp('.*' + searchTerm + '.*', 'i'));
		if (!isHierarchicalFilter) {
			const labelledData = data
				.map(d => {
					return {
						...d,
						label: getLabelForStandardComponent(
							d.key,
							multiList.componentId,
							t,
						),
					};
				})
				.filter(matchesSearchTerm);
			return sortDataBy(
				labelledData,
				[sortLabelBy?.label || 'doc_count'],
				[sortLabelBy?.value || 'asc'],
			);
		}

		const preparedData = new HierarchicalDataPreparer(data, vocabScheme)
			.modifyNodes(d => {
				d.label = getLabelForStandardComponent(
					d.key,
					multiList.componentId,
					t,
				)
				d.matchesSearch = matchesSearchTerm(d)
			})
			.modifyNodes(d => {
				d.expanded =
					d.key in itemStates
						? itemStates[d.key].expanded
						: expandItemsDefault
				d.hidden =
					!d.matchesSearch &&
					findAllChildNodes(d, (e) => e.matchesSearch).length === 0
			}).data;
		return addSelectedFlag(
			sortDataBy(
				preparedData,
				[sortLabelBy?.label || 'doc_count'],
				[sortLabelBy?.value || 'asc'],
			),
			value,
		);
	};

	const selectHierarchicalNode = node => {
		let newValues = values ? values : [];
		// add node key to values
		newValues = [...newValues, node.key];

		// remove child-keys from values (because all children will be marked as "selected" anyway)
		const selectedChildren = findAllChildNodes(node, e => e.selected).map(
			e => e.key,
		);
		newValues = newValues.filter(e => !selectedChildren.includes(e));
		return newValues;
	};

	const deselectHierarchicalNode = d => {
		let newValues = values ? values : [];
		// also deselect parent node
		if (d.parent) {
			newValues = deselectHierarchicalNode(d.parent);
		}

		// remove node key from values
		newValues = newValues.filter(v => v !== d.key);

		// add selected sibling keys to values
		const selectedSiblings = getSiblings(d).filter(
			(e) => e.selected && !newValues.includes(e.key)
		)
		newValues = [...newValues, ...selectedSiblings.map(e => e.key)];
		return newValues;
	};

	return (
		multiList && (
			<Box
				className={theme.classPrefix + 'multilist-component-box'}
				sx={{
					width: '100%',
					maxWidth: '100%',
					margin: '0 auto',
					mb: '0.3em',
				}}
			>
				<Accordion
					disableGutters
					onChange={onChangeExpanded}
					className={
						theme.classPrefix + 'multilist-component-accordion'
					}
					sx={{
						boxShadow: '0 0px 1px 0 rgba(0,0,0,0.1)',
						borderRadius: '4px',
						mb: '0.1em',
						transition: 'all .3s',
						'&:hover': {
							boxShadow: '0 0 30px -10px rgba(0,0,0,0.15)',
						}
					}}
				>
					<AccordionSummary
						expandIcon={
							<ExpandMore
								sx={{
									fontSize: '1.5em',
								}}
								className={
									theme.classPrefix +
									'multilist-component-accordion-summary-arrow-drop-down-icon'
								}
							/>
						}
						onClick={() => setExpanded(!isExpanded)}
						aria-controls='panel1a-content'
						id='panel1a-header'
						className={
							theme.classPrefix +
							'multilist-component-accordion-summary'
						}
						sx={{
							verticalAlign: 'center',
							borderRadius: '4px',
						}}
					>
						<Grid container>
							<Grid xs={10}>
								<Typography
									variant='h2'
									className={
										theme.classPrefix +
										'multilist-component-accordion-summary-title'
									}
								>
									{multiList?.title?.indexOf('translation.') > -1
										? t(multiList?.title)
										: multiList?.title}
								</Typography>
							</Grid>
						</Grid>
					</AccordionSummary>
					<AccordionDetails
						className={
							theme.classPrefix +
							'multilist-component-accordion-details'
						}
						sx={{
							position: 'relative',
							borderBottom: '30px solid white',
							padding: '0',
						}}
					>
						<Grid>
							{multiList?.showSearch && (
								<InputField
									value={textFieldValue}
									onChange={e => handleClearClick(e)}
									placeholder={t(
										'translation.INPUT_PLACEHOLDER',
										{
											what:
												multiList?.title?.indexOf(
													'translation.',
												) > -1
													? t(multiList?.title)
													: multiList?.title,
										},
									)}
									inputProps={{
										'aria-label': 'search-' + 'author',
									}}
									className={
										theme.classPrefix +
										'multilist-component-input-field'
									}
									sx={{
										width: '100%',
										display: 'flex'
									}}
								/>
							)}
						</Grid>
						<Grid
							className={
								theme.classPrefix + 'multilist-component-grid'
							}
						>
							<Grid
								item
								xs={12}
								sx={{
									display: 'flex',
									flexDirection: 'row',
									justifyContent: 'right',
									px: '1em',
									pt: '0.5em',
								}}
								className={
									theme.classPrefix +
									'multilist-component-grid-item'
								}
							>
								<SortMultiList
									sortBy={sortLabelBy}
									onChangeSortBy={value => {
										setSortLabelBy(value);
									}}
									theme={theme}
								/>
							</Grid>
							<MultiAppSearchList
								className={
									theme.classPrefix +
									'multi-list-data-multi-list-component'
								}
								{...multiList}
								showSearch={false}
								value={values}
								onChange={setValues}
								title={
									// Set the title of the multi-list to false. It
									// already shows up in the accordion summary title
									// above.
									multiList?.showTitle === false
										? multiList?.title?.indexOf(
											'translation.',
										) > -1
											? t(multiList?.title)
											: multiList?.title
										: ''
								}
								react={{ and: multiList?.and }}
								defaultQuery={() => defaultQuery}
								customQuery={
									multiList?.customQuery
										? eval(multiList?.customQuery)
										: null
								}
								innerClass={{
									// This class is disabled
									// title:
									// 	theme.classPrefix +
									// 	'multi-list-inner-class-title',
									input:
										theme.classPrefix +
										'multi-list-inner-class-input',
									list:
										theme.classPrefix +
										'multi-list-inner-class-list',
									checkbox:
										theme.classPrefix +
										'multi-list-inner-class-checkbox',
									label:
										theme.classPrefix +
										'multi-list-inner-class-label',
									count:
										theme.classPrefix +
										'multi-list-inner-class-count',
								}}
							>
								{({
									loading,
									error,
									data,
									value,
									handleChange,
								}) => {
									if (!loading && !error && isExpanded) {
										if (isHierarchicalFilter) {
											return (
												<HierarchicalMultiSelect
													component={
														multiList.componentId
													}
													data={transformData(
														data,
														value,
													)}
													onToggleExpandItem={
														onToggleExpandItem
													}
													value={value}
													theme={theme}
													onSelectionChange={d => {
														setValues(
															d.selected
																? deselectHierarchicalNode(
																	d,
																)
																: selectHierarchicalNode(
																	d,
																),
														);
													}}
													t={t}
												/>
											);
										} else {
											return (
												<MultiListRender
													data={transformData(
														data,
														value,
													)}
													value={value}
													onSelectionChange={
														handleChange
													}
												/>
											);
										}
									}
								}}
							</MultiAppSearchList>
						</Grid>
					</AccordionDetails>
				</Accordion>
			</Box>
		)
	);
};

MultiList.propTypes = {
	multiList: PropTypes.object.isRequired,
};

export default MultiList;
