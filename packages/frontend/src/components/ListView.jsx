/**
 *
 * @package OERSI
 * @license: MIT
 *
 * @author: Hunar Karim <Hunar.Karim@tib.eu>
 * Copyright (c) 2022 TIB <https://www.tib.eu/en>
 */

import React, { useState } from 'react';
import {
	Card,
	CardMedia as CardMediaMUI,
	useTheme,
	Typography,
	Box,
	Link,
	Button,
	CardContent
} from '@mui/material';
import { LazyLoadComponent } from 'react-lazy-load-image-component';
import Sanitize from '@components/Sanitize';
import License from '@components/License';
import { getSafeUrl, getThumbnailUrl, joinArrayField } from '@helpers';
import { useTranslation } from 'react-i18next';
import DatasetLinkedOutlinedIcon from '@mui/icons-material/DatasetLinkedOutlined';
import { alpha } from '@mui/system';

const ListView = ({ data }) => {
	const { name, image, description, learningResourceType, license, creator, id } = data;
	const theme = useTheme();
	const { t } = useTranslation();
	const [hasError, setHasError] = useState(false);
	const [isExpanded, setIsExpanded] = useState(false);

	const thumbnailUrl = getThumbnailUrl(data?._id);
	const imageUrl = thumbnailUrl || image;

	const handleError = (e) => {
		console.error('Error loading image: ', e);
		setHasError(true);
	};

	const renderImageOrIcon = () => {
		if (hasError || !imageUrl) {
			return (
				<Box
					sx={{
						height: 220,
						width: 220,
						display: 'flex',
						justifyContent: 'center',
						alignItems: 'center',
						backgroundColor: theme.palette.grey[100],
						border: `2px solid ${theme.palette.grey[200]}`,
						borderRadius: '4px',
						[theme.breakpoints.down('sm')]: {
							height: '40%',
						},
						'&:hover': {
							backgroundColor: alpha(theme.palette.grey[200], 0.9),
						},
					}}
					title={name}
					role="img"
					aria-label={`Failed to load image: ${name}`}
				>
					<DatasetLinkedOutlinedIcon
						sx={{
							width: '110px',
							borderRadius: '50%',
							height: '110px',
							background: alpha(theme.palette.grey[300], 0.7),
							color: theme.palette.grey[100],
							padding: '1em',
							[theme.breakpoints.down('sm')]: {
								width: '80px',
								height: '80px',
							},
						}} />
				</Box>
			);
		}
		return (
			<CardMediaMUI
				component='img'
				sx={{
					width: 220,
					minWidth: 220,
					objectFit: 'contain',
					height: 'auto',
					alignSelf: 'start',
					borderRadius: '4px',
				}}
				image={imageUrl}
				alt={name || 'Image'}
				onError={handleError}
				className={theme.classPrefix + 'list-view-card-media'}
			/>
		);
	};

	return (
		<LazyLoadComponent
			delayTime={100}
			className={theme.classPrefix + 'list-view-lazy-load-component'}
		>
			<Card sx={{
				display: 'flex',
				gap: 2,
				borderRadius: 0,
				backgroundColor: 'transparent',
				height: 'auto',
				boxShadow: 'none',
				transition: 'all .3s',
				[theme.breakpoints.down('sm')]: {
					height: 'auto',
					flexDirection: 'column'
				},
				'&:hover': {
					backgroundColor: 'transparent',
					boxShadow: 'none',
					transform: 'translateY(0em)'
				}
			}}
				className={theme.classPrefix + 'list-view-card-container'}
				role="region"
				aria-labelledby={`list-view-card-title-${id}`}
			>
				<Box>
					{renderImageOrIcon()}
				</Box>
				<Box sx={{
					display: 'flex',
					flexDirection: 'column',
					width: '100%',
					gap: 2,
					margin: 1,
					maxWidth: '800px',
				}}
					className={theme.classPrefix + 'list-view-card-box-container'}
				>
					<CardContent sx={{
						flex: '1 0 auto',
						backgroundColor: 'transparent'
					}}
						className={theme.classPrefix + 'list-view-card-content'}
					>
						{learningResourceType && (
							<Typography
								sx={{
									color: theme.palette.grey[700],
									fontSize: '0.8em',
									fontWeight: '600',
									textTransform: 'uppercase',
								}}
								className={theme.classPrefix + 'list-view-card-box-footer-container-learning-recourse-type'}
							>
								{joinArrayField(
									learningResourceType,
									item => item.id,
									label => t('labels.' + label),
									true
								)}
							</Typography>
						)}
						<Link
							underline='hover'
							href={getSafeUrl(id)}
							target='_blank'
							className={theme.classPrefix + 'list-view-card-content-link'}
							sx={{
								color: theme.palette.grey[800],
								transition: 'all .3s',
								textDecoration: 'none!important',
								'&:hover': {
									color: theme.palette.grey[500],
								}
							}}
							id={`list-view-card-title-${id}`}
						>
							<Typography
								component="div"
								variant="h3"
								className={theme.classPrefix + 'list-view-card-content-name'}
								sx={{
									fontSize: '1.8em',
									my: 0.5,
								}}
							>
								<Sanitize>{name}</Sanitize>
							</Typography>
						</Link>
						<Typography
							component="div"
							className={theme.classPrefix + 'list-view-card-content-creator'}
							sx={{
								fontStyle: 'italic',
								marginBottom: 1,
								fontSize: '0.85em',
							}}
						>
							{creator && joinArrayField(creator, item => item.name, null)}
						</Typography>
						{description && (
							<>
								<Typography
									className={theme.classPrefix + 'list-view-card-content-description'}
								>
									{isExpanded ? (
										<Sanitize>
											{description?.oersiSubStr(-1)}
										</Sanitize>
									) : (
										<Sanitize>
											{description?.oersiSubStr(350)}
										</Sanitize>
									)}
								</Typography>
								<Typography
									className={theme.classPrefix + 'list-view-card-content-show-more-button'}
								>
									{description.length > 350 && (
										<Button
											variant='contained'
											onClick={() => setIsExpanded(!isExpanded)}
											sx={{
												mt: 1,
												fontSize: '0.85em',
												backgroundColor: '#a4d8d8',
												'&:hover': {
													backgroundColor: '#c3e5e5',
												},
											}}
										>
											<Typography
												sx={{
													fontSize: '1em',
													color: theme.palette.grey[700]
												}}
											>
												{isExpanded
													? t('translation.SHOW_LESS_TEXT')
													: t('translation.SHOW_MORE_TEXT')}
											</Typography>
										</Button>
									)}
								</Typography>
							</>
						)}
					</CardContent>
					<Box sx={{
						display: 'flex',
						alignItems: 'center',
						pb: 1
					}}
						className={theme.classPrefix + 'list-view-card-box-footer-container'}
					>
						{license && (
							<License
								license={license}
								sx={{
									mr: 1,
									width: 'auto',
									color: theme.palette.grey[700],
									backgroundColor: theme.palette.grey[100],
									border: `2px solid ${theme.palette.grey[200]}`,
									fontSize: '0.8em',
									'&:hover': {
										backgroundColor: theme.palette.grey[700],
										borderColor: theme.palette.grey[100],
										color: theme.palette.grey[100],
									},
								}}
								className={theme.classPrefix + 'list-view-card-box-footer-container-license'}
							/>
						)}
					</Box>
				</Box>
			</Card>
		</LazyLoadComponent>
	);
};

export default ListView;
