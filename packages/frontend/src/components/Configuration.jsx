/**
 *
 * @package OERSI
 * @license: MIT
 *
 * @author Edmond Kacaj <Edmond.Kacaj@tib.eu>
 *
 * Copyright (c) 2022 TIB <https://www.tib.eu/en>
 */

import React from 'react';
import App from '../App';
import Loading from '@components/Loading';
import NotFound from '@components/NotFound';
import { useFetch } from '@hooks';
import { getElasticsSearchAPI } from '@api';
import i18next from 'i18next';

const Configuration = () => {

	React.useEffect(() => {
		i18next.changeLanguage(window?.OERSI?.language || 'de');
	}, [window?.OERSI?.language]);

	const { data, isError, error, isLoading } = useFetch(getElasticsSearchAPI);
	if (isLoading) return <Loading />;
	if (isError) return <NotFound error={error} />;
	if (!isLoading && !isError && data) {
		return <App data={data} />;
	}
};

Configuration.propTypes = {};
Configuration.defaultProps = {};

export default Configuration;
