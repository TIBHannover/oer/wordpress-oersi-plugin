/**
 *
 * @package OERSI
 * @license: MIT
 *
 * @author Edmond Kacaj <Edmond.Kacaj@tib.eu>
 *
 * Copyright (c) 2022 TIB <https://www.tib.eu/en>
 */

import React from 'react';
import DOMPurify from 'dompurify';
import PropTypes from 'prop-types';

const Sanitize = ({ children }) => {
	const unsafe = React.Children.toArray(children)
		.filter(child => typeof child === 'string')
		.join('');
	const config = {
		RETURN_TRUSTED_TYPE: true,
		BLOCK_TAGS: ['iframe', 'script'],
		BLOCK_ATTR: ['style', 'on*', 'formaction', 'formtarget'],
		WHOLE_DOCUMENT: false,
	};
	const safe = DOMPurify.sanitize(unsafe, config);

	return window?.OERSI?.configuration?.showHtmlTags &&
		window?.OERSI?.configuration?.showHtmlTags === true ? (
		<div className="oersi-frontend-sanitize-div" dangerouslySetInnerHTML={{ __html: safe }} />
	) : (
		<div className='oersi-frontend-non-sanitize-div'>{children}</div>
	);
};

Sanitize.propTypes = {
	children: PropTypes.node.isRequired,
};

Sanitize.defaultProps = {
	children: <></>,
};

export default Sanitize;
