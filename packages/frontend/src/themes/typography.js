// ==============================|| DEFAULT THEME - TYPOGRAPHY  ||============================== //

const Typography = fontFamily => ({
	htmlFontSize: 16,
	fontFamily,
	fontWeightLight: 300,
	fontWeightRegular: 400,
	fontWeightMedium: 500,
	fontWeightBold: 600,
	color: '#141414',
	h1: {
		fontWeight: 500,
		fontSize: '1.5em',
		lineHeight: 1.21,
	},
	h2: {
		fontWeight: 500,
		fontSize: '1.3em',
		lineHeight: 1.27,
	},
	h3: {
		fontWeight: 500,
		fontSize: '1.1em',
		lineHeight: 1.33,
	},
	h4: {
		fontWeight: 500,
		fontSize: '1.0em',
		lineHeight: 1.4,
	},
	h5: {
		fontWeight: 500,
		fontSize: '0.9em',
		lineHeight: 1.5,
	},
	h6: {
		fontWeight: 500,
		fontSize: '0.7em',
		lineHeight: 1.57,
	},
	caption: {
		fontWeight: 400,
		fontSize: '0.6em',
		lineHeight: 1.66,
	},
	body1: {
		fontSize: '1em',
		lineHeight: 1.57,
	},
	body2: {
		fontSize: '1em',
		lineHeight: 1.66,
	},
	subtitle1: {
		fontSize: '1em',
		fontWeight: 500,
		lineHeight: 1.57,
	},
	subtitle2: {
		fontSize: '0.8em',
		fontWeight: 500,
		lineHeight: 1.66,
	},
	overline: {
		lineHeight: 1.66,
	},
	button: {
		textTransform: 'capitalize',
	},
});

export default Typography;
