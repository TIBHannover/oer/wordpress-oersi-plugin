// material-ui
import { createTheme } from '@mui/material/styles';

// third-party
import { presetPalettes } from '@ant-design/colors';

// project import
import ThemeOption from './theme';

// ==============================|| DEFAULT THEME - PALETTE  ||============================== //

const Palette = mode => {
	const colors = presetPalettes;

	const greyPrimary = [
		'#ffffff',
		'#fafafa',
		'#f5f5f5',
		'#f0f0f0',
		'#d9d9d9',
		'#bfbfbf',
		'#8c8c8c',
		'#595959',
		'#262626',
		'#141414',
		'#000000',
	];
	colors.actionColor = ['#A6D492', '#F38C3E'];
	colors.baseColor = ['#54B6B5', '#F25B68'];

	colors.groundColor = [
		'#0A1F40',
		'#D1D8E1',
		'#F0F2F6', // white
		'#FAFAFC', // white
	];

	const greyAscent = [
		'#fafafa',
		'#bfbfbf',
		'#434343',
		'#1f1f1f',
		'#E0E0E0',
		'#0a1f40',
	];
	const greyConstant = ['#fafafb', '#e6ebf1'];

	colors.grey = [...greyPrimary, ...greyAscent, ...greyConstant];

	const paletteColor = ThemeOption(colors);

	return createTheme({
		palette: {
			mode,
			common: {
				black: '#000',
				white: '#fff',
			},
			...paletteColor,
			text: {
				primary: paletteColor.grey[800],
				secondary: paletteColor.grey[600],
				disabled: paletteColor.grey[400],
			},
			action: {
				active: paletteColor.grey[600],
				disabled: paletteColor.grey[400],
			},
			divider: paletteColor.grey[200],
			background: {
				paper: '#fff',
				default: '#fff',
			},
		},
	});
};

export default Palette;
