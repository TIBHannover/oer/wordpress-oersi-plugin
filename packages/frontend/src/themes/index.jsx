import React, { useMemo } from 'react';
import PropTypes from 'prop-types';

// material-ui
import { StyledEngineProvider } from '@mui/material';
import { createTheme, ThemeProvider } from '@mui/material/styles';

// project import
import Palette from './palette';
import Typography from './typography';
import CustomShadows from './shadows';
import componentsOverride from './overrides';

const classPrefix =
	import.meta.env.OERSI_PLUGIN_CLASS_PREFIX || 'oersi-frontend-';
function ThemeCustomization({ children, container }) {
	const theme = Palette('light');
	// eslint-disable-next-line react-hooks/exhaustive-deps
	const themeTypography = Typography(`inherit`);
	const themeCustomShadows = useMemo(() => CustomShadows(theme), [theme]);

	const themeOptions = useMemo(
		() => ({
			breakpoints: {
				values: {
					xs: 0,
					sm: 768,
					md: 1024,
					lg: 1266,
					xl: 1536,
					xxl: 1920,
					xxxl: 2560,
				},
			},
			direction: 'ltr',
			mixins: {
				toolbar: {
					minHeight: 60,
					paddingTop: 8,
					paddingBottom: 8,
				},
			},
			palette: theme.palette,
			customShadows: themeCustomShadows,
			typography: themeTypography,
			classPrefix: classPrefix,
		}),
		[theme, themeTypography, themeCustomShadows],
	);

	const themes = createTheme(themeOptions);
	themes.components = componentsOverride(themes, container);

	return (
		<StyledEngineProvider>
			<ThemeProvider theme={themes}>
				{children}
			</ThemeProvider>
		</StyledEngineProvider>
	);
}

ThemeCustomization.propTypes = {
	children: PropTypes.node,
};

export default ThemeCustomization;
