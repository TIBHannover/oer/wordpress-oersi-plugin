// material-ui
import { alpha } from '@mui/material/styles';

// ==============================|| OVERRIDES - INPUT FIELD ||============================== //

export default function InputField(theme) {
	const boxShadowColor = alpha(theme.palette.twilloColor.teal, 0.5);
	return {
		MuiFormControl: {
			styleOverrides: {
				root: {
					margin: '1em 1em 0',
					width: 'calc(100% - 2em)',
					'.MuiInputBase-formControl': {
						padding: '0.25em 0',
						borderRadius: '4px',
						backgroundColor: alpha(theme.palette.grey[100], 0.3),
						transition: 'all 0.3s',
						'input::placeholder': {
							color: '#666666',
							fontStyle: 'italic',
						},
						'&:hover fieldset': {
							borderColor: boxShadowColor,
							backgroundColor: alpha(theme.palette.grey[200], 0.2)
						},
						'&:focus fieldset': {
							borderColor: boxShadowColor,
							boxShadow: `0 0 0 3px ${boxShadowColor}`,
						},
					},
					'.Mui-focused fieldset': {
						borderColor: boxShadowColor,
						boxShadow: `0 0 0 3px ${boxShadowColor}!important`,
						transition: 'all 0.3s',
					}
				},
			},
		},
	}
}
