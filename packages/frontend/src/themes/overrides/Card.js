// ==============================|| OVERRIDES - LINER PROGRESS ||============================== //

export default function Card(theme) {
	const cardHeight = '450px';
	return {
		MuiCard: {
			styleOverrides: {
				root: {
					height: '450px',
					position: 'sticky',
					boxShadow: '1px 1px 6px 0 rgba(0,0,0,0.1)',
					maxWidth: '100%',
					transition: 'all 0.3s ease',
					'&:hover': {
						boxShadow: '1px 1px 20px 0 rgba(0,0,0,0.2)',
					},
					[theme.breakpoints.down('sm')]: {
						height: '300px'
					},
				},
				media: {
					width: '100%',
					height: '100%',
				},
				content: {
					width: '100%',
					height: '10px',
				},
			},
		},
		MuiCardHeader: {
			styleOverrides: {
				root: {
					backgroundColor: theme.palette.twilloColor.darkBlue,
					color: theme.palette.grey['A100'],
					width: '100%',
					height: '50px',
					display: 'flex',
					justifyContent: 'center',
					flexDirection: 'row',
				},
				avatar: {
					alignSelf: 'left',
					textAlign: 'left',
				},
				content: {
					alignSelf: 'right',
					textAlign: 'right',
				},
			},
		},
		MuiCardContent: {
			styleOverrides: {
				root: {
					backgroundColor: theme.palette.twilloColor.alabaster,
					width: '100%',
					padding: '0px',
					'h3': {
						fontFamily: '-apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Noto Sans", Ubuntu, "Droid Sans", "Helvetica Neue", sans-serif',
						fontWeight: '500',
						fontSize: '1.25em',
						lineHeight: '1.33',
						display: '-webkit-box',
						'-webkit-box-orient': 'vertical',
						'-webkit-line-clamp': '2',
						overflow: 'hidden',
						textOverflow: 'ellipsis'
					}
				},
			},
		},
		MuiCardMedia: {
			styleOverrides: {
				root: {
					width: '100%',
					height: '50%',
					objectFit: 'cover',
				},
			},
		},
		MuiCardActions: {
			styleOverrides: {
				root: {
					backgroundColor: theme.palette.twilloColor.white,
					position: 'sticky',
					bottom: '0',
					width: '100%',
					height: '50px',
					boxShadow: '0px -1px 8px 0px rgba(0,0,0,0.1)',
					'.button-details': {
						textDecoration: 'none'
					}
				},
			},
		},
	};
}
