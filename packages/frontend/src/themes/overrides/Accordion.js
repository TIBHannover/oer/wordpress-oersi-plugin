// ==============================|| OVERRIDES - LINER PROGRESS ||============================== //

export default function Accordion(theme) {
	return {
		MuiAccordion: {
			styleOverrides: {
				root: {
					backgroundColor: theme.palette.grey[0],
					marginBottom: theme.spacing(2),
					borderRadius: '4px',
					'&.Mui-expanded': {
						margin: 0,
						boxShadow: '0 0 30px -10px rgba(0,0,0,0.15)',
					},
				},
			},
		},
		MuiAccordionSummary: {
			styleOverrides: {
				root: {
					minHeight: '54px',
					height: '54px',
					width: '100%',
					backgroundColor: '#a4d8d8',
					'&:hover': {
						backgroundColor: '#c3e5e5',
					},
					'&.Mui-expanded': {
						borderRadius: '4px 4px 0 0',
						backgroundColor: '#c3e5e5',
					},
					'h2': {
						fontFamily: '-apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Noto Sans", Ubuntu, "Droid Sans", "Helvetica Neue", sans-serif',
						fontSize: '1.2em',
						fontWeight: '500',
					}
				},
				expandIconWrapper: {
					color: theme.palette.grey[700],
				},
			},
		},
		MuiAccordionDetails: {
			styleOverrides: {
				root: {
					maxHeight: '100%',
					maxWidth: '100%',
				},
			},
		},
	};
}
