// ==============================|| OVERRIDES - LINER PROGRESS ||============================== //

export default function Paper(theme) {
	return {
		MuiPaper: {
			styleOverrides: {
				root: {
					backgroundColor: theme.palette.grey[0],
				},
			},
		},
	};
}
