// ==============================|| OVERRIDES - CHIP ||============================== //

export default function Chip(theme) {
	return {
		MuiChip: {
			styleOverrides: {
				root: {
					borderRadius: 4,
					height: 'auto',
					overflow: 'none',
					textOverflow: 'none',
					textAlign: 'center',
				},
				sizeLarge: {
					fontSize: '1.5em',
					height: 40,
				},
				light: {
					color: theme.palette.primary.main,
					backgroundColor: theme.palette.twilloColor.darkBlue,
					borderColor: theme.palette.primary.light,
					'&.MuiChip-lightError': {
						color: theme.palette.error.main,
						backgroundColor: theme.palette.twilloColor.darkBlue,
						borderColor: theme.palette.error.light,
					},
					'&.MuiChip-lightSuccess': {
						color: theme.palette.success.main,
						backgroundColor: theme.palette.twilloColor.darkBlue,
						borderColor: theme.palette.success.light,
					},
					'&.MuiChip-lightWarning': {
						color: theme.palette.warning.main,
						backgroundColor: theme.palette.twilloColor.darkBlue,
						borderColor: theme.palette.warning.light,
					},
				},
				label: {
					whiteSpace: 'nowrap',
					overflow: 'visible',
					textOverflow: 'clip',
				},
			},
		},
	};
}
