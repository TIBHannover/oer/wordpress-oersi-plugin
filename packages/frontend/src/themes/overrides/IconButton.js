// ==============================|| OVERRIDES - ICON BUTTON ||============================== //

export default function IconButton(theme) {
	return {
		MuiIconButton: {
			styleOverrides: {
				root: {
					borderRadius: 4,
					backgroundColor: theme.palette.primary.contrastText,
				},
				sizeLarge: {
					width: theme.spacing(5.5),
					height: theme.spacing(5.5),
					fontSize: '1.25em',
				},
				sizeMedium: {
					width: theme.spacing(4.5),
					height: theme.spacing(4.5),
					fontSize: '1em',
				},
				sizeSmall: {
					width: theme.spacing(3.75),
					height: theme.spacing(3.75),
					fontSize: '0.75em',
				},
			},
		},
	};
}
