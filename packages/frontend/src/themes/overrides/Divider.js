// ==============================|| OVERRIDES - DIVIDER ||============================== //

export default function Divider(theme) {
	return {
		MuiDivider: {
			styleOverrides: {
				root: {
					backgroundColor: 'transparent',
					height: '2px',
				}
			}
		}
	};
}
