// third-party
import { merge } from 'lodash';

// project import
import Accordion from './Accordion';
import AppBar from './AppBar';
import Badge from './Badge';
import Button from './Button';
import Card from './Card';
import Checkbox from './Checkbox';
import Chip from './Chip';
import ControlLabel from './ControlLabel';
import CssBaseLine from './CssBaseLine';
import Divider from './Divider';
import IconButton from './IconButton';
import InputField from './InputField';
import InputLabel from './InputLabel';
import LinearProgress from './LinearProgress';
import Link from './Link';
import ListItemIcon from './ListItemIcon';
import OutlinedInput from './OutlinedInput';
import Pagination from './Pagination';
import Paper from './Paper';
import SearchComponent from './SearchComponent';
import Tab from './Tab';
import TableCell from './TableCell';
import Tabs from './Tabs';
import ToolTip from './ToolTip';
import Toolbar from './Toolbar';
import Typography from './Typography';


export default function ComponentsOverrides(theme) {
	return merge(
		Accordion(theme),
		AppBar(theme),
		Button(theme),
		Badge(theme),
		Card(theme),
		Checkbox(theme),
		Chip(theme),
		ControlLabel(theme),
		CssBaseLine(theme),
		Divider(theme),
		IconButton(theme),
		InputField(theme),
		InputLabel(theme),
		LinearProgress(),
		Link(),
		ListItemIcon(),
		OutlinedInput(theme),
		Pagination(theme),
		Paper(theme),
		SearchComponent(theme),
		Tab(theme),
		TableCell(theme),
		Tabs(),
		Toolbar(theme),
		ToolTip(theme),
		Typography(),
	);
}
