// ==============================|| OVERRIDES - CHIP ||============================== //

export default function ControlLabel(theme) {
	return {
		MuiFormControlLabel: {
			styleOverrides: {
				label: {
					width: '100%',
					marginLeft: 0,
					// top: '10px',
					fontSize: 'theme.typography.h5.fontSize',
				},
				root: {
					'&:hover': {
						backgroundColor: theme.palette.grey[300],
					},
				},
			},
		},
	};
}
