// material-ui
import { red } from '@ant-design/colors';
import { alpha } from '@mui/material/styles';

// ==============================|| OVERRIDES - SEARCH COMPONENT ||============================== //

export default function SearchComponent(theme) {
	const boxShadowColor = alpha(theme.palette.twilloColor.teal, 0.5);
	return {
		searchComponent: {
			appBase: {
				backgroundColor: 'transparent',
				width: '100%',
				boxShadow: 'none',
			},
			dataSearchTitle: {
				display: 'none',
			},
			dataSearchInputGroup: {
				position: 'relative',
				zIndex: 2,
				borderColor: '#e7e7e7',
				borderSize: '1px',
				boxShadow: '0 0 10px 0 rgba(0, 0, 0, 0.1)',
				padding: '0.9em 1.3em 1em',
				backgroundColor: 'white',
				width: '100%',
				height: '100%',
				borderRadius: '2em',
				fontSize: '1em',
				transition: 'all .3s',
				'&::placeholder': {
					color: '#999999',
					fontStyle: 'italic',
				},
				'&:hover': {
					borderColor: boxShadowColor,
				},
				'&:focus': {
					borderColor: boxShadowColor,
					boxShadow: `0 0 0 3px ${boxShadowColor}`,
				},
			},
			searchInputGroup: {
				boxShadow: 'none',
				backgroundColor: 'transparent',
				borderRadius: '2em',
				'& div': {
					'& div': {
						'& div': {
							zIndex: '3',
							padding: '0',
							gap: 0,
							justifyContent: 'end',
							'& div:has(.oersi-frontend-search-component-input-icon)': {
								width: 'auto',
								maxWidth: '100%',
								'& div': {
									maxWidth: '100%',
								},
							},
							'& div:has(.search-icon)': {
								paddingRight: '10px',
								paddingLeft: '10px',
								'& svg.search-icon-svg': {
									fill: theme.palette.grey[800],
								},
							},
							'& div:has(.clear-icon)': {
								borderRight: '1px solid #efefef',
								paddingRight: '8px',
								'& svg.clear-icon-svg': {
									fill: theme.palette.grey[600],
								},
							},
						},
					},
				},
			},
			dataSearchInputAddon: {
				borderLeftColor: 'transparent !important',
				backgroundColor: 'transparent',
				width: '20%',
				padding: '4px',
				'&:last-of-type': {
					borderRadius: '0 20px 20px 0',
				},
				'@media(max-width: 500px)': {
					width: '40%',
				},
			},
			dataSearchInputIcon: {
				color: theme.palette.grey[700],
				width: '100%',
				height: '100%',
			},
			dataSearchInputIconButton: {
				backgroundColor: '#a4d8d8',
				margin: '0',
				padding: '0',
				width: '12em',
				height: '100%',
				fontSize: '1em',
				borderRadius: '2em',
				borderTopLeftRadius: '0',
				borderBottomLeftRadius: '0',
				minWidth: '10px',
				'&:hover': {
					//backgroundColor: '#c3e5e5'
				},
				'@media(max-width: 700px)': {
					//marginLeft: '-3em',
					width: '5em'
				},
			},
			dataSearchListItem: {
				width: '100%',
				color: 'white',
				fontSize: '1em',
				backgroundColor: theme.palette.grey[800],
				'&:hover': {
					backgroundColor: 'theme.palette.grey[500]',
				},
			},
		},
	};
}
