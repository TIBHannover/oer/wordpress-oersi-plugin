// ==============================|| OVERRIDES - CHECKBOX ||============================== //

export default function Toolbar(theme) {
	return {
		MuiToolbar: {
			overrides: {
				root: {
					backgroundColor: theme.palette.grey[0],
				},
			},
		},
	};
}
