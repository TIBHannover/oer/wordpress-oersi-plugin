// ==============================|| OVERRIDES - PAGINATION ||============================== //

import { alpha } from '@mui/system';

export default function Pagination(theme) {
	return {
		MuiPagination: {
			styleOverrides: {
				root: {
					display: 'flex',
					alignSelf: 'center',
					width: 'auto',
					height: '50px',
					margin: '0 1px',
					padding: '9px 4px',
					border: `1px solid ${theme.palette.grey[200]}`,
					borderTopLeftRadius: '50px',
					borderBottomLeftRadius: '50px',
					backgroundColor: theme.palette.grey[200],
					[theme.breakpoints.down('sm')]: {
						borderTopRightRadius: '50px',
						borderBottomRightRadius: '50px',
						margin: '0',
					},
				},
				ul: {
					display: 'flex',
					justifyContent: 'center',
					alignItems: 'center',
					listStyleType: 'none',
					[theme.breakpoints.down('sm')]: {
						flexWrap: 'nowrap',
					},
					'& li': {
						listStyleType: 'none'
					},
				},
			},
		},
		MuiPaginationItem: {
			styleOverrides: {
				root: {
					color: theme.palette.grey[800],
					fontSize: '1em',
					display: 'flex',
					justifyContent: 'center',
					lineHeight: 'normal',
					[theme.breakpoints.down('sm')]: {
						fontSize: '0.85em',
						minWidth: '28px',
						height: '28px',
						margin: '0 1px',
					},
				},
				page: {
					'&:hover': {
						backgroundColor: theme.palette.grey[700],
						color: theme.palette.grey[100],
					},
					'&:active': {
						backgroundColor: theme.palette.grey[700],
						color: theme.palette.grey[100],
					},
					'&:focus': {
					},
					'&.Mui-selected': {
						backgroundColor: theme.palette.grey[700],
						color: theme.palette.grey[100],

					},

				},
			},
		},
		MuiSelect: {
			styleOverrides: {
				root: {
					height: '50px',
					margin: '0 1px',
					border: `1px solid ${alpha(theme.palette.grey[300], 0.5)}`,
					borderTopRightRadius: '50px',
					borderBottomRightRadius: '50px',
					backgroundColor: alpha(theme.palette.grey[300], 0.5),
					alignSelf: 'center',
					padding: '9px 16px 9px 0',
					fontSize: '1em',
					'& .MuiOutlinedInput-notchedOutline': {
						border: 'none',
						borderRadius: '0px',
					},
					'&:hover .MuiOutlinedInput-notchedOutline': {
						border: '0px',
					},
					'&.Mui-focused .MuiOutlinedInput-notchedOutline': {
						border: '0px',
					},
					'& .MuiSelect-icon': {
						color: theme.palette.grey[800],
						fontSize: '1.3em',
						right: '12px',
					},
					'.MuiSelect-select': {
						paddingRight: '20px!important',
						lineHeight: 'normal',
					},
					[theme.breakpoints.down('sm')]: {
						padding: '9px 30px',
						borderTopLeftRadius: '50px',
						borderBottomLeftRadius: '50px',
					}
				},
			},
		},
		MuiPopover: {
			styleOverrides: {
				paper: {
					boxShadow: '0px 4px 20px rgba(0, 0, 0, 0.1)',
					marginTop: '5px',
					marginLeft: '-1px'
				},
			},
		},
		MuiTypography: {
			styleOverrides: {
				root: {
					'.range-start': {
						fontWeight: 'bold'
					},
					'.range-end': {
						fontWeight: 'bold'
					},
					'.total': {
					},

				},
			},
		},
	};
}
