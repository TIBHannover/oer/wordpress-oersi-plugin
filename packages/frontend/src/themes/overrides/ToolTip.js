// ==============================|| OVERRIDES - TOOLTIP ||============================== //

export default function ToolTip(theme) {
	return {
		MuiTooltip: {
			styleOverrides: {
				tooltip: {
					fontSize: '0.85em',
					fontWeight: 'normal',
					color: 'white',
				},
			},
		},
	};
}
