const Style = {
	base: [
		'color: #fff',
		'background-color: #444',
		'padding: 1px 1px',
		'border-radius: 2px',
	],
	WARNING: ['background-color: #0288d1'],
	SUCCESS: ['background-color: #2e7d32'],
	ERROR: ['background-color: #d22d2d'],
	INFO: ['background-color: blue'],
};

/**
 *
 * @param {string} title  Title of the log example: 'User created' it will be displayed as [ USER CREATED ]
 * @param {string} msg   Message to be displayed in the log example: 'User created successfully'
 * @returns {void} It will log the message in the console
 */
const _log = console.log;
console.log = function () {
	const style = Style.base.join(';') + ';';
	const args = Array.from(arguments);
	args.unshift(`%c[OERSI-LOG] `, style);
	_log.apply(this, args);
};

/**
 *
 * @param {string} title  Title of the log example: 'User created' it will be displayed as [ USER CREATED ]
 * @param {string} msg   Message to be displayed in the log example: 'User created successfully'
 * @returns {void} It will log the message in the console
 */
const _warn = console.warn;
console.warn = function () {
	const style = Style.base.join(';') + ';' + Style.WARNING.join(';');
	const args = Array.from(arguments);
	args.unshift(`%c[OERSI-WARN] `, style);
	_warn.apply(this, args);
};

/**
 *
 * @param {string} title  Title of the log example: 'User created' it will be displayed as [ USER CREATED ]
 * @param {string} msg   Message to be displayed in the log example: 'User created successfully'
 * @returns {void} It will log the message in the console
 */
const _success = console.table;
console.success = function () {
	const style = Style.base.join(';') + ';' + Style.SUCCESS.join(';');
	const args = Array.from(arguments);
	args.unshift(`%c[OERSI-SUCCESS] `, style);
	_success.apply(this, args);
};

/**
 *
 * @param {string} title  Title of the log example: 'User created' it will be displayed as [ USER CREATED ]
 * @param {string} msg   Message to be displayed in the log example: 'User created successfully'
 * @returns {void} It will log the message in the console
 */
const _error = console.error;
console.error = function () {
	const style = Style.base.join(';') + ';' + Style.ERROR.join(';');
	const args = Array.from(arguments);
	args.unshift(`%c[OERSI-ERROR] `, style);
	_error.apply(this, args);
};

/**
 * @param {string} title  Title of the log example: 'User created' it will be displayed as [ USER CREATED ]
 * @param {string} msg   Message to be displayed in the log example: 'User created successfully'
 * @returns {void} It will log the message in the console
 */
const _info = console.info;
console.info = function () {
	const style = Style.base.join(';') + ';' + Style.INFO.join(';');
	const args = Array.from(arguments);
	args.unshift(`%c[OERSI-INFO] `, style);
	_info.apply(this, args);
};
