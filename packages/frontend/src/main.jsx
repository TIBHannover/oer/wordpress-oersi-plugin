/**
 *
 * @package OERSI
 * @license: MIT
 *
 * @author Edmond Kacaj <Edmond.Kacaj@tib.eu>
 *
 * Copyright (c) 2022 TIB <https://www.tib.eu/en>
 */

import React, { Suspense } from 'react';
import ReactDOM from 'react-dom/client';
import ThemeCustomization from '@theme';
import '@utils';
import { CacheProvider } from '@emotion/react';
import createCache from '@emotion/cache';
import Configuration from '@components/Configuration';
import Loading from '@components/Loading';

import './i18-next/i18n';
import { getConfigurationAPI } from '@api';
import loadScript from '@utils/loadScript';
import StyleOverride from '@components/StyleOverride';
const container = document.getElementById('root');

if (import.meta.env.MODE === 'development') {
	loadScript('/js/oer-elastics.js', true);
}

document.addEventListener('DOMContentLoaded', async () => {
	const configuration = await getConfigurationAPI();
	window.OERSI || (window.OERSI = {});

	Object.assign(window.OERSI, configuration);
	if (!container) return;

	const cache = createCache({
		key: 'css',
		prepend: true,
		// container: emotionRoot
	});
	ReactDOM.createRoot(container).render(
		<React.StrictMode>
			<Suspense fallback={<Loading />}>
				<CacheProvider value={cache}>
					<ThemeCustomization>
						{/* <QueryProvider> */}
						{configuration && (
							<>
								<Configuration />
								<StyleOverride />
							</>
						)}
						{/* </QueryProvider> */}
					</ThemeCustomization>
				</CacheProvider>
			</Suspense>
		</React.StrictMode>,
	);
});
