const customQueryLicense = (value, props) => {
	return value instanceof Array
		? {
			query: {
				bool: {
					should: [
						...value.map((v) => ({
							prefix: {
								"license.id": v,
							},
						})),
						...value.map((v) => ({
							prefix: {
								"license.id": v.replace("https:/", "http:/"),
							},
						})),
					],
				},
			},
		}
		: {}
}

function getPrefixAggregationQueryLicense() {
	const prefixList = [
		"https://creativecommons.org/licenses/by/",
		"https://creativecommons.org/licenses/by-sa/",
		"https://creativecommons.org/licenses/by-nd/",
		"https://creativecommons.org/licenses/by-nc-sa/",
		"https://creativecommons.org/licenses/by-nc/",
		"https://creativecommons.org/licenses/by-nc-nd/",
		"https://creativecommons.org/publicdomain/zero/",
		"https://creativecommons.org/publicdomain/mark",
	];
	const fieldName = "license.id";
	var aggsScript = "if (doc['" + fieldName + "'].size()==0) { return null }"
	aggsScript += prefixList.reduce(
		(result, prefix) =>
			result +
			" else if (doc['" +
			fieldName +
			"'].value.startsWith('" +
			prefix +
			"') || doc['" +
			fieldName +
			"'].value.startsWith('" +
			prefix.replace("https:/", "http:/") +
			"')) { return '" +
			prefix +
			"'}",
		""
	)
	aggsScript += " else { return doc['" + fieldName + "'] }"
	return () => ({
		aggs: {
			"license.id": {
				terms: {
					size: 100,
					script: {
						source: aggsScript,
						lang: "painless",
					},
				},
			},
		},
	})
}

// Query function for filter
function createFilteredQuery(...filters) {
	return () => ({
		track_total_hits: true,
		query: {
			bool: {
				must: filters.map(filter => ({
					match: {
						[filter.field]: filter.value
					}
				}))
			}
		}
	});
}

// Global
window.createFilteredQuery = createFilteredQuery;
window.getPrefixAggregationQueryLicense = getPrefixAggregationQueryLicense;
