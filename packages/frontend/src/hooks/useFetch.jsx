import React from 'react';

/**
 * A custom hook that provides the ability to fetch data from a remote source.
 * @param {function} fetchFunction A function that returns a promise that resolves to the data.
 * @returns {object} An object with the following properties:
 * - data: The data returned by the fetch function.
 * - isLoading: A boolean indicating whether the data is loading.
 * - error: A boolean indicating whether an error occurred.
 * - isFetching: A boolean indicating whether the data is loading.
 * - isError: A boolean indicating whether an error occurred.
 * @example const { data, isLoading, error, isFetching, isError } = useFetch(getElasticsSearchAPI);
 * if (isLoading) return <Loading />;
 * if (isError) return <NotFound error={error} />;
 * if (!isLoading && !isError && data) {
 *  return <App data={data} />;
 * }
 */
const useFetch = fetchFunction => {
	const [data, setData] = React.useState(null);
	const [isLoading, setIsLoading] = React.useState(true);
	const [error, setError] = React.useState('');
	const [isFetching, setIsFetching] = React.useState(false);
	const [isError, setIsError] = React.useState(false);
	React.useEffect(() => {
		setIsFetching(true);
		setIsError(false);
		setError(null);
		setData(null);
		setIsLoading(true);
		const fetch = async () => {
			try {
				const response = await fetchFunction();
				setData(response);
			} catch (e) {
				setError(e?.message);
				setIsError(true);
			} finally {
				setIsLoading(false);
				setIsFetching(false);
			}
		};
		fetch();
	}, [fetchFunction]);
	return {
		data,
		isLoading,
		error,
		isFetching,
		isError,
	};
};

export default useFetch;
