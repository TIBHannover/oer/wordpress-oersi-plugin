/**
 *
 * @package OERSI
 * @license: MIT
 *
 * @author Edmond Kacaj <Edmond.Kacaj@tib.eu>
 *
 * Copyright (c) 2022 TIB <https://www.tib.eu/en>
 */

import React from 'react';

/**
 * useAggregation - is a hook that allows to use the aggregation query
 *
 * @param {object} props - Props used to initialize the hook :
 *   @type {string} dataField - The data field to be used for the aggregation example: 'name' (default: '')
 * 	 @type {number} size - The size of the aggregation example: '100' (default: 1000)
 * 	 @type {number} minSearchLength - The minimum length of the search term to trigger the aggregation example: '3' (default: 3)
 * 	 @type {boolean} reloadOnSearch - Reload the aggregation on search example: 'true' (default: true)
 * 	 @type {string} allowedSearchRegex - The regex to be used for the search example: '^[a-zA-Z0-9]*$' (default: null)
 * 	 @type {number} debounce - The debounce time in ms example: '200' (default: 200)
 * 	 @type {string} defaultQuery - The default query   (default: null)
 *
 *
 * @returns {object} 	- Returns an object with the following properties:
 *   @type {string} 	- dataField: The data field used to query the aggregation
 *   @type {string} 	- value: The value used to query the aggregation
 *   @type {string} 	- searchTerm: The search term used to query the aggregation
 *   @type {function}  - setSearchTerm: A function that sets the search term used to query the aggregation
 *   @type {object}    - defaultQuery: The default query value used to query the aggregation
 *   @type {function} 	- setDefaultQuery: A function that sets the default query value used to query the aggregation
 *   @type {function} 	- onUpdateSearchTerm: The default search term used to query the aggregation
 *
 *
 *
 * @example
 *
 * import { useAggregation } from '../hooks/useAggregation';
 * const { dataField, value, searchTerm, setSearchTerm, defaultQuery, setDefaultQuery, onUpdateSearchTerm } = useAggregation(
 * {
 *  dataField: 'name',
 *  size: 100,
 *  minSearchLength: 3,
 *  reloadOnSearch: true,
 *  allowedSearchRegex: null,
 *  debounce: 200,
 *  defaultQuery: null,
 * }
 *
 * */
const useAggregation = params => {
	const {
		dataField,
		debounce,
		minSearchLength,
		reloadOnSearch,
		allowedSearchRegex,
		size,
	} = params;
	const [searchTerm, setSearchTerm] = React.useState('');
	const [defaultQuery, setDefaultQuery] = React.useState(
		typeof params.defaultQuery === 'function'
			? params.defaultQuery()
			: null,
	);

	const onUpdateSearchTerm = term => {
		if (allowedSearchRegex && !term.match(allowedSearchRegex)) {
			return;
		}
		setSearchTerm(term);
	};

	React.useEffect(() => {
		const updateAggsSearchQuery = term => {
			if (!term || term.length < minSearchLength) {
				setDefaultQuery(null);
				return;
			}
			const script =
				"def results = []; " +
				"for (def a : doc['" + dataField + "']) { " +
				"  if (a.toLowerCase(Locale.ROOT).contains('" + term.toLowerCase() + "')) { " +
				"    results.add(a); " +
				"  } " +
				"} " +
				"if (results.size() > 0) { " +
				"  return results; " +
				"} " +
				"return null;";

			const query = {
				aggs: {
					[dataField]: {
						terms: {
							script: { source: script },
							size: size,
							order: { _count: 'desc' },
						},
					},
				},
			};
			setDefaultQuery(query);
		};

		if (!reloadOnSearch) return;

		const timer = setTimeout(
			() => updateAggsSearchQuery(searchTerm),
			debounce,
		);

		return () => {
			clearTimeout(timer);
		};
	}, [
		searchTerm,
		dataField,
		debounce,
		minSearchLength,
		reloadOnSearch,
		size,
	]);

	return {
		dataField,
		searchTerm,
		setSearchTerm,
		defaultQuery,
		setDefaultQuery,
		onUpdateSearchTerm,
	};
};

export default useAggregation;
