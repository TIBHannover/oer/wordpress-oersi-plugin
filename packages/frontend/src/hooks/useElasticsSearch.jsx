/**
 *
 * @package OERSI
 * @license: MIT
 *
 * @author Edmond Kacaj <Edmond.Kacaj@tib.eu>
 *
 * Copyright (c) 2022 TIB <https://www.tib.eu/en>
 */

import { getElasticsSearchAPI } from '@api';
import { useQuery } from '@tanstack/react-query';

/**
 * A Hook that make a request to the elastics search API and returns the elastics search.
 * @returns {Object} - The UseQuery result of the elastics search query.
 * @example
 * const { data, isLoading, isError } = useElasticsSearch();
 **/
function useElasticsSearch() {
	return useQuery(['elasticsSearch'], getElasticsSearchAPI);
}

export default useElasticsSearch;
