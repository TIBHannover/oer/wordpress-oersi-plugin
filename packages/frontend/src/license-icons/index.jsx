/**
 *
 * @package OERSI
 * @license: MIT
 *
 * @author Edmond Kacaj <Edmond.Kacaj@tib.eu>
 *
 * Copyright (c) 2022 TIB <https://www.tib.eu/en>
 */

import LicenseZeroIcon from './LicenseZeroIcon';
import JsonLinkedDataIcon from './JsonLinkedDataIcon';
import LicenseCcByIcon from './LicenseCcByIcon';
import LicenseCcByNcIcon from './LicenseCcByNcIcon';
import LicenseCcByNdIcon from './LicenseCcByNdIcon';
import LicenseCcBySaIcon from './LicenseCcBySaIcon';
import LicenseCcByNcNdIcon from './LicenseCcByNcNdIcon';
import LicenseCcByNcSaIcon from './LicenseCcByNcSaIcon';
import LicenseCcZeroIcon from './LicenseCcZeroIcon';
import LicensePdIcon from './LicensePdIcon';
import LicenseIcon from './LicenseIcon';

export default LicenseIcon;
export {
	LicenseZeroIcon,
	JsonLinkedDataIcon,
	LicenseCcByIcon,
	LicenseCcByNcIcon,
	LicenseCcByNdIcon,
	LicenseCcBySaIcon,
	LicenseCcByNcNdIcon,
	LicenseCcByNcSaIcon,
	LicenseCcZeroIcon,
	LicensePdIcon,
};
