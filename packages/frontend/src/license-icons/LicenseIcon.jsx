/**
 *
 * @package OERSI
 * @license: MIT
 *
 * @author Edmond Kacaj <Edmond.Kacaj@tib.eu>
 *
 * Copyright (c) 2022 TIB <https://www.tib.eu/en>
 */

import React from 'react';
import {
	LicenseCcByIcon,
	LicenseCcByNcIcon,
	LicenseCcByNcNdIcon,
	LicenseCcByNdIcon,
	LicenseCcBySaIcon,
	LicensePdIcon,
	LicenseCcZeroIcon,
	LicenseCcByNcSaIcon,
} from '@license-icons';
import HelpOutline from '@mui/icons-material/HelpOutline';
import PropTypes from 'prop-types';
/**
 * @param {string} licenseGroup - The license group to render. One of: 'by', 'by-nc', 'by-nc-nd', 'by-nc-sa', 'by-nd', 'by-sa', 'pdm', 'zero'
 * @param {Object} props - The props to pass to the LicenseIcon component.
 * @returns {React.Component} - The LicenseIcon component based on the license group.
 **/
const LicenseIcon = ({ licenseGroup, ...props }) => {
	if (!licenseGroup) return null;
	if (licenseGroup === 'by') return <LicenseCcByIcon {...props} />;
	if (licenseGroup === 'by-nc') return <LicenseCcByNcIcon {...props} />;
	if (licenseGroup === 'by-nc-nd') return <LicenseCcByNcNdIcon {...props} />;
	if (licenseGroup === 'by-nc-sa') return <LicenseCcByNcSaIcon {...props} />;
	if (licenseGroup === 'by-nd') return <LicenseCcByNdIcon {...props} />;
	if (licenseGroup === 'by-sa') return <LicenseCcBySaIcon {...props} />;
	if (licenseGroup === 'pdm') return <LicensePdIcon {...props} />;
	if (licenseGroup === 'zero') return <LicenseCcZeroIcon {...props} />;

	return <HelpOutline {...props} />;
};

// LicenseIcon.propTypes = {
// 	licenseGroup: PropTypes.string.optional,
// };
LicenseIcon.defaultProps = {
	licenseGroup: '',
};

export default LicenseIcon;
