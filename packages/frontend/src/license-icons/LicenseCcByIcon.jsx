/**
 *
 * @package OERSI
 * @license: MIT
 *
 * @author Edmond Kacaj <Edmond.Kacaj@tib.eu>
 *
 * Copyright (c) 2022 TIB <https://www.tib.eu/en>
 */

import React from 'react';
import CreativeCommonsIcon from './CreativeCommonsIcon';
import LicenseByIcon from './LicenseByIcon';

/**
 *
 * @param {Object} props - props to be passed to the component
 * @returns  {ReactElement} - React element containing the icon
 */
const LicenseCcByIcon = props => {
	return (
		<React.Fragment>
			<CreativeCommonsIcon sx={{ mr: '.25em' }} {...props} />
			<LicenseByIcon {...props} />
		</React.Fragment>
	);
};

export default LicenseCcByIcon;
