import { defineConfig } from 'vite';
import react from '@vitejs/plugin-react';
import { viteStaticCopy } from 'vite-plugin-static-copy';
import path from 'path';

export default defineConfig({
	plugins: [
		react(),
		viteStaticCopy({
			targets: [
				{ src: 'src/images/**/*', dest: 'images' },
				{ src: 'src/assets/**/*.js', dest: 'js' },
				{ src: 'src/assets/languages/**/*.json', dest: 'languages' },
			],
		}),
	],
	resolve: {
		alias: [
			{ find: '@components', replacement: path.resolve('src/components') },
			{ find: '@theme', replacement: path.resolve('src/themes') },
			{ find: '@utils', replacement: path.resolve('src/utils') },
			{ find: '@hooks', replacement: path.resolve('src/hooks') },
			{ find: '@helpers', replacement: path.resolve('src/helpers') },
			{ find: '@providers', replacement: path.resolve('src/providers') },
			{ find: '@api', replacement: path.resolve('src/api') },
			{ find: '@images', replacement: path.resolve('src/images') },
			{ find: '@license-icons', replacement: path.resolve('src/license-icons') },
			{ find: '@assets', replacement: path.resolve('src/assets') },
		],
	},
	envPrefix: 'OERSI_',
	envDir: '../../',
	build: {
		cssCodeSplit: true,
		rollupOptions: {
			input: {
				'oer-frontend': path.resolve(__dirname, 'src/main.jsx'),
				'oer-elastics': path.resolve(__dirname, 'src/assets/oer-elastics.js')
			},
			output: {
				dir: '../../assets/',
				entryFileNames: 'js/[name].js',  // Separate files for every entry point
				assetFileNames: assetInfo => {
					const name = assetInfo.name || '';
					const ext = name.split('.').pop();
					if (ext === 'css') return 'css/oer-frontend.css';
					else if (/png|jpe?g|svg|gif|tiff|bmp|ico/i.test(ext)) return 'images/[name].[ext]';
					else if (ext === 'js') return 'js/[name].js';
					return '[name].[ext]';
				}
			}
		},
		chunkSizeWarningLimit: 2000,
	},
});
