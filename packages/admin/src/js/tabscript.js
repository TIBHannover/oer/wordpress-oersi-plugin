function initTabs() {
	// Store tabs variables
	const tabs = document.querySelectorAll('ul.nav-tabs > li');

	// Function to switch tabs
	function switchTab(event) {
		event.preventDefault();

		const activeTab = document.querySelector('ul.nav-tabs li.active');
		const activePane = document.querySelector('.tab-pane.active');
		const clickedTab = event.currentTarget;
		const anchor = event.target;
		const activePaneID = anchor.getAttribute('href');

		// Remove active classes
		if (activeTab) {
			activeTab.classList.remove('active');
		}
		if (activePane) {
			activePane.classList.remove('active');
		}

		// Add active classes to the clicked tab and corresponding pane
		clickedTab.classList.add('active');
		document.querySelector(activePaneID).classList.add('active');
	}

	// Add click event listeners to tabs
	tabs.forEach(tab => {
		tab.addEventListener('click', switchTab);
	});
}

function initSmoothScroll() {
	// Smooth scroll for internal links in the documentation
	document.querySelectorAll('a.nav-link').forEach(anchor => {
		anchor.addEventListener('click', function (e) {
			e.preventDefault();
			const target = document.querySelector(this.getAttribute('href'));
			if (target) {
				target.scrollIntoView({
					behavior: 'smooth'
				});
			}
		});
	});
}

// Initialize the tabs and smooth scrolling when the window loads
window.addEventListener('load', function () {
	initTabs();
	initSmoothScroll();
});
