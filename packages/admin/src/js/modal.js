window.addEventListener('load', function () {
	async function copyJsonToClipboard() {
		const jsonElement = document.getElementById('textToCopy');
		if (jsonElement) {
			const jsonToCopy = jsonElement.innerText;

			if (navigator.clipboard) {
				try {
					await navigator.clipboard.writeText(jsonToCopy);
				} catch (err) {
					console.error('Fehler beim Kopieren mit Clipboard API:', err);
				}
			} else {
				const tempInput = document.createElement('textarea');
				tempInput.value = jsonToCopy;
				document.body.appendChild(tempInput);

				tempInput.select();
				try {
					document.execCommand('copy');
				} catch (err) {
					console.error('Fehler beim Kopieren mit execCommand:', err);
				}
				document.body.removeChild(tempInput);
			}
		} else {
			console.error('Element mit ID "textToCopy" wurde nicht gefunden.');
		}
	}

	const triggers = document.querySelectorAll('[data-model-trigger]');
	const dialogs = document.querySelectorAll('dialog');

	triggers.forEach(function (el) {
		el.addEventListener('click', () => {
			const getTarget = el.getAttribute('data-target');
			const target = document.querySelector(`[data-name="${getTarget}"]`);
			if (target.hasAttribute('open')) {
				target.close();
			} else {
				copyJsonToClipboard();
				target.showModal();
			}
		});
	});

	dialogs.forEach(function (el) {
		el.addEventListener('click', ({ target: dialog }) => {
			if (dialog.nodeName === 'DIALOG') {
				dialog.close('dismiss');
			}
		});
	});
});
