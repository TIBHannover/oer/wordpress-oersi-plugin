const path = require('path');
const TerserPlugin = require('terser-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
// eslint-disable-next-line no-unused-vars
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');
const { DefinePlugin } = require('webpack');
require('dotenv').config({ path: path.join(__dirname, '../../.env') });

module.exports = {
	entry: {
		index: './src/index.js',
		editor: './src/js/codeEditor.js'
	},
	output: {
		filename: 'js/oer-[name].js',
		path: path.resolve(__dirname + '../../../assets/')
	},

	mode: 'production',
	// here we tell webpak that third party library (like lodash) to use in different file
	// so the file is not big
	// split in other page onl if the file is ore than 30 kilobyte less file don't seperate
	optimization: {
		splitChunks: {
			chunks: 'async',
			minSize: 20000,
			minRemainingSize: 0,
			minChunks: 1,
			maxAsyncRequests: 30,
			maxInitialRequests: 30,
			enforceSizeThreshold: 50000,
			cacheGroups: {
				defaultVendors: {
					test: /[\\/]node_modules[\\/]/,
					priority: -10,
					reuseExistingChunk: true
				},
				default: {
					minChunks: 2,
					priority: -20,
					reuseExistingChunk: true
				}
			}
		}
	},

	module: {
		rules: [
			// {
			//   // find all photo
			//   test: /\.(png|jpeg|jpg|svg)$/,
			//   use: [
			//     {
			//       loader: "file-loader",
			//       options: {
			//         outputPath: "images/",
			//         name: "[name].[ext]",
			//         publicPath: "../../assets/images/",
			//       },
			//     },
			//   ],
			// },
			{
				// find css loader
				test: /\.css$/,
				use: [
					MiniCssExtractPlugin.loader,
					'css-loader' // css loader is to load file css
				]
			},
			{
				// find sas loader
				test: /\.scss$/,
				use: [
					MiniCssExtractPlugin.loader,
					'css-loader',
					{
						loader: 'postcss-loader',
						options: {
							postcssOptions: {
								plugins: [require('autoprefixer')]
							}
						}
					},
					'sass-loader'
				]
			},
			{
				// find sas loader
				test: /\.js$/,
				exclude: /node_modules/,
				use: {
					loader: 'babel-loader',
					options: {
						presets: ['@babel/env'],
						plugins: ['@babel/plugin-transform-class-properties']
						// filename: "js/[name].oer-[hash].js",
					}
				}
			}
			// {
			//   test: /.(ttf|otf|eot|svg|woff(2)?)(\?[a-z0-9]+)?$/,
			//   use: [
			//     {
			//       loader: "file-loader",
			//       options: {
			//         name: "[name].[ext]",
			//         outputPath: "fonts/",
			//       },
			//     },
			//   ],
			// },
		]
	},
	plugins: [
		// get all process.env.OERSI_*
		new DefinePlugin({
			'process.env': Object.keys(process.env)
				.filter(key => key.startsWith('OERSI_'))
				.reduce((obj, key) => {
					obj[key] = JSON.stringify(process.env[key]);
					return obj;
				}, {})
		}),
		new TerserPlugin(),
		new MiniCssExtractPlugin({
			filename: 'css/oer-[name].css'
		}),
		// new CleanWebpackPlugin({
		//   root: process.cwd(),
		//   verbose: true,
		//   dry: false,
		//   // clean all except images folder
		//   cleanOnceBeforeBuildPatterns: ["**/*"],
		// }),
		new CopyPlugin({
			patterns: [{ from: './src/images', to: 'images' }]
		})
	]
};
