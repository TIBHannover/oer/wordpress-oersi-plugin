<?php

/**
 * Created on Fri Jul 08 2022
 *
 * @package OERSI
 * @license: MIT
 *
 * @author Edmond Kacaj <Edmond.Kacaj@tib.eu>
 *
 * Copyright (c) 2022 TIB <https://www.tib.eu/en>
 */

namespace Inc\DefaultData;

final class Translation
{

    public static $translations = [
        "en" => [
            "translations" => [
                "N/A"                 => "Not defined",
                "INPUT_PLACEHOLDER"   => "Search by {{what}} ",
                "CLEAR_ALL"           => "Clear all",
                "SHOW_RESULT_STATS"   => "{{total}} results found",
                "PAGE_SIZE_SELECTION" => "{{size}} / page",
                "SHOW_TOTAL_OF"       => "of",
                "SHOW_TOTAL_RESULTS"  => "items",
                "SHOW_TOTAL"          => "{{rangeStart}}-{{rangeEnd}} of {{total}} items",
                "SORT_BY_COUNTER"     => "Sort by Count {{what}}",
                "SORT_BY_LABEL"       => "Sort by label {{what}}",
                "NOT_FOUND_TITLE"     => "Sorry, page not found!",
                "NOT_FOUND_BODY"      => "Sorry, we couldn’t find the page you’re looking for. Perhaps you’ve mistyped the URL? Be sure to check your spelling.",
                "NOT_FOUND_LINK"      => "go to plugin",
                "ABOUT"               => "Subject",
                "LICENSE"             => "License",
                "RESOURCE_TYPE"       => "Material type",
                "AUTHOR"              => "Author",
                "ORGANISATION"        => "Source",
                "LANGUAGE"            => "Languages",
                "PROVIDER"            => "Provider",
                "SEARCH"              => "Search",
                "SEARCH_LABEL"        => "Search",
                "SEARCH_PLACEHOLDER"  => "Search for OER ...",
                "SEARCH_BUTTON_TEXT"  => "Search",
                "CLEAR_SEARCH"        => "Clear search",
                "SHOW_MORE_TEXT"      => "Show more",
                "SHOW_LESS_TEXT"      => "Show less",
                "LIST_VIEW"           => "List View",
                "CARD_VIEW"           => "Card View"
            ],
            "cards"        => [
                "SUBJECT"        => "Subject :",
                "MATERIAL_TYPE"  => " Material type :",
                "DESCRIPTION"    => "Description",
                "NAME"           => "Name",
                "LICENSE"        => "License :",
                "AUTHOR"         => "Author :",
                "DATE_CREATED"   => "Date Created :",
                "DATE_PUBLISHED" => "Publishing year :",
                "KEYWORDS"       => "Keywords :",
                "DETAILS"        => "To Material",
            ],
        ],
        "de" => [
            "translations" => [
                "N/A"                 => "Nicht definiert",
                "INPUT_PLACEHOLDER"   => "Suche bei {{what}}",
                "SORT_BY_COUNTER"     => "Sortieren nach nummer {{what}}",
                "SORT_BY_LABEL"       => "Sortieren nach label {{what}}",
                "CLEAR_ALL"           => "Filter zurücksetzen",
                "SHOW_RESULT_STATS"   => "{{total}} Ergebnisse",
                "PAGE_SIZE_SELECTION" => "{{size}} / Seite",
                "SHOW_TOTAL_OF"       => "von",
                "SHOW_TOTAL_RESULTS"  => "Einträgen",
                "SHOW_TOTAL"          => "{{rangeStart}}-{{rangeEnd}} von {{total}} Einträgen",
                "NOT_FOUND_TITLE"     => "Seite nicht gefunden!",
                "NOT_FOUND_BODY"      => "Leider konnten wir die von Ihnen gesuchte Seite nicht finden. Vielleicht haben Sie sich bei der URL vertippt? Überprüfen Sie bitte Ihre Rechtschreibung.",
                "NOT_FOUND_LINK"      => "Zur Startseite",
                "ABOUT"               => "Fach",
                "LICENSE"             => "Lizenz",
                "RESOURCE_TYPE"       => "Materialart",
                "AUTHOR"              => "Autor:in",
                "ORGANISATION"        => "Herkunft",
                "LANGUAGE"            => "Sprache",
                "PROVIDER"            => "Quelle",
                "SEARCH"              => "Suchen",
                "SEARCH_LABEL"        => "Suchen",
                "SEARCH_PLACEHOLDER"  => "OER finden...",
                "SEARCH_BUTTON_TEXT"  => "Suche",
                "CLEAR_SEARCH"        => "Suche zurücksetzen",
                "SHOW_MORE_TEXT"      => "Mehr zeigen",
                "SHOW_LESS_TEXT"      => "Weniger zeigen",
                "LIST_VIEW"           => "Listenansicht",
                "CARD_VIEW"           => "Kachelansicht"
            ],
            "cards"        => [
                "SUBJECT"        => "Fach :",
                "NAME"           => "Name",
                "DESCRIPTION"    => "Bezeichnung",
                "MATERIAL_TYPE"  => " Materialart :",
                "LICENSE"        => "Lizenz :",
                "AUTHOR"         => "Autor:in :",
                "DATE_CREATED"   => "Datum erstellt :",
                "DATE_PUBLISHED" => "Erscheinungsjahr :",
                "KEYWORDS"       => "Schlüsselwörter :",
                "DETAILS"        => "Zum Material",
            ],
        ],
    ];
}
