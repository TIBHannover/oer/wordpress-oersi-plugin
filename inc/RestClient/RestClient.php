<?php

/**
 * Created on Fri August 04 2023
 *
 * @package OERSI
 * @license: MIT
 *
 * @author Edmond Kacaj <Edmond.Kacaj@tib.eu>
 *
 * Copyright (c) 2023 TIB <https://www.tib.eu/en>
 */

namespace Inc\RestClient;

use GuzzleHttp\Client as HttpClient;
use GuzzleHttp\Exception\ClientException;
use InvalidArgumentException;
use Psr\Http\Message\ResponseInterface;

abstract class RestClient implements RestClientInterface
{

    /**
     * HttpClient instance
     * @var HttpClient
     */
    protected $httpClient;

    /**
     * Here's an example of creating a client using a base_uri and an array of
     * default request options to apply to each request:
     *
     * ```
     * $client = new Client([
     *    'base_uri'        => 'http://www.foo.com/1.0/',
     *    'timeout'         => 0,   (optional)
     *    'allow_redirects' => false,   (optional)
     *    'proxy'           => '192.168.16.1:10'   (optional)
     *     ]);
     * ```
     * @return array client Configurations.
     */
    // phpcs:ignore Squiz.WhiteSpace.FunctionSpacing.Before
    abstract protected function getClientConfig(): array;


    /**
     * Get additional headers for the API call.
     *
     * @return array
     */
    // phpcs:ignore Squiz.WhiteSpace.FunctionSpacing.Before
    protected function getHeaders()
    {
        return [
            'Accept'       => 'application/json',
            'Content-Type' => 'application/json',
            'origin'       => get_site_url(),
        ];
    }


    /**
     * Construct the RestClient.
     */
    public function __construct()
    {
        $this->setup();
    }


    protected function setup()
    {
        $config = $this->getClientConfig();
        if (empty($config['base_uri'])) {
            throw new InvalidArgumentException("Base URL must be provided.");
        }

        $this->httpClient = new HttpClient($config);
    }


    /**
     * {@inheritdoc}
     */
    public function get($path, $queryParams = []): ResponseInterface
    {

        try {
            $headers = $this->getHeaders();

            return $this->httpClient->get($path, [
                'query'   => $queryParams,
                'headers' => $headers,
            ]);
        } catch (ClientException $e) {
            $this->logError($e->getMessage());
            return null;
        }
    }


    /**
     * {@inheritdoc}
     */
    public function post($path, $bodyArg = [], $queryParams = []): ResponseInterface
    {
        try {
            $headers = $this->getHeaders();

            return $this->httpClient->post($path, [
                'query'   => $queryParams,
                'headers' => $headers,
                'json'    => $bodyArg,
            ]);
        } catch (ClientException $e) {
            $this->logError($e->getMessage());
            return null;
        }
    }


    /**
     * {@inheritdoc}
     */
    public function put($path, $bodyArg = [], $queryParams = []): ResponseInterface
    {
        try {
            $headers = $this->getHeaders();

            return $this->httpClient->put($path, [
                'query'   => $queryParams,
                'headers' => $headers,
                'json'    => $bodyArg,
            ]);
        } catch (ClientException $e) {
            $this->logError($e->getMessage());
            return null;
        }
    }


    /**
     * {@inheritdoc}
     */
    public function delete($path, $queryParams = []): ResponseInterface
    {
        try {
            $headers = $this->getHeaders();

            return $this->httpClient->delete($path, [
                'query'   => $queryParams,
                'headers' => $headers,
            ]);
        } catch (ClientException $e) {
            $this->logError($e->getMessage());
            return null;
        }
    }


    /**
     * {@inheritdoc}
     */
    public function patch($path, $bodyArg = [], $queryParams = []): ResponseInterface
    {
        try {
            $headers = $this->getHeaders();

            return $this->httpClient->patch($path, [
                'query'   => $queryParams,
                'headers' => $headers,
                'json'    => $bodyArg,
            ]);
        } catch (ClientException $e) {
            $this->logError($e->getMessage());
            return null;
        }
    }


    /**
     * {@inheritdoc}
     */
    public function options($path, $queryParams = []): ResponseInterface
    {
        try {
            $headers = $this->getHeaders();

            return $this->httpClient->options($path, [
                'query'   => $queryParams,
                'headers' => $headers,
            ]);
        } catch (ClientException $e) {
            $this->logError($e->getMessage());
            return null;
        }
    }


    /**
     * {@inheritdoc}
     */
    public function head($path, $queryParams = []): ResponseInterface
    {
        try {
            $headers = $this->getHeaders();

            return $this->httpClient->head($path, [
                'query'   => $queryParams,
                'headers' => $headers,
            ]);
        } catch (ClientException $e) {
            $this->logError($e->getMessage());
            return null;
        }
    }


    private function logError($message)
    {
        error_log('Error initializing RestClient: '.$message, 3, WP_CONTENT_DIR.'/rest_client_error.log');
    }


}
