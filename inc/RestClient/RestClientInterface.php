<?php

/**
 * Created on Fri August 04 2023
 *
 * @package OERSI
 * @license: MIT
 *
 * @author Edmond Kacaj <Edmond.Kacaj@tib.eu>
 *
 * Copyright (c) 2023 TIB <https://www.tib.eu/en>
 */

namespace Inc\RestClient;

use Psr\Http\Message\ResponseInterface;

/**
 * Interface for a REST API client.
 */
interface RestClientInterface
{


    /**
     * Send a GET request to the specified API endpoint.
     *
     * @param string $path        The API endpoint path.
     * @param array  $queryParams Optional. Query parameters to include in the request.
     * @return \Psr\Http\Message\ResponseInterface The response data from the API.
     */
    public function get($path, $queryParams = []):ResponseInterface;


    /**
     * Send a POST request to the specified API endpoint.
     *
     * @param string $path        The API endpoint path.
     * @param array  $bodyArg     Optional. The request body data.
     * @param array  $queryParams Optional. Query parameters to include in the request.
     * @return \Psr\Http\Message\ResponseInterface The response data from the API.
     */
    public function post($path, $bodyArg = [], $queryParams = []):ResponseInterface;


    /**
     * Send a PUT request to the specified API endpoint.
     *
     * @param string $path        The API endpoint path.
     * @param array  $bodyArg     Optional. The request body data.
     * @param array  $queryParams Optional. Query parameters to include in the request.
     * @return \Psr\Http\Message\ResponseInterface The response data from the API.
     */
    public function put($path, $bodyArg = [], $queryParams = []):ResponseInterface;


    /**
     * Send a DELETE request to the specified API endpoint.
     *
     * @param string $path        The API endpoint path.
     * @param array  $queryParams Optional. Query parameters to include in the request.
     * @return \Psr\Http\Message\ResponseInterface The response data from the API.
     */
    public function delete($path, $queryParams = []):ResponseInterface;


    /**
     * Send a PATCH request to the specified API endpoint.
     *
     * @param string $path        The API endpoint path.
     * @param array  $bodyArg     Optional. The request body data.
     * @param array  $queryParams Optional. Query parameters to include in the request.
     * @return \Psr\Http\Message\ResponseInterface The response data from the API.
     */
    public function patch($path, $bodyArg = [], $queryParams = []):ResponseInterface;


    /**
     * Send a HEAD request to the specified API endpoint.
     *
     * @param string $path        The API endpoint path.
     * @param array  $queryParams Optional. Query parameters to include in the request.
     * @return \Psr\Http\Message\ResponseInterface The response headers from the API.
     */
    public function head($path, $queryParams = []):ResponseInterface;


    /**
     * Send an OPTIONS request to the specified API endpoint.
     *
     * @param string $path        The API endpoint path.
     * @param array  $queryParams Optional. Query parameters to include in the request.
     * @return \Psr\Http\Message\ResponseInterface The response data from the API.
     */
    public function options($path, $queryParams = []):ResponseInterface;


}
