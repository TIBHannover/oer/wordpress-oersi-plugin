<?php
/**
 * Created on Fri Jul 08 2022
 *
 * @package OERSI
 * @license: MIT
 *
 * @author Edmond Kacaj <Edmond.Kacaj@tib.eu>
 *
 * Copyright (c) 2022 TIB <https://www.tib.eu/en>
 */


namespace Inc;

final class Init
{


    /**
     * Store all the classes inside an array
     * @return array Full list of classes
     */
    public static function get_services()
    {
        return [
            Base\MetaLinks::class,
            Pages\Configuration\Configuration::class,
            Pages\Configuration\ConfigurationAPI::class,
            Pages\ElasticsSearch\ElasticsSearchConfig::class,
            Pages\ElasticsSearch\ElasticsSearchRestAPI::class,
            Base\EnqueueCustomCss::class,
            Pages\CssOverride\CssOverride::class,
            Pages\CssOverride\CssOverrideRestAPI::class,
            Pages\Translation\TranslationConfig::class,
            Pages\Translation\TranslationRestAPI::class,
            Pages\Vocabularies\VocabulariesAPI::class,
            Pages\Vocabularies\VocabulariesAPIParent::class,
            Base\EnqueueCss::class,
            Base\EnqueueJs::class,
            Base\SettingsLinks::class,
            Pages\ShortCode::class,
        ];
    }//end get_services()


    /**
     * Loop through the classes, initialize them,
     * and call the register() method if it exists
     * @return
     */
    public static function register_oersi_plugin()
    {
        foreach (self::get_services() as $class) {
            $service = self::instantiate($class);
            if (method_exists($service, 'register')) {
                $service->register();
            }
        }
    }//end register_oersi_plugin()


    /**
     * Initialize the class
     * @param  class $class    class from the services array
     * @return class instance  new instance of the class
     */
    private static function instantiate($class)
    {
        $service = new $class();

        return $service;
    }//end instantiate()


}//end class
