<?php

/**
 * Created on Fri Jul 08 2022
 *
 * @package OERSI
 * @license: MIT
 *
 * @author Edmond Kacaj <Edmond.Kacaj@tib.eu>
 *
 * Copyright (c) 2022 TIB <https://www.tib.eu/en>
 */

namespace Inc\Api;

class RestAPI extends \WP_REST_Controller
{

    /**
     * A variable to store the api namespace
     * @var string
     */
    private $apiNamespace = "";

    /**
     * A variable to store base route
     * @var string
     */
    public $baseRoute = '';

    /**
     * A variable to store the api routes
     * @var array
     */
    public $apiRoutes = [];


    /**
     * A function that is used to register the admin pages,subPages,settings,sections and fields
     *  This function is called from the register function in the init class
     * @return void
     */
    public function register()
    {
        $this->apiNamespace = ($_ENV['OERSI_API_NAMESPACE'] ?? 'oersi/v1');
        if (!empty($this->baseRoute) || !empty($this->apiRoutes)) {
            add_action('rest_api_init', [$this, 'registerRoute']);
        }
    } //end register()


    /**
     * A function that set api routes
     * @return $this
     */
    public function setApiRoutes(array $routes)
    {
        $this->apiRoutes = $routes;

        return $this;
    } //end setApiRoutes()


    /**
     * A function that set base route
     * @return $this
     */
    public function setBaseRoute(string $route)
    {
        $this->baseRoute = $route;

        return $this;
    } //end setBaseRoute()


    /**
     * A function that register the api routes
     * @return void
     */
    public function registerRoute()
    {
        foreach ($this->apiRoutes as $route) {
            register_rest_route($this->apiNamespace, isset($route['endpoint']) ? $route['endpoint'] : '', [
                'methods'             => ($route['methods'] ?? \WP_REST_Server::READABLE),
                'callback'            => isset($route['callback']) ? $route['callback'] : "",
                'permission_callback' => isset($route['permission_callback']) ? $route['permission_callback'] : (($route['methods'] == \WP_REST_Server::READABLE ) ? [$this, 'get_permissions_check'] : [$this, 'set_permissions_check']),
                'args'                => isset($route['args']) ? $route['args'] : \WP_REST_Controller::get_endpoint_args_for_item_schema(\WP_REST_Server::READABLE),
            ]);
        }
    } //end registerRoute()


    /**
     * Check permissions for the read.
     *
     * @param WP_REST_Request $request get data from request.
     *
     * @return bool|WP_Error
     */
    public function set_permissions_check()
    {
        if ($_ENV['OERSI_API_PERMISSIONS'] == 'true') {
            return true;
        }

        if (!current_user_can('manage_options')) {
            return new \WP_Error('rest_forbidden', __('Only administrators can access the REST API.'), ['status' => 403]);
        }

        return true;
    } //end permissionCheck()


    /**
     * Check permissions for the read.
     *
     * @param WP_REST_Request $request get data from request.
     *
     * @return bool|WP_Error
     */
    public function get_permissions_check()
    {
        if ($_ENV['OERSI_API_PERMISSIONS'] == 'true') {
            return true;
        }
        if (!current_user_can('read')) {
            return new \WP_Error('rest_forbidden', __('Please Login to access this resource', "oersi-domain"), ['status' => 403]);
        }
        return true;
    }


}//end class
