<?php

/**
 * Created on Fri August 04 2023
 *
 * @package OERSI
 * @license: MIT
 *
 * @author Edmond Kacaj <Edmond.Kacaj@tib.eu>
 *
 * Copyright (c) 2023 TIB <https://www.tib.eu/en>
 */

namespace Inc\Pages\EduSharing;

use Inc\RestClient\RestClient;
use InvalidArgumentException;

class EduSharingRestClient extends RestClient
{

    private $baseUrl;


    public function __construct($eduSharingUrl)
    {
        if (empty($eduSharingUrl)) {
            throw new InvalidArgumentException("Base URL must be provided.");
        }
        $this->baseUrl = $eduSharingUrl;
        parent::__construct();
    }


    /**
     * {@inheritdoc}
     */
    protected function getClientConfig(): array
    {

        return [
            'base_uri' => $this->baseUrl,
        ];
    }


    protected function getHeaders()
    {
        return [
            'Accept'       => 'application/json',
            'Content-Type' => 'application/json',
            'origin'       => get_site_url(),
        ];
    }


}
