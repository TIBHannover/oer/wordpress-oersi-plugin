<?php

/**
 * Created on Thu Jul 07 2022
 *
 * @package  OERSI
 * @license: MIT
 *
 * @author Edmond Kacaj <Edmond.Kacaj@tib.eu>
 *
 * Copyright (c) 2022 TIB <https://www.tib.eu/en>
 */


namespace Inc\Pages\ElasticsSearch;

use Inc\Pages\ElasticsSearch\ElasticsSearchConfigCallbacks;
use Inc\Api\SettingsApi;
use Inc\Base\Utils;

/**
 *
 */
class ElasticsSearchConfig extends ElasticsSearchConfigCallbacks
{

    /**
     * A property to store the settings api class
     *
     * @var SettingsApi
     */
    public $settings;

    /**
     * A property to store an array of all the pages we want to add to the wordpress admin menu
     *
     * @var array
     */
    public $subPages = [];


    /**
     *  A function that is called when we register it in the init method
     *  Register contain all the methods that we want to call when we register the plugin
     *
     * @return void
     */
    public function register()
    {
        $this->settings = new SettingsApi();

        $this->setSubPages();
        $this->setSettings();
        $this->setSections();
        $this->setFields();

        $this->settings->addSubPages($this->subPages)->register();
    } //end register()


    /**
     *  A function that we can add all the sub pages we want to add to the wordpress admin menu
     *  it will assign the sub pages to the $this->subpages array
     *
     * @return void
     */
    public function setSubPages()
    {
        $this->subPages = [
            [
                'parent_slug' => 'oersi_plugin',
                'page_title'  => __('ElasticsSearch', 'oersi-domain'),
                'menu_title'  => __('ElasticsSearch', 'oersi-domain'),
                'capability'  => 'manage_options',
                'menu_slug'   => 'oersi_plugin_elastics_search',
                'callback'    => [
                    $this,
                    'template',
                ],
            ],
        ];
    } //end setSubPages()


    /**
     *  A function that registers all the settings we want to add to the wordpress admin menu
     *  it will assign the settings to the $this->settings
     *
     * @return void
     */
    public function setSettings()
    {
        $args = [
            [
                'option_group' => self::$optionGroup,
                'option_name'  => self::$optionName,
                'callback'     => [
                    $this,
                    'sanitizeInputs',
                ],
            ],
        ];

        $this->settings->setSettings($args);
    } //end setSettings()


    /**
     *  Part of the Settings API. this  define all the sections we want to add  at the top of the settings section before the actual fields.
     *
     *  it will assign the sections to the $this->sections
     *
     * @return void
     */
    public function setSections()
    {
        $args = [
            [
                'id'       => self::$sectionId,
                'title'    => __('Elasticserach Configuration', 'oersi-domain'),
                'callback' => [
                    $this,
                    'sectionManager',
                ],
                'page'     => self::$optionName,
            ],
        ];

        $this->settings->setSections($args);
    } //end setSections()


    /**
     *  Part of the Settings API. this  define all the fields we want to add to the settings section.
     *  it will assign the fields to the $this->fields
     *
     * @return void
     */
    public function setFields()
    {
        // Sichere Initialisierung der Optionswerte
        $optionValues = get_option(self::$optionName);
        if ($optionValues === false) {
            $optionValues = array(
                'url' => '',
                'app_name' => '',
                'credentials' => '',
                'providerName' => '',
                'fields' => ''
            );
        }

        $args = [];
        foreach ($this->fields() as $key) {
            if (Utils::are_conditions_met(isset($key['conditions']) ? $key['conditions'] : null, $optionValues)) {
                $args[] = [
                    'id'       => $key['name'],
                    'title'    => $key['title'],
                    'callback' => [
                        $this,
                        'render',
                    ],
                    'page'     => self::$optionName,
                    'section'  => self::$sectionId,
                    'args'     => [
                        'label_for'    => $key['name'],
                        'class'        => isset($key['class']) ? $key['class'] : '',
                        'placeholder'  => isset($key['placeholder']) ? $key['placeholder'] : '',
                        'option_name'  => self::$optionName,
                        'type'         => isset($key['type']) ? $key['type'] : 'text',
                        'data-tooltip' => isset($key['data-tooltip']) ? $key['data-tooltip'] : '',
                    ],
                ];
            } else {
                unset($optionValues[$key['name']]);
                update_option(self::$optionName, $optionValues, true);
            }//end if
        }//end foreach

        $this->settings->setFields($args);
    } //end setFields()


}//end class
