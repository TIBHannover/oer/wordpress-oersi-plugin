<?php

/**
 * Created on Thu Jul 07 2022
 *
 * @package  OERSI
 * @license: MIT
 *
 * @author Edmond Kacaj <Edmond.Kacaj@tib.eu>
 *
 * Copyright (c) 2022 TIB <https://www.tib.eu/en>
 */


namespace Inc\Pages\ElasticsSearch;

use Inc\Api\RestAPI;
use Inc\Pages\ElasticsSearch\ElasticsSearchConfigCallbacks;

/**
 *
 */
class ElasticsSearchRestAPI extends ElasticsSearchConfigCallbacks
{

    /**
     * A property to store the rest api class
     *
     * @var restAPI
     */
    public $restAPI;

    /**
     * A variable to store base route
     * @var string
     */
    public $baseRoute = '';

    /**
     * A variable to store the api routes
     * @var array
     */
    public $apiRoutes = [];


    /**
     *  A function that is called when we register it in the init method
     *  Register contain all the methods that we want to call when we register the plugin
     *
     * @return void
     */
    public function register()
    {
        $this->restAPI = new RestAPI();
        $this->setApiRoutes();
        $this->restAPI->setApiRoutes($this->apiRoutes)->register();
    } //end register()


    /**
     *  A function that we can add all the methods we want to add to the wordpress rest api
     *  it will assign the pages to the $this->apiRoutes array
     *
     * @return void
     */


    public function setApiRoutes()
    {
        $this->apiRoutes = [
            [
                'endpoint'            => 'elasticssearch',
                'methods'             => "GET",
                'permission_callback' => "__return_true",
                'callback'            => [
                    $this,
                    'getElasticsSearch',
                ],
            ],
        ];
    } //end setApiRoutes()


    /**
     *  A callback function that we can use to get the elasticssearch config
     * it will return the elasticssearch config
     * @return array
     */
    public function getElasticsSearch()
    {
        $elasticssearch = get_option(self::$optionName);
        if (empty($elasticssearch)) {
            return new \WP_Error('rest_not_found', __('No ElasticsSearch found', 'oersi-domain'), ['status' => 404]);
        }
        return $elasticssearch;
    }


}//end class
