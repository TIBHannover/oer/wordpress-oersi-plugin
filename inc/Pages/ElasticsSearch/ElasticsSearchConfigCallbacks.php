<?php
/**
 * Created on Thu Jul 07 2022
 *
 * @package  OERSI
 * @license: MIT
 *
 * @author Edmond Kacaj <Edmond.Kacaj@tib.eu>
 *
 * Copyright (c) 2022 TIB <https://www.tib.eu/en>
 */

namespace Inc\Pages\ElasticsSearch;

use Inc\Api\AbstractCallback;

class ElasticsSearchConfigCallbacks extends AbstractCallback
{

    /**
     * Declare the option group, for setSettings() method
     *
     * @var string
     */
    public static $optionGroup = 'oersi_plugin_elasticsSearch';

    /**
     * Declare the option name, for setSettings() method
     *
     * @var string
     */
    public static $optionName = 'oersi_plugin_elasticsSearch';

    /**
     * Declare the Id for setSettings() method
     *
     * @var string
     */
    public static $sectionId = 'oersi_elasticsSearch_index';



    /**
     *  Fields to be generated in the settings page, this function is called in the register method.
     *
     * @since  1.0.0
     * @return array
     */


    public function fields()
    {
        return [
            [
                'name'         => 'url',
                'title'        => __('URL', 'oersi-domain'),
                'class'        => 'oersi-ui-elastics_app_url',
                'placeholder'  => __('e.g. http://localhost:9200', 'oersi-domain'),
                'data-tooltip' => __('URL where the Elasticsearch cluster is hosted (see Documentation).', 'oersi-domain'),
                'type'         => 'text',
            ],
            [
                'name'         => 'app_name',
                'title'        => __('App name', 'oersi-domain'),
                'class'        => 'oersi-ui-elastics_app_name',
                'placeholder'  => __('e.g. oer_data', 'oersi-domain'),
                'data-tooltip' => __('Refers to an index if you\'re using your own Elasticsearch cluster (see Documentation).', 'oersi-domain'),
                'type'         => 'text',

            ],
            [
                'name'         => 'credentials',
                'title'        => __('Credentials (Optional)', 'oersi-domain'),
                'placeholder'  => __('e.g. TOKEN', 'oersi-domain'),
                'class'        => 'oersi-ui-elastics_app_crdentials',
                'data-tooltip' => __('Insert the string format "username:password" (see Documentation).', 'oersi-domain'),
                'type'         => 'text',
            ],
            [
                'name'         => 'providerName',
                'id'           => 'providerName',
                'title'        => __('Provider Name (Optional)', 'oersi-domain'),
                'class'        => 'oersi-ui-text-providerName',
                'data-tooltip' => __('Add the provider name, e.g. twillo (see Documentation).', 'oersi-domain'),
                'placeholder'  => 'e.g. twillo',
                'type'         => 'text',
            ],
            [
                'name'         => 'providerImageLink',
                'id'           => 'providerImageLink',
                'title'        => __('Provider Image Link (Optional)', 'oersi-domain'),
                'class'        => 'oersi-ui-text-field-providerImageLink',
                'data-tooltip' => __('It accept a provider image link e.g.: https://www.example.com/image.png  (see Documentation).', 'oersi-domain'),
                'type'         => 'text',
                'placeholder'  => 'e.g. https://www.example.com/image.png',
                'conditions'   => [
                    'relation' => 'and',
                    'terms'    => [
                        [
                            'name'     => 'providerName',
                            'operator' => '!=',
                            'value'    => '',
                        ],
                    ],
                ],
            ],
            [
                'name'         => 'fields',
                'title'        => __('Component', 'oersi-domain'),
                'class'        => 'oersi-ui-elastics_app_fields',
                'placeholder'  => __('e.g. {}', 'oersi-domain'),
                'data-tooltip' => __('Insert an array of objects (see Documentation).', 'oersi-domain'),
                'type'         => 'textArea',
            ],
        ];
    }//end fields()


    /**
     *  Return the Html template, this function is called as a callback in the setPage function.
     *
     * @since  1.0.0
     * @return void
     */
    public function template()
    {
        return include_once "$this->pluginPath/templates/elasticSearch/elasticsSearch.php";
    }//end template()


    /**
     *  function is called as a callback in setSection function.
     *
     * @since  1.0.0
     * @return void
     */
    public function sectionManager()
    {
        echo '<p style="max-width: 65ch; margin-bottom:2.5em;">' . __('Manage the sections and fields of the Elasticsearch index. After making any changes, please refresh your browser to see the updates.', 'oersi-domain') . '</p>';
    }//end sectionManager()


    /**
     * This method with deliver the input based on type of input.
     *
     * @since  1.0.0
     *
     * @param  string $args contains input arguments
     * @return void
     */
    public function render($args)
    {
        $type = ($args['type'] ?? 'text');
        if ($type == 'text') {
            $this->textField($args);
        } elseif ($type == 'textArea') {
            $this->textAreaField($args);
        }
    }//end render()


    /**
     *  This method will generate a text field. and will be called as a callback in the renderInputs method.
     *
     * @since  1.0.0
     *
     * @param  array $args contains input arguments
     * @return void
     */
    public function textField($args)
    {
        $name = $args['label_for'];
        $classes = ($args['class'] ?? $name).'-ui-text-field';
        $placeholder = ($args['placeholder'] ?? '');
        $id = ($args['id'] ?? $name).'-ui-text-field-id';
        $optionName = $args['option_name'];
        $value = get_option($optionName);
        echo '<input type="text" name="'.$optionName.'['.$name.']"'.' class="'.$classes.'" size="100" placeholder="'.$placeholder.'" value="'.(isset($value[$name]) ? $value[$name] : '').'" id="'.$id.'" />';
    }//end textField()


    /**
     *  This method will generate a textarea field. and will be called as a callback in the renderInputs method.
     *
     * @since  1.0.0
     *
     * @param  array $args contains input arguments
     * @return void
     */
    public function textAreaField($args)
    {
        $name = $args['label_for'];
        $classes = ($args['class'] ?? $name).'-ui-text-area';
        $id = ($args['id'] ?? $name).'-ui-text-area-id';
        $optionName = $args['option_name'];
        $value = get_option($optionName);
        $renderHtml = '<textarea id="elastics_search" style="display: none !important;" hidden name="'.$optionName.'['.$name.']"'.' id="'.$id.'" class="'.$classes.'" />'.(isset($value[$name]) ? $value[$name] : '').'</textarea>';
        echo $renderHtml;
    }//end textAreaField()


}//end class
