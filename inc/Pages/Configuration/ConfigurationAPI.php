<?php

/**
 * Created on Thu Jul 07 2022
 *
 * @package  OERSI
 * @license: MIT
 *
 * @author Edmond Kacaj <Edmond.Kacaj@tib.eu>
 *
 * Copyright (c) 2022 TIB <https://www.tib.eu/en>
 */


namespace Inc\Pages\Configuration;

use Inc\Api\RestAPI;
use Inc\Base\BaseController;
use Inc\Base\ExternalAPI;
use stdClass;

/**
 *
 */
class ConfigurationAPI extends BaseController
{

    /**
     * A property to store the rest api class
     *
     * @var restAPI
     */
    public $restAPI;

    /**
     * A variable to store base route
     * @var string
     */
    public $baseRoute = '';

    /**
     * A variable to store the api routes
     * @var array
     */
    public $apiRoutes = [];

    public $apiRoutessss = stdClass::class;


    /**
     *  A function that is called when we register it in the init method
     *  Register contain all the methods that we want to call when we register the plugin
     *
     * @return void
     */
    public function register()
    {
        $this->restAPI = new RestAPI();
        $this->setApiRoutes();
        $this->restAPI->setApiRoutes($this->apiRoutes)->register();
    } //end register()


    /**
     *  A function that we can add all the methods we want to add to the wordpress rest api
     *  it will assign the pages to the $this->apiRoutes array
     *
     * @return void
     */


    public function setApiRoutes()
    {
        $this->apiRoutes = [
            [
                'endpoint'            => 'configuration',
                'methods'             => "GET",
                'permission_callback' => "__return_true",
                'callback'            => [
                    $this,
                    'getConfiguration',
                ],
            ],
        ];
    } //end setApiRoutes()


    /**
     *  A callback function that we can use to get the elasticssearch config
     * it will return the elasticssearch config
     * @return array
     */
    public function getConfiguration()
    {
        $config = [
            'title'           => 'OERSI',
            'description'     => 'OERSI is a plugin that allows you to search for data in the OERSI database.',
            'plugin_url'      => admin_url('/admin.php?page=oersi_plugin'),
            'version'         => $_ENV['OERSI_PLUGIN_VERSION'],
            'apiUrl'          => home_url('/wp-json/'.($_ENV['OERSI_API_NAMESPACE'] ?? 'oersi/v1')),
            'nonce'           => wp_create_nonce('wp_rest'),
            // Use for authentication.
            'assetURL'        => $this->pluginAssets,
            'language'        => explode('-', get_bloginfo('language'))[0],
            'siteUrl'         => home_url(),
            'timezone_string' => get_option('timezone_string'),
            'date_format'     => wp_date(get_option('date_format')),
            'date_format_js'  => get_option('date_format'),
            'start_of_week'   => get_option('start_of_week'),
            'time_format'     => get_option('time_format'),
            'timezone'        => get_option('timezone_string'),
            'configuration'   => $this->getFields(get_option(ConfigurationCallbacks::$optionName)),
        ];

        $response = new \WP_REST_Response($config, 200);
        $response->set_headers(['Cache-Control' => 'max-age=3600']);
        return $response;
    } //end getConfiguration()


    private function getFields($configuration)
    {
        if (empty($configuration) || !is_array($configuration)) {
            return [];
        }
        $callback = new ConfigurationCallbacks();
        foreach ($callback->fields() as $field) {
            // If the input is a checkbox, we need to check if it is checked or not.
            if ($field['type'] == 'checkbox') {
                $configuration[$field['name']] = isset($configuration[$field['name']]) && $configuration[$field['name']] == "on" ? true : false;
            }
            // If the input is a radio button, we need to check if it is checked or not.
            if ($field['type'] == 'radio') {
                $configuration[$field['name']] = isset($configuration[$field['name']]) ? true : false;
            }
            // If the input is a number, we need to check if it is a number or not.
            if ($field['type'] == 'number' || $field['type'] == 'range') {
                $configuration[$field['name']] = isset($configuration[$field['name']]) ? intval($configuration[$field['name']]) : 0;
            }
            // If the input is a text, we need to check if it is a text or not.
            if ($field['type'] == 'text' || !$field['name'] == 'pageSizeOptions') {
                $configuration[$field['name']] = is_string($configuration[$field['name']]) ? strip_tags(stripslashes($configuration[$field['name']])) : '';
            }
            // If the value is a a text with comma separated values, we need to check if the values are number or not.
            if ($field['type'] == 'text' && $field['name'] == 'pageSizeOptions') {
                if (isset($configuration[$field['name']]) && !empty($configuration[$field['name']])) {
                    strpos($configuration[$field['name']], ',') !== false ? $configuration[$field['name']] = array_map('intval', explode(',', $configuration[$field['name']])) : $configuration[$field['name']] = [intval($configuration[$field['name']])];
                } else {
                    $configuration[$field['name']] = [];
                }
            }
            // If the input is a textArea, we need to check if it is a textArea or not.
            if ($field['type'] == 'textArea') {
                $configuration[$field['name']] = strip_tags(stripslashes($configuration[$field['name']]));
            }
        }//end foreach

        return $configuration;
    } //end getFields()


}//end class
