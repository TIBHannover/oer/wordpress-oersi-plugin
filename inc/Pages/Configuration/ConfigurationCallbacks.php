<?php

/**
 * Created on Thu Jul 07 2022
 *
 * @package  OERSI
 * @license: MIT
 *
 * @author Edmond Kacaj <Edmond.Kacaj@tib.eu>
 *
 * Copyright (c) 2022 TIB <https://www.tib.eu/en>
 */

namespace Inc\Pages\Configuration;

use Inc\Api\AbstractCallback;

class ConfigurationCallbacks extends AbstractCallback
{

    /**
     * Declare the option group, for setSettings() method
     *
     * @var string
     */
    public static $optionGroup = 'oersi_plugin_configuration';

    /**
     * Declare the option name, for setSettings() method
     *
     * @var string
     */
    public static $optionName = 'oersi_plugin_configuration';

    /**
     * Declare the Id for setSettings() method
     *
     * @var string
     */
    public static $sectionId = 'oersi_configuration';



    /**
     *  Fields to be generated in the settings page, this function is called in the register method.
     *
     * @since  1.0.0
     * @return array
     */


    public function fields()
    {
        return [
            [
                'name'         => 'viewTypeFrontEnd',
                'id'           => 'viewTypeFrontEnd',
                'title'        => __('Default view type', 'oersi-domain'),
                'class'        => 'oersi-ui-select-field-viewTypeFrontEnd',
                'data-tooltip' => __('You can choose the default view type between List and Card.', 'oersi-domain'),
                'default'      => 'card',
                'type'         => 'select',
                'options'      => [
                    'list' => __('List', 'oersi-domain'),
                    'card' => __('Card', 'oersi-domain'),
                ],
            ],
            [
                'name'         => 'pageSize',
                'id'           => 'pageSize',
                'title'        => __('Default number of cards', 'oersi-domain'),
                'class'        => 'oersi-ui-number-field-pageSize',
                'data-tooltip' => __('You can decide the default number of cards (see documentation).', 'oersi-domain'),
                'default'      => '12',
                'type'         => 'number',
            ],
            [
                'name'         => 'pageSizeOptions',
                'id'           => 'pageSizeOptions',
                'title'        => __('Card selection in dropdown', 'oersi-domain'),
                'class'        => 'oersi-ui-number-field-pageSizeOptions',
                'data-tooltip' => __('Set the card selection in the pagination dropdown (see documentation). Numbers must be comma-separated.', 'oersi-domain'),
                'placeholder'  => '10,20,30,...',
                'default'      => '12,24,36',
                'type'         => 'text',
            ],
            [
                'name'         => 'showProgressBar',
                'id'           => 'showProgressBar',
                'title'        => __('Show progress bar', 'oersi-domain'),
                'class'        => 'oersi-ui-number-field-showProgressBar',
                'data-tooltip' => __('Show the progress bar as a scroll indicator in each card (see documentation).', 'oersi-domain'),
                'default'        => 'checked',
                'type'         => 'checkbox',
            ],
            [
                'name'         => 'showHtmlTags',
                'id'           => 'showHtmlTags',
                'title'        => __('Implement HTML tags', 'oersi-domain'),
                'class'        => 'oersi-ui-number-field-showHtmlTags',
                'data-tooltip' => __('Enabled for the correct interpretation of the existing HTML tags. ( see documentation )', 'oersi-domain'),
                'default'        => 'checked',
                'type'         => 'checkbox',
            ],
            [
                'name'         => 'searchTextLength',
                'id'           => 'searchTextLength',
                'title'        => __('Max. number of text characters', 'oersi-domain'),
                'class'        => 'oersi-ui-range-field-searchTextLength',
                'data-tooltip' => __('Set the maximum number of text characters for the search dropdown (see documentation).', 'oersi-domain'),
                'default'      => '150',
                'type'         => 'range',
                'min'          => '0',
                'max'          => '300',
                'step'         => '1',
            ],
        ];
    } //end fields()


    /**
     *  Return the Html template, this function is called as a callback in the setPage function.
     *
     * @since  1.0.0
     * @return void
     */
    public function template()
    {
        return include_once "$this->pluginPath/templates/configuration/configuration.php";
    } //end template()


    /**
     *  function is called as a callback in setSection function.
     *
     * @since  1.0.0
     * @return void
     */
    public function sectionManager()
    {
        echo '<p style="max-width: 65ch; margin-bottom:2.5em;">' . __('Manage the sections of configurations. Simply use the WordPress shortcode and insert <code>[oersi_search]</code> in your page or post.', 'oersi-domain') . '</p>';
    } //end sectionManager()


    /**
     * This method with deliver the input based on type of input.
     *
     * @since  1.0.0
     *
     * @param  string $args contains input arguments
     * @return void
     */
    public function render($args)
    {
        $type = ($args['type'] ?? 'text');
        if ($type == 'text') {
            $this->textField($args);
        } elseif ($type == 'textArea') {
            $this->textAreaField($args);
        } elseif ($type == 'checkbox') {
            $this->checkboxField($args);
        } elseif ($type == 'number') {
            $this->numberField($args);
        } elseif ($type == 'range') {
            $this->rangeField($args);
        } elseif ($type == 'select') {
            $this->dropDownField($args);
        }
    } //end render()


    /**
     *  This method will generate a text field. and will be called as a callback in the renderInputs method.
     *
     * @since  1.0.0
     *
     * @param  array $args contains input arguments
     * @return void
     */
    public function textField($args)
    {
        $name = $args['label_for'];
        $classes = ($args['class'] ?? $name).'-ui-text-field';
        $placeholder = ($args['placeholder'] ?? '');
        $default = ($args['default'] ?? '');
        $id = ($args['id'] ?? $name).'-ui-text-field-id';
        $optionName = $args['option_name'];
        $value = get_option($optionName);
        echo '<input type="text" name="'.$optionName.'['.$name.']"'.' class="'.$classes.'" size="100" placeholder="'.$placeholder.'" value="'.(isset($value[$name]) ? $value[$name] : $default).'" id="'.$id.'" />';
    } //end textField()


    /**
     *  This method will generate a textarea field. and will be called as a callback in the renderInputs method.
     *
     * @since  1.0.0
     *
     * @param  array $args contains input arguments
     * @return void
     */
    public function textAreaField($args)
    {
        $name = $args['label_for'];
        $classes = ($args['class'] ?? $name).'-ui-text-area';
        $id = ($args['id'] ?? $name).'-ui-text-area-id';
        $optionName = $args['option_name'];
        $value = get_option($optionName);
        $renderHtml = '<textarea id="configuration_text_area" style="display: none !important;" hidden name="'.$optionName.'['.$name.']"'.' id="'.$id.'" class="'.$classes.'" />'.(isset($value[$name]) ? $value[$name] : '').'</textarea>';
        echo $renderHtml;
    } //end textAreaField()


    /**
     *  This method will generate a checkbox field. and will be called as a callback in the renderInputs method.
     *
     * @since  1.0.0
     *
     * @param  array $args contains input arguments
     * @return void
     */
    public function checkboxField($args)
    {
        $name = $args['label_for'];
        $classes = $args['class'];
        $option_name = $args['option_name'];
        $default = isset($args['default']) && $args['default'] === 'checked';
        $checkbox = get_option($option_name);

        // Wenn keine Option gesetzt ist, verwende den Standardwert
        $value = isset($checkbox[$name]) ? $checkbox[$name] : $default;

        echo '<div class="' . $classes . '">
            <input type="checkbox"
                id="' . $name . '"
                name="' . $option_name . '[' . $name . ']"
                value="1"
                class=""
                ' . ($value ? 'checked' : '') . '>
            <label for="' . $name . '">
                <div></div>
            </label>
        </div>';
    } //end checkboxField()


    /**
     *  This method will generate a input number and range field. and will be called as a callback in the renderInputs method.
     *
     * @since  1.0.0
     *
     * @param  array $args contains input arguments
     * @return void
     */
    public function numberField($args)
    {
        $name = $args['label_for'];
        $classes = ($args['class'] ?? $name).'-ui-number-field';
        $id = ($args['id'] ?? $name).'-ui-number-field-id';
        $default = ($args['default'] ?? '10');
        $optionName = $args['option_name'];
        $value = get_option($optionName);
        echo '<div class="'.$classes.'"><input type="number" id="'.$id.'" name="'.$optionName.'['.$name.']" value="'.(isset($value[$name]) ? $value[$name] : $default).'" class="'.$classes.'"  min="1" max="200"><label for="'.$name.'"><div></div></label></div>';
    } //end checkboxField()


    public function rangeField($args)
    {
        $name = $args['label_for'];
        $classes = ($args['class'] ?? $name).'-ui-range-field';
        $id = ($args['id'] ?? $name).'-ui-range-field-id';
        $default = ($args['default'] ?? '150');
        $min = ($args['min'] ?? '0');
        $max = ($args['max'] ?? '300');
        $step = ($args['step'] ?? '1');
        $optionName = $args['option_name'];
        $value = get_option($optionName);

        echo '<div class="range-field">';

        echo '<div class="range-head mb-2">';
        echo '<span>Min. '.$min.'</span>';
        echo '<span>Max. '.$max.'</span>';
        echo '</div>';

        echo '<input type="number" id="'.$id.'_input" value="'.(isset($value[$name]) ? $value[$name] : $default).'" min="'.$min.'" max="'.$max.'" step="'.$step.'" oninput="document.getElementById(\''.$id.'\').value = this.value">';


        echo '<input type="range" id="'.$id.'" name="'.$optionName.'['.$name.']" value="'.(isset($value[$name]) ? $value[$name] : $default).'" class="'.$classes.' mt-2" min="'.$min.'" max="'.$max.'" step="'.$step.'" style="width: 100%;" oninput="document.getElementById(\''.$id.'_input\').value = this.value">';

        echo '</div>';
    }//end rangeField()


    /**
     *  This method will generate a select field. and will be called as a callback in the renderInputs method.
     *
     * @since  1.0.0
     *
     * @param  array $args contains input arguments
     * @return void
     */
    public function dropDownField($args)
    {
        $name = $args['label_for'];
        $classes = ($args['class'] ?? $name).'-ui-select-field';
        $default = ($args['default'] ?? '');
        $id = ($args['id'] ?? $name).'-ui-select-field-id';
        $optionName = $args['option_name'];
        $select = get_option($optionName);
        $select = ($select[$name] ?? $default);
        $options = ($args['options'] ?? []);


        $buildHtmlTag  = '<div class="'.$classes.'">';
        $buildHtmlTag .= '<select name="'.$optionName.'['.$name.']" id="'.$id.'">';

        foreach ($options as $key => $value) {
            $buildHtmlTag  .= '<option value="'.$key.'" '.($select == $key ? 'selected' : '').'>'.$value.'</option>';
        }
        $buildHtmlTag  .= '</select>';
        $buildHtmlTag  .= '<label for="'.$name.'"><div></div></label></div>';
        echo $buildHtmlTag;
    }//end checkboxField()


}//end class
