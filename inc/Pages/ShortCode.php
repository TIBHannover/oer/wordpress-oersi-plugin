<?php

/**
 * @package  OERSI
 */

namespace Inc\Pages;

use Inc\Base\BaseController;

/**
 *
 */
class ShortCode extends BaseController
{


    public function register()
    {
        add_shortcode('oersi_search', [$this, 'oersi_search_shortcode']);
    } //end register()


    public function oersi_search_shortcode($atts, $content = "")
    {
        $a = shortcode_atts([
            'products' => '',
            'classes'  => '',
            'style'    => '',
            'hidden'   => '',
        ], $atts);

        return '<div id="root" style="min-height: 70vh;"></div>';
    }//end oersi_search_shortcode()


}//end class
