<?php

/**
 * Created on Fri Jul 08 2022
 *
 * @package OERSI
 * @license: MIT
 *
 * @author Edmond Kacaj <Edmond.Kacaj@tib.eu>
 *
 * Copyright (c) 2022 TIB <https://www.tib.eu/en>
 */


namespace Inc\Pages\Translation;

use Inc\Api\AbstractCallback;

class TranslationCallBack extends AbstractCallback
{

    /**
     * Declare the option group, for setSettings() method
     *
     * @var string
     */
    public static $optionGroup = 'oersi_plugin_translation';

    /**
     * Declare the option name, for setSettings() method
     *
     * @var string
     */
    public static $optionName = 'oersi_plugin_translation';

    /**
     * Declare the Id for setSettings() method
     *
     * @var string
     */
    public static $sectionId = 'oersi_plugin_translation_index';


    /**
     *  Return the Html template, this function is called as a callback in the setPage function.
     *
     * @since  1.0.0
     * @return void
     */
    public function template()
    {
        return require_once("$this->pluginPath/templates/translation/translations.php");
    }


    /**
     *  Fields to be generated in the settings page, this function is called in the register method.
     *
     * @since  1.0.0
     * @return array
     */
    public function fields()
    {
        return [
            [
                'name'         => 'translations',
                'title'        => '',
                'class'        => 'oersi-ui-oersi_plugin_translation',
                'data-tooltip' => '',
                'type'         => 'textArea',
            ],
            // [
            // 'name'         => 'supportedLngs',
            // 'title'        => __('Add Supported Languages', 'oersi'),
            // 'class'        => 'oersi-ui-supportedLngs',
            // 'data-tooltip' => __('You can add the languages that you want to support', 'oersi'),
            // 'placeholder'  => "example: en, de, it",
            // 'type'         => 'text',
            // ],
        ];
    }


    /**
     *  Text For one section of the elasticsSearch page.
     *
     * @since  1.0.0
     * @return void
     */
    public function sectionManager()
    {
        echo '<p style="max-width: 65ch;">' . __('Edit the JSON data for translations in different languages. After making any changes, please refresh your browser to see the updates.', 'oersi-domain') . '</p>';
    }


    /**
     *  This method will generate a textarea for working with Json fields.
     */
    public function renderTexArea($args)
    {
        $name = $args['label_for'];
        $classes = ($args['class'] ?? $name).'-ui-text-area';
        $id = ($args['id'] ?? $name).'-ui-text-area-id';
        $optionName = $args['option_name'];
        $value = get_option($optionName);
        $renderHtml = '<textarea id="translation" style="display: none !important;"  hidden name="'.$optionName.'['.$name.']"'.'class="'.$classes.' id="'.$id.'" />'.(isset($value[$name]) ? $value[$name] : '').'</textarea>';
        echo $renderHtml;
    } //end renderTexArea()


    /**
     * This method with deliver the input based on type of input.
     *
     * @since  1.0.0
     * @return void
     */
    public function render($args)
    {
        $type = ($args['type'] ?? 'text');
        if ($type == 'text') {
            $this->textField($args);
        } elseif ($type == 'textArea') {
            $this->renderTexArea($args);
        }
    } //end render()


    /**
     *  This method will generate a text field. and will be called as a callback in the renderInputs method.
     *
     * @since  1.0.0
     *
     * @param  array $args contains input arguments
     * @return void
     */
    public function textField($args)
    {
        $name = $args['label_for'];
        $classes = ($args['class'] ?? $name).'-ui-text-field';
        $placeholder = ($args['placeholder'] ?? '');
        $default = ($args['default'] ?? '');
        $id = ($args['id'] ?? $name).'-ui-text-field-id';
        $optionName = $args['option_name'];
        $value = get_option($optionName);
        echo '<input type="text" name="'.$optionName.'['.$name.']"'.' class="'.$classes.'" size="100" placeholder="'.$placeholder.'" value="'.(isset($value[$name]) ? $value[$name] : $default).'" id="'.$id.'" />';
    } //end textField()


}//end class TranslationCallBack
