<?php

/**
 * Created on Thu Jul 07 2022
 *
 * @package  OERSI
 * @license: MIT
 *
 * @author Edmond Kacaj <Edmond.Kacaj@tib.eu>
 *
 * Copyright (c) 2022 TIB <https://www.tib.eu/en>
 */


namespace Inc\Pages\Translation;

use Inc\Api\RestAPI;
use Inc\Pages\Translation\TranslationCallBack;

/**
 *
 */
class TranslationRestAPI extends TranslationCallBack
{

    /**
     * A property to store the rest api class
     *
     * @var restAPI
     */
    public $restAPI;

    /**
     * A variable to store base route
     * @var string
     */
    public $baseRoute = '';

    /**
     * A variable to store the api routes
     * @var array
     */
    public $apiRoutes = [];


    /**
     *  A function that is called when we register it in the init method
     *  Register contain all the methods that we want to call when we register the plugin
     *
     * @return void
     */
    public function register()
    {
        $this->restAPI = new RestAPI();
        $this->setApiRoutes();
        $this->restAPI->setApiRoutes($this->apiRoutes)->register();
    } //end register()


    /**
     *  A function that we can add all the methods we want to add to the wordpress rest api
     *  it will assign the pages to the $this->apiRoutes array
     *
     * @return void
     */


    public function setApiRoutes()
    {
        // $this->baseRoute = 'oersi/v1';
        // give a parameter ?lang=en to the url to get the translation for that language
        $this->apiRoutes = [
            [
                'endpoint'            => 'locales/translation',
                'methods'             => "GET",
                'permission_callback' => "__return_true",
                'callback'            => [
                    $this,
                    'getTranslation',
                ],
            ],
            [
                'endpoint'            => 'locales/cards',
                'methods'             => "GET",
                'permission_callback' => "__return_true",
                'callback'            => [
                    $this,
                    'getCards',
                ],
            ],
            [
                'endpoint'            => 'locales/languages',
                'methods'             => "GET",
                'permission_callback' => "__return_true",
                'callback'            => [
                    $this,
                    'getLanguages',
                ],
            ],
        ];
    } //end setApiRoutes()


    /**
     *  A callback function to get the translation for a specific language
     * @param \WP_REST_Request $request the request object from the rest api
     * @return array
     */
    public function getTranslation(\WP_REST_Request $request)
    {
        $lang = ($request->get_param('lang') ?? 'de');
        $ns = "translations";
        $fieldName = self::fields()[0]['name'];
        $translation = json_decode(get_option(self::$optionName)[$fieldName], true);
        $translation = ($translation[$lang][$ns] ?? []);
        if (empty($translation)) {
            return new \WP_Error('rest_not_found', __('No Translations found', 'oersi-domain'), ['status' => 404]);
        }

        $result = new \WP_REST_Response($translation, 200);
        $result->set_headers(['Cache-Control' => 'max-age=3600']);
        // Set the cache to 1 hour.
        return $result;
    } //end getTranslation()


    /**
     *  A callback function that return the cards object for the given language
     * @param \WP_REST_Request $request the request object from the rest api
     * @return array
     */
    public function getCards(\WP_REST_Request $request)
    {
        $lang = ($request->get_param('lang') ?? 'de');
        $ns = "cards";
        $fieldName = self::fields()[0]['name'];
        $translation = json_decode(get_option(self::$optionName)[$fieldName], true);
        $translation = ($translation[$lang][$ns] ?? []);
        if (empty($translation)) {
            return new \WP_Error('rest_not_found', __('No Translations found', 'oersi-domain'), ['status' => 404]);
        }

        $result = new \WP_REST_Response($translation, 200);
        $result->set_headers(['Cache-Control' => 'max-age=3600']);
        // Set the cache to 1 hour.
        return $result;
    } //end getCards()


    /**
     *  A callback function that return the languages object for the given language
     * @param \WP_REST_Request $request the request object from the rest api
     * @return array
     */
    public function getLanguages(\WP_REST_Request $request)
    {
        $lang = ($request->get_param('lang') ?? 'de');
        $json_data_File = $this->pluginUrl.'assets/languages/'.$lang.'.json';
        $file_content = @file_get_contents($json_data_File);
        if ($file_content === false) {
            return new \WP_Error('rest_not_found', __('No Translations found', 'oersi-domain'), ['status' => 404]);
        }
        $result = new \WP_REST_Response(json_decode($file_content), 200);
        $result->set_headers(['Cache-Control' => 'max-age=3600']);
        // Set the cache to 1 hour.
        return $result;
    }//end getLanguages()

}//end class
