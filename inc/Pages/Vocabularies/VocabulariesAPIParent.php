<?php

/**
 * Created on Thu Jul 07 2022
 *
 * @package  OERSI
 * @license: MIT
 *
 * @author Edmond Kacaj <Edmond.Kacaj@tib.eu>
 *
 * Copyright (c) 2022 TIB <https://www.tib.eu/en>
 */


namespace Inc\Pages\Vocabularies;

use Inc\Api\RestAPI;
use Inc\Base\BaseController;

/**
 *
 */
class VocabulariesAPIParent extends BaseController
{

    /**
     * A property to store the rest api class
     *
     * @var restAPI
     */
    public $restAPI;

    /**
     * A variable to store base route
     * @var string
     */
    public $baseRoute = '';

    /**
     * A variable to store the api routes
     * @var array
     */
    public $apiRoutes = [];

    public $apiRoutessss = stdClass::class;

    private $vocabularies = [];


    /**
     *  A function that is called when we register it in the init method
     *  Register contain all the methods that we want to call when we register the plugin
     *
     * @return void
     */
    public function register()
    {
        $this->restAPI = new RestAPI();
        $this->setApiRoutes();
        $this->restAPI->setApiRoutes($this->apiRoutes)->register();
    } //end register()


    /**
     *  A function that we can add all the methods we want to add to the wordpress rest api
     *  it will assign the pages to the $this->apiRoutes array
     *
     * @return void
     */
    public function setApiRoutes()
    {
        $this->apiRoutes = [
            [
                'endpoint'            => 'locales/vocabularies_parents',
                'methods'             => "GET",
                'permission_callback' => "__return_true",
                'callback'            => [
                    $this,
                    'getVocabularies',
                ],
            ],
        ];
    } //end setApiRoutes()


    /**
     *  A callback function that we can use to get the vocabularies list
     * it will return the json with the vocabularies list and language
     * @return array
     */
    public function getVocabularies(\WP_REST_Request $request)
    {

        $api = new VocabulariesRestClient('https://skohub.io');
        $responseData = $api->get(
            '/dini-ag-kim//hochschulfaechersystematik/heads/master/w3id.org/kim/hochschulfaechersystematik/scheme.json'
        );

        if ($responseData->getStatusCode() === 200) {
            $this->flatten(json_decode($responseData->getBody()->getContents(), true)["hasTopConcept"]);
        }


        if (count($this->vocabularies) <= 0) {
            return new \WP_Error('no_vocabularies', 'No vocabularies found', ['status' => 404]);
        }

        $response = new \WP_REST_Response($this->vocabularies, 200);
        $response->set_headers(['Cache-Control' => 'max-age=3600']);
        return $response;
    } //end getConfiguration()


    /**
     *  A a function that can remove nested nested array from the array,  it will return a flat array
     * it will return a flat array
     * @param array $array - the array that we want to flatten
     * @return array
     */
    private function flatten(array $array, string $id = '')
    {

        foreach ($array as $item) {
            $children = [];
            if (isset($item['narrower']) && is_array($item['narrower'])) {
                $children = $this->flatten($item['narrower'], $item['id']);
            }
            if (!empty($id) && strlen($id) > 0) {
                $this->vocabularies[$item['id']] = $id;
            }
        }
    } //end flatten()


}//end class
