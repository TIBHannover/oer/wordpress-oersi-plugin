<?php

/**
 * Created on Thu Jul 07 2022
 *
 * @package  OERSI
 * @license: MIT
 *
 * @author Edmond Kacaj <Edmond.Kacaj@tib.eu>
 *
 * Copyright (c) 2022 TIB <https://www.tib.eu/en>
 */


namespace Inc\Pages\Vocabularies;

use Inc\Api\RestAPI;
use Inc\Base\BaseController;
use stdClass;

/**
 *
 */
class VocabulariesAPI extends BaseController
{

    /**
     * A property to store the rest api class
     *
     * @var restAPI
     */
    public $restAPI;

    /**
     * A variable to store base route
     * @var string
     */
    public $baseRoute = '';

    /**
     * A variable to store the api routes
     * @var array
     */
    public $apiRoutes = [];


    /**
     *  A function that is called when we register it in the init method
     *  Register contain all the methods that we want to call when we register the plugin
     *
     * @return void
     */
    public function register()
    {
        $this->restAPI = new RestAPI();
        $this->setApiRoutes();
        $this->restAPI->setApiRoutes($this->apiRoutes)->register();
    } //end register()


    /**
     *  A function that we can add all the methods we want to add to the wordpress rest api
     *  it will assign the pages to the $this->apiRoutes array
     *
     * @return void
     */
    public function setApiRoutes()
    {
        $this->apiRoutes = [
            [
                'endpoint'            => 'locales/labels',
                'methods'             => "GET",
                'permission_callback' => "__return_true",
                'callback'            => [
                    $this,
                    'getLabels',
                ],
            ],
        ];
    } //end setApiRoutes()


    /**
     * A callback function that we can use to get the vocabularies list
     * it will return the json with the vocabularies list and language
     * @return array
     */
    public function getLabels(\WP_REST_Request $request)
    {
        $locale = ($request->get_param('lang') ?? 'de');
        $fieldName = ($request->get_param('field') ?? '');

        $api = new VocabulariesRestClient(($_ENV['OERSI_PUBLIC_URL'] ?? 'https://oersi.org'));
        $responseData = $api->get($this->getLabelUrl($locale, $fieldName));
        if ($responseData->getStatusCode() === 200) {
            $response = json_decode($responseData->getBody()->getContents());
        }
        $response = new \WP_REST_Response($response, 200);
        $response->set_headers(['Cache-Control' => 'max-age=3600']);
        return $response;
    } //end getLabels()


    /**
     * A function that constructs the URL with parameters for the public label endpoint
     *
     * @param string $locale   The optional locale for the label set
     * @param string $fieldName The optional field to get labels for
     * @return string The constructed URL
     */
    private function getLabelUrl($locale = 'de', $fieldName = '')
    {
        $baseApiUrl = 'resources/api';
        $url = $baseApiUrl.'/label/'.urlencode($locale);

        $url .= $fieldName !== '' ? '?field='.urlencode($fieldName) : '';

        return $url;
    }


}//end class
