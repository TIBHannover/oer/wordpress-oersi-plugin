<?php

/**
 * Created on Fri Jul 08 2022
 *
 * @package OERSI
 * @license: MIT
 *
 * @author Edmond Kacaj <Edmond.Kacaj@tib.eu>
 *
 * Copyright (c) 2022 TIB <https://www.tib.eu/en>
 */

namespace Inc\Base;

use Inc\Pages\Translation\TranslationCallBack;

final class Utils
{


    /**
     * A function that is used to build a correct path based on operating system.
     * example:
     *      $path = Utils::build_path('assets', 'css', 'style.css'); // returns 'assets\css\style.css'
     *     $path = Utils::build_path('assets', 'css', 'style.css'); // returns 'assets/css/style.css' if the operating system is linux
     * @return string $path  - the correct path
     */
    public static function build_path(...$segments)
    {
        return join(DIRECTORY_SEPARATOR, $segments);
    } //end build_path()


    /**
     * A function that get Supported Languages from the plugin
     * @return array
     */
    public static function getSupportedLanguages()
    {
        $languages = get_option(TranslationCallBack::$optionName);
        if (empty($languages) || !is_array($languages)) {
            return [];
        }
        if (isset($languages['supportedLngs']) && !empty($languages['supportedLngs'])) {
            return strpos($languages['supportedLngs'], ',') !== false ? array_map('trim', explode(',', $languages['supportedLngs'])) : [$languages['supportedLngs']];
        } else {
            return [];
        }
    } //end getSupportedLanguages()


    /**
     * Check if the given conditions are met based on the data provided.
     *
     * @param array  $conditions   The conditions to check.
     * @param array $optionValues The array containing option names as keys and their respective values as values.
     *
     * @return bool True if all conditions are met, false otherwise.
     */
    public static function are_conditions_met($conditions, $optionValues)
    {
        // Security checks
        if (!isset($conditions) || !is_array($conditions)) {
            return true;
        }

        // If $optionValues is false or not an array, use empty array
        if (!is_array($optionValues)) {
            $optionValues = array();
        }

        if (!isset($conditions['terms']) || !is_array($conditions['terms'])) {
            return true;
        }

        $relation = isset($conditions['relation']) ? $conditions['relation'] : 'and';
        $allConditionsMet = true;

        foreach ($conditions['terms'] as $term) {
            if (!isset($term['name']) || !isset($term['value'])) {
                continue;
            }

            $conditionFieldName = $term['name'];
            $operator = isset($term['operator']) ? $term['operator'] : '===';
            $conditionValue = $term['value'];

            // Safe array access
            $actualValue = isset($optionValues[$conditionFieldName]) ? $optionValues[$conditionFieldName] : null;

            // Check if the condition is met.
            switch ($operator) {
                case '===':
                    if ($actualValue !== $conditionValue) {
                        $allConditionsMet = false;
                    }
                    break;
                case '==':
                    if ($actualValue != $conditionValue) {
                        $allConditionsMet = false;
                    }
                    break;
                case '!=':
                    if ($actualValue == $conditionValue) {
                        $allConditionsMet = false;
                    }
                    break;
                case '!==':
                    if ($actualValue !== $conditionValue) {
                        $allConditionsMet = false;
                    }
                    break;
                case 'in':
                    if (!in_array($actualValue, $conditionValue)) {
                        $allConditionsMet = false;
                    }
                    break;
                case '!in':
                    if (in_array($actualValue, $conditionValue)) {
                        $allConditionsMet = false;
                    }
                    break;
                case 'contains':
                    if (strpos($actualValue, $conditionValue) === false) {
                        $allConditionsMet = false;
                    }
                    break;
                case '!contains':
                    if (strpos($actualValue, $conditionValue) !== false) {
                        $allConditionsMet = false;
                    }
                    break;
                case '<':
                    if (!($actualValue < $conditionValue)) {
                        $allConditionsMet = false;
                    }
                    break;
                case '<=':
                    if (!($actualValue <= $conditionValue)) {
                        $allConditionsMet = false;
                    }
                    break;
                case '>':
                    if (!($actualValue > $conditionValue)) {
                        $allConditionsMet = false;
                    }
                    break;
                case '>=':
                    if (!($actualValue >= $conditionValue)) {
                        $allConditionsMet = false;
                    }
                    break;
                default:
                    // Handle unknown operator (optional).
                    $allConditionsMet = false;
                    break;
            }//end switch

            // Break the loop if we encounter a condition that doesn't meet the relation requirements.
            if ($relation === 'and' && !$allConditionsMet) {
                break;
            } elseif ($relation === 'or' && $allConditionsMet) {
                break;
            }
        }//end foreach

        return $allConditionsMet;
    } // end check_conditions


}//end class
