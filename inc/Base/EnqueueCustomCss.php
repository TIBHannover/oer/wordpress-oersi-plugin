<?php

/**
 * Created on Fri Jul 08 2022
 *
 * @package OERSI
 * @license: MIT
 *
 * @author Edmond Kacaj <Edmond.Kacaj@tib.eu>
 *
 * Copyright (c) 2022 TIB <https://www.tib.eu/en>
 */


namespace Inc\Base;

use Inc\Pages\CssOverride\CssOverrideCallbacks;

/**
 *
 */
class EnqueueCustomCss extends CssOverrideCallbacks
{


    /**
     * A function that is used to register the Action Hooks,
     * This function is called from the register function in the init class
     * @return void
     */
    public function register()
    {
        add_action('wp_head', [$this, 'enqueue']);
    } //end register()


    /**
     * A function that is used to register in the head
     * it will not be registered again
     * @return void
     */
    public function enqueue()
    {
        $cssOverride = get_option(self::$optionName);
        if (!empty($cssOverride)) {
            ?>
            <style>
                <?php echo $cssOverride['css'];?>
            </style>
            <?php
        }
    } //end enqueue()


}//end class
