<?php

/**
 * Created on Fri Jul 08 2022
 *
 * @package  OERSI
 * @license: MIT
 *
 * @author Edmond Kacaj <Edmond.Kacaj@tib.eu>
 *
 * Copyright (c) 2022 TIB <https://www.tib.eu/en>
 */

namespace Inc\Base;

class CustomSection
{

    /**
     * Prints out all settings sections added to a particular settings page
     *
     * Part of the Settings API. Use this in a settings page callback function
     * to output all the sections and fields that were added to that $page with
     * add_settings_section() and add_settings_field()
     *
     * @global array $wp_settings_sections Storage array of all settings sections added to admin pages.
     * @global array $wp_settings_fields Storage array of settings fields and info about their pages/sections.
     * @since 2.7.0
     *
     * @param string $page The slug name of the page whose settings sections you want to output.
     */
    public static function do_settings_sections($page)
    {
        global $wp_settings_sections, $wp_settings_fields;

        if (!isset($wp_settings_sections[$page])) {
            return;
        }

        foreach ((array) $wp_settings_sections[$page] as $section) {
            if ($section['title']) {
                echo "<h2>" . esc_html(__($section['title'], 'oersi-domain')) . "</h2>\n";
            }

            if ($section['callback']) {
                call_user_func($section['callback'], $section);
            }

            if (!isset($wp_settings_fields) || !isset($wp_settings_fields[$page]) || !isset($wp_settings_fields[$page][$section['id']])) {
                continue;
            }
            echo '<table class="form-table" role="presentation">';
            self::do_settings_fields($page, $section['id']);
            echo '</table>';
        }
    } //end do_settings_sections()


    /**
     * Prints out the settings fields for a particular settings section.
     *
     * Part of the Settings API. Use this in a settings page to output
     * a specific section. Should normally be called by do_settings_sections()
     * rather than directly.
     *
     * @global array $wp_settings_fields Storage array of settings fields and their pages/sections.
     *
     * @since 2.7.0
     *
     * @param string $page Slug title of the admin page whose settings fields you want to show.
     * @param string $section Slug title of the settings section whose fields you want to show.
     */
    public static function do_settings_fields($page, $section)
    {
        global $wp_settings_fields;

        if (!isset($wp_settings_fields[$page][$section])) {
            return;
        }

        foreach ((array) $wp_settings_fields[$page][$section] as $field) {
            $class = '';

            if (!empty($field['args']['class'])) {
                $class = ' class="' . esc_attr($field['args']['class']) . '"';
            }

            echo "<tr{$class}>";

            if (!empty($field['args']['label_for'])) {
                $title = __($field['title'], 'oersi-domain');
                $tooltip = !empty($field['args']['data-tooltip']) ? esc_attr(__($field['args']['data-tooltip'], 'oersi-domain')) : '';

                echo '<th scope="row"><label for="' . esc_attr($field['args']['label_for']) . '">' . $title .
                     (!empty($tooltip) ? '<i class="oersi-tooltip" data-tooltip="' . $tooltip . '">?</i>' : '') .
                     '</label></th>';
            } else {
                echo '<th scope="row">' . __($field['title'], 'oersi-domain') . '</th';
            }

            echo '<td>';
            call_user_func($field['callback'], $field['args']);
            echo '</td>';
            echo '</tr>';
        } //end foreach
    } //end do_settings_fields()

} //end class
