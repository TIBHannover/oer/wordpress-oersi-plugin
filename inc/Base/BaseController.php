<?php
/**
 * Created on Fri Jul 08 2022
 *
 * @package OERSI
 * @license: MIT
 *
 * @author Edmond Kacaj <Edmond.Kacaj@tib.eu>
 *
 * Copyright (c) 2022 TIB <https://www.tib.eu/en>
 */

namespace Inc\Base;

class BaseController
{

    public $pluginPath;

    public $pluginUrl;

    public $plugin;

    public $pluginFileName;

    public $pluginName;

    public $pluginVersion;

    public $pluginAssets;

    public $pluginLogsDir;

    public $pluginLogsFileName;


    public function __construct()
    {
        $this->pluginPath = plugin_dir_path(dirname(__FILE__, 2));
        $this->pluginUrl = plugin_dir_url(dirname(__FILE__, 2));
        $this->pluginFileName = 'oersi.php';
        $this->plugin = plugin_basename(dirname(__FILE__, 3)).'/'.$this->pluginFileName;
        $this->pluginName = $_ENV['OERSI_PLUGIN_NAME'];
        $this->pluginVersion = $_ENV['OERSI_PLUGIN_VERSION'];
        $this->pluginAssets = $this->pluginUrl.'assets/';
    }//end __construct()


}//end class
