<?php

/**
 * Created on Fri Jul 08 2022
 *
 * @package OERSI
 * @license: MIT
 *
 * @author Edmond Kacaj <Edmond.Kacaj@tib.eu>
 *
 * Copyright (c) 2022 TIB <https://www.tib.eu/en>
 */


namespace Inc\Base;

use Inc\Base\BaseController;

/**
 *
 */
class EnqueueJs extends BaseController
{

    // List of css files to be enqueued.
    private static $js_files_admin = [
        [
            'handle'  => 'main_script',
            'src'     => 'assets/js/oer-index.js',
            'deps'    => false,
            'version' => false,
            'index'   => [
                'strategy' => 'defer',
                'in_footer' => true
            ],
        ],
        [
            'handle'  => 'monaco_editor',
            'src'     => 'assets/js/oer-editor.js',
            'deps'    => false,
            'version' => false,
            'index'   => [
                'strategy' => 'defer',
                'in_footer' => true
            ],
        ],
    ];

    // List of js files to be enqueued for the frontend.
    private static $js_files_frontend = [
        [
            'handle'  => 'elastics_configuration',
            'src'     => 'assets/js/oer-elastics.js',
            'deps'    => false,
            'version' => false,
            'index'   => [
                'strategy' => 'defer',
                'in_footer' => true
            ],
        ],
        [
            'handle'  => 'frontend_script',
            'src'     => 'assets/js/oer-frontend.js',
            'deps'    => false,
            'version' => false,
            'index'   => [
                'strategy' => 'defer',
                'in_footer' => true
            ],
        ],
    ];


    /**
     * A function that is used to register the Action Hooks,
     * This function is called from the register function in the init class
     * @return void
     */
    public function register()
    {
        add_action('admin_enqueue_scripts', [$this, 'enqueue']);
        add_action('wp_enqueue_scripts', [$this, 'registerFrontEndScript']);
        // We need to change the script in order to use type modules in the front end.
        add_filter("script_loader_tag", [$this, 'add_module_to_my_script'], 10, 3);
    }


    /**
     * A function that is used to register the js files, if the js file has not been registered, otherwise
     * it will not be registered again
     * @return void
     */
    public function enqueue()
    {
        // Enqueue all our js files.
        foreach (self::$js_files_admin as $js_file) {
            wp_enqueue_script(
                $js_file['handle'],
                $this->pluginUrl.$js_file['src'],
                $js_file['deps'],
                $js_file['version'],
                $js_file['index']
            );
        }
    }


    /**
     * A function that will register the js files for the front end
     * it will not be registered again
     * @return void
     */
    public function registerFrontEndScript()
    {

        // Enqueue all our js files.
        foreach (self::$js_files_frontend as $js_file) {
            wp_register_script(
                $js_file['handle'],
                $this->pluginUrl.$js_file['src'],
                $js_file['deps'],
                $js_file['version'],
                $js_file['index']
            );
            wp_enqueue_script($js_file['handle']);
        }

        $script_params = [
            'siteUrl'   => get_site_url(),
            'pluginUrl' => admin_url('/admin.php?page=oersi_plugin'),
            'apiUrl'    => home_url('/wp-json/'.($_ENV['OERSI_API_NAMESPACE'] ?? 'oersi/v1')),
            'assetsUrl' => $this->pluginAssets,
            'language'  => explode('-', get_bloginfo('language'))[0],
        ];
        wp_localize_script('frontend_script', $_ENV['OERSI_PLUGIN_NAME'], $script_params);
    }


    public function add_module_to_my_script($tag, $handle, $src)
    {
        if ($handle == "frontend_script") {
            $tag = '<script type="module" src="'.esc_url($src).'"></script>';
        }

        return $tag;
    }//end add_module_to_my_script


}
