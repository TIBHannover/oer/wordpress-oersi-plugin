<?php
/**
 * Created on Fri Jul 08 2022
 *
 * @package OERSI
 * @license: MIT
 *
 * @author Edmond Kacaj <Edmond.Kacaj@tib.eu>
 *
 * Copyright (c) 2022 TIB <https://www.tib.eu/en>
 */

namespace Inc\Base;

use Inc\Base\BaseController;

class SettingsLinks extends BaseController
{


    /**
     * A function that is used to register the Action Hooks,
     * This function is called from the register function in the init class
     * @return void
     */
    public function register()
    {
        add_filter("plugin_action_links_$this->plugin", [ $this, 'settings_link' ]);
    }//end register()


    /**
     * A function that is used to add a link to the plugin name, in plugin's list
     * it will not be registered again
     * @return void
     */
    public function settings_link($links)
    {
        $settingsLink = '<a href="admin.php?page=oersi_plugin">Settings</a>';
        array_push($links, $settingsLink);
        return $links;
    }//end settings_link()


}//end class
