<?php
/**
 * Created on Fri Jul 08 2022
 *
 * @package OERSI
 * @license: MIT
 *
 * @author Edmond Kacaj <Edmond.Kacaj@tib.eu>
 *
 * Copyright (c) 2022 TIB <https://www.tib.eu/en>
 */

namespace Inc\Base;

use Inc\Base\BaseController;

class MetaLinks extends BaseController
{


    /**
     * Function that is used to register the Action Hooks, This function is called from the register function in the init class
     * @return void
     */
    public function register()
    {
        add_filter('plugin_row_meta', [$this, 'meta_link'], 10, 2);
    }//end register()


    /**
     * Function that is used add meta links to the plugin
     * it will not be registered again
     * @param array $plugin_meta Array of meta links, we want to expand
     * @param string $pluginFile File name of the plugin
     * @return array $plugin_meta Must return the modified plugin meta links
     */
    public function meta_link($pluginMeta, $pluginFile)
    {
        if (strpos($pluginFile, $this->pluginFileName) !== false) {
            $pluginMeta[] = '<a href="https://gitlab.com/TIBHannover/oer/wordpress-oersi-plugin/-/boards" target="_blank">'.__("Gitlab Issues", "oersi-domain").'</a>';
            $pluginMeta[] = '<a href="https://www.twillo.de/oer/web/kontakt/" target="_blank">'.__("Contact us", "oersi-domain").'</a>';
            $pluginMeta[] = '<a href="https://twillo.de/" target="_blank">'.__("Go to twillo", "oersi-domain").'</a>';
            // Helper link to the plugin.
            // $pluginMeta[] = '<a href="admin.php?page='.$_ENV['OERSI_PLUGIN_NAME'].'-documentation">'.__("Documentation", "oersi-domain").'</a>';.
        }
        return $pluginMeta;
    }//end meta_link()


}//end class
