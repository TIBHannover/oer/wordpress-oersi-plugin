<?php
/**
 * Created on Fri Jul 08 2022
 *
 * @package OERSI
 * @license: MIT
 *
 * @author Edmond Kacaj <Edmond.Kacaj@tib.eu>
 *
 * Copyright (c) 2022 TIB <https://www.tib.eu/en>
 */
namespace Inc\Base;

class Deactivate
{


    public static function deactivate()
    {
        flush_rewrite_rules();
    }//end deactivate()


}//end class
