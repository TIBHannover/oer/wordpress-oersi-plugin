# How to release this project

To release this project, follow these steps:

1. **Ensure all changes are merged into the `master` branch**:
    - Make sure all features, bug fixes, and other changes are merged into the `master` branch.
    - Ensure the `master` branch is stable and passes all tests.

2. **Update the version number**:
    - Update the version number in `composer.json` and `package.json`.

3. **Create a Git tag**:
    - Create a new Git tag for the release. Use the format `X.X.X` (e.g., `1.0.0`).

4. **CI/CD pipeline**:
    - The CI/CD pipeline will automatically build the project and create the release artifacts.
    - Ensure the pipeline completes successfully.
    - The `build releases` job in the `.gitlab-ci.yml` file will handle uploading the release artifacts to the [package registry](https://gitlab.com/TIBHannover/oer/wordpress-oersi-plugin/-/packages) automatically.
    - Verify that the artifacts are correctly uploaded.

5. **Create a release page in GitLab**:
    - Go to the GitLab project page.
    - Navigate to the "[Releases](https://gitlab.com/TIBHannover/oer/wordpress-oersi-plugin/-/releases)" section.
    - Click on "New release".
    - Fill in the tag name, release title, description and milestone.
    - Add the link to the release artifact in the package registry under "Release assets". Use "oersi.zip" as title and "Package" as type.
    - Click "Create release".
