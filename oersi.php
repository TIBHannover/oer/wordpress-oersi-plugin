<?php

/**
 *
 * @package           OERSi
 * @wordpress-plugin
 * Plugin Name:       OERSI
 * Plugin URI:        https://gitlab.com/TIBHannover/oer/wordpress-oersi-plugin
 * Description:        OERSI(is a plugin that allows you to search OER materials in your wordpress site, it is a simplified version of the oersi.org)
 * Version:           2.0.0
 * Requires at least: 5.2
 * Requires PHP:      7.2
 * Author:            Edmond Kacaj <Edmond.Kacaj@tib.eu>
 * Author URI:        https://github.com/Edmondi-Kacaj
 * Text Domain:       oersi-domain
 * Domain Path:       /languages
 * @copyright         2022 TIB Hannover
 * License:           MIT
 * License URI:       https://opensource.org/licenses/MIT
 * Update URI:        https://gitlab.com/TIBHannover/oer/wordpress-oersi-plugin
 */

/*
 * This file is part of OERSi.
 *
 * MIT License
 *
 * Copyright (c) 2020 TIB Hannover / OER
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


// NO WHITESPACE OR NEWLINES BEFORE <?php !!!
defined('ABSPATH') or die();
if (file_exists(dirname(__FILE__).'/vendor/autoload.php')) {
    require_once dirname(__FILE__).'/vendor/autoload.php';
    Dotenv\Dotenv::createImmutable(__DIR__)->load();
}

use Inc\Base\Activate;
use Inc\Base\Deactivate;

/**
 * The code that runs during plugin activation.
 */

register_activation_hook(__FILE__, function() {
    if (ob_get_level()) {
        ob_end_clean();
    }
    ob_start();

    try {
        Activate::activate();
    } catch (\Exception $e) {
        error_log('OERSI Plugin Activation Error: ' . $e->getMessage());
    }

    ob_end_clean(); // Only buffer cleaning
});

/**
 * The code that runs during plugin deactivation.
 */


function deactivateOersiPlugin()
{
    Deactivate::deactivate();
}//end ddeactivateOersiPlugin()


register_deactivation_hook(__FILE__, "deactivateOersiPlugin");



if (class_exists('Inc\\Init')) {
    Inc\Init::register_oersi_plugin();
}

/**
 * Translation.
 */
function loadOersiTextdomain() {
    load_plugin_textdomain('oersi-domain', false, dirname(plugin_basename(__FILE__)) . '/languages');
}
add_action('plugins_loaded', 'loadOersiTextdomain');
