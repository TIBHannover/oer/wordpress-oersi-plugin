<?php if (!defined('ABSPATH')) exit; ?>

<?php /* cssOverride codeExample Components */ ?>

<pre>
    <code>
    .<?php echo $_ENV['OERSI_PLUGIN_CLASS_PREFIX']; ?>layout-container {
        background-color: white;
        font-size: .85rem;
        color: rgb(0,0,0,.8);
        ...
        ...
        etc.
    }

    .<?php echo $_ENV['OERSI_PLUGIN_CLASS_PREFIX']; ?>multilist-component-accordion-summary {
        background-color: green;
        border-radius: .25rem;
        font-size: 1rem;
        ...
        ...
        etc.
    }
    </code>
</pre>
