<div class="wrap oersi-wrap">
<h1 class="screen-reader-text"><?php echo esc_html($_ENV['OERSI_PLUGIN_NAME']); ?></h1>
    <?php

    use Inc\Pages\CssOverride\CssOverrideCallbacks;
    use Inc\Base\CustomSection;

    settings_errors(); ?>

    <ul class="nav nav-tabs">
        <li class="active">
            <a href="#tab-1">
                <span class="tab-icon">
                    <img src="<?php echo esc_url(plugins_url('../../assets/images/cog-8-tooth.svg', __FILE__)); ?>" alt="" width="24"/>
                </span>
                <?php esc_html_e('Css overrides', 'oersi-domain'); ?>
            </a>
        </li>
        <li>
            <a href="#tab-2">
                <span class="tab-icon">
                    <img src="<?php echo esc_url(plugins_url('../../assets/images/document-text.svg', __FILE__)); ?>" alt="" width="24"/>
                </span>
                <?php esc_html_e('Documentation', 'oersi-domain'); ?>
            </a>
        </li>
    </ul>

    <div class="tab-content">
        <div id="tab-1" class="tab-pane tab-pane active oersi-ui-tab-pane">
            <form method="post" action="options.php">
                <?php
                settings_fields(CssOverrideCallbacks::$optionGroup);
                CustomSection::do_settings_sections(CssOverrideCallbacks::$optionName);
                ?>

                <div id="editor" data-editor="css_override" class="oersi-ui-css_override_fields"></div>

                <?php
                submit_button(null, 'button-primary button-hero');
                ?>
            </form>
            <script>
                window.addEventListener("load", function() {
                    const elements_css = document.querySelector("[data-editor='css_override']");
                    const textarea_css = document.getElementById("css_override");
                    var value = textarea_css.value;
                    const cssEditor = window.codeEditor
                        .setElement(elements_css)
                        .setValue(value !== "" ? codeEditor.formatCss(value) : "")
                        .setOption({
                            autoScrollEditorIntoView: true,
                            maxLines: 0,
                            minLines: 30,
                            placeholder: "e.g.: .oersi-frontend-layout-container {\n    background: #fff;\n}"
                        }).show().getEditor();

                    cssEditor.getSession().setValue(cssEditor.getSession().getValue() || "/**\n e.g.:\n .oersi-frontend-layout-container {\n    background: #fff;\n}\n **/");

                    cssEditor.getSession().on('change', function(e) {
                        textarea_css.value = cssEditor.getValue();
                    });
                });
            </script>
        </div>

        <div id="tab-2" class="tab-pane">
            <main id="oersi-doc-main-doc">
                <nav id="oersi-doc-navbar">
                    <ul>
                        <li>
                            <a href="#Information" class="nav-link">
                                <?php esc_html_e('Information', 'oersi-domain'); ?>
                            </a>
                        </li>
                    </ul>
                </nav>
                <div class="container">
                    <section class="main-section" id="Information">
                        <header>
                            <h2><?php esc_html_e('Information', 'oersi-domain'); ?></h2>
                        </header>
                        <p>
                            <?php
                            echo wp_kses_post(__('OERSI provides a default style for the search (frontend) part. Some users want to have their own <strong>colors</strong>, <b>margins</b>, etc. OERSI gives you the opportunity to add your own style.', 'oersi-domain'));
                            ?>
                            </p>
                            <p>
                            <?php
                            echo wp_kses_post(__('In OERSI, we have included some classes that can be overridden by CSS in the admin panel without needing to rebuild the project again. Every element contains classes that start with <strong>oersi-frontend-* </strong>. These classes remain the same in every feature build, so you don\'t need to re-style again because of incompatibilities.', 'oersi-domain'));
                            ?>
                        </p>
                        <h3><?php echo esc_html__('Example of styling', 'oersi-domain'); ?></h3>
                        <?php include_once plugin_dir_path(__FILE__) .  'codeExample.php'; ?>
                        <hr />
                    </section>
                </div>
            </main>
        </div>
    </div>
