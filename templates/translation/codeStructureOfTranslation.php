<?php
if (!defined('ABSPATH')) {
    exit;
}

$jsonData = [
    "en" => [
        "translations" => [
            "N/A" => "Not defined",
            "INPUT_PLACEHOLDER" => "Search by {{what}} ",
            "CLEAR_ALL" => "Clear all",
            "SHOW_RESULT_STATS" => "{{total}} results found",
            "PAGE_SIZE_SELECTION" => "{{size}} / page",
            "SHOW_TOTAL" => "{{rangeStart}}-{{rangeEnd}} of {{total}} items",
            "SORT_BY_COUNTER" => "Sort by Count {{what}}",
            "SORT_BY_LABEL" => "Sort by label {{what}}",
            "NOT_FOUND_TITLE" => "Sorry, page not found!",
            "NOT_FOUND_BODY" => "Sorry, we couldn’t find the page you’re looking for. Perhaps you’ve mistyped the URL? Be sure to check your spelling.",
            "NOT_FOUND_LINK" => "go to plugin",
            "ABOUT" => "Subject",
            "LICENSE" => "License",
            "RESOURCE_TYPE" => "Material type",
            "AUTHOR" => "Author",
            "ORGANISATION" => "Source",
            "LANGUAGE" => "Languages",
            "PROVIDER" => "Provider",
            "SEARCH" => "Search",
            "SEARCH_LABEL" => "Search",
            "SEARCH_PLACEHOLDER" => "Search for OER ...",
            "SEARCH_BUTTON_TEXT" => "Suche",
            "SHOW_MORE_TEXT" => "Show more",
            "SHOW_LESS_TEXT" => "Show less",
            "LIST_VIEW" => "List View",
            "CARD_VIEW" => "Card View",
            "SORT_OPTION" => "Sort by ...",
            "SORT_OPTION_LABEL_ABOUT" => "About",
            "SORT_OPTION_LABEL_MODIFIED" => "Modified"
        ],
        "cards" => [
            "SUBJECT" => "Subject :",
            "MATERIAL_TYPE" => " Material type :",
            "DESCRIPTION" => "Description",
            "NAME" => "Name",
            "LICENSE" => "License :",
            "AUTHOR" => "Author :",
            "DATE_CREATED" => "Date Created :",
            "DATE_PUBLISHED" => "Publishing year :",
            "KEYWORDS" => "Keywords :",
            "DETAILS" => "To Material"
        ]
    ]
];

echo '<pre><code>' . htmlspecialchars(json_encode($jsonData, JSON_PRETTY_PRINT)) . '</code></pre>';
