<?php
if (!defined('ABSPATH')) {
    exit;
}


$jsonContent = '
{
    "de": {
        "translations": {
            "N\/A": "Nicht definiert",
            "INPUT_PLACEHOLDER": "Suche nach {{what}}",
            ".....": "....."
        },
        "cards": {
            "SUBJECT": "v",
            "MATERIAL_TYPE": "Materialart",
            ".....": "....."
        }
    },
    "it": {
        "translations": {
            "N\/A": "Non definito",
            "INPUT_PLACEHOLDER": "Cerca per {{what}}",
            ".....": "....."
        },
        "cards": {
            "SUBJECT": "Tema",
            "MATERIAL_TYPE": "Tipo di materiale",
            ".....": "....."
        }
    }
}';

echo '<pre><code>' . htmlspecialchars($jsonContent) . '</code></pre>';
