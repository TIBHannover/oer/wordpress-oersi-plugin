<div class="wrap oersi-wrap">
<h1 class="screen-reader-text"><?php echo esc_html($_ENV['OERSI_PLUGIN_NAME']); ?></h1>
    <?php

    use Inc\Pages\Translation\TranslationCallBack;
    use Inc\Base\CustomSection;
    use Inc\DefaultData\Translation;

    settings_errors();
    ?>
    <ul class="nav nav-tabs">
        <li class="active">
            <a href="#tab-1">
                <span class="tab-icon">
                    <img src="<?php echo esc_url(plugins_url('../../assets/images/cog-8-tooth.svg', __FILE__)); ?>" alt="" width="24"/>
                </span>
                <?php esc_html_e('Translation Front-End', 'oersi-domain'); ?>
            </a>
        </li>
        <li>
            <a href="#tab-2">
                <span class="tab-icon">
                    <img src="<?php echo esc_url(plugins_url('../../assets/images/document-text.svg', __FILE__)); ?>" alt="" width="24"/>
                </span>
                <?php esc_html_e('Documentation', 'oersi-domain'); ?>
            </a>
        </li>
    </ul>

    <div class="tab-content">
        <div id="tab-1" class="tab-pane active oersi-ui-tab-pane">
            <form method="post" action="options.php">
                <?php
                settings_fields(TranslationCallBack::$optionGroup);
                CustomSection::do_settings_sections(TranslationCallBack::$optionName);
                ?>
                <div id="editor" data-editor="translation" class="oersi-ui-translation"></div>
                <?php submit_button(null, 'button-primary button-hero'); ?>
            </form>
        </div>
        <div id="tab-2" class="tab-pane">
            <main id="oersi-doc-main-doc">
                <nav id="oersi-doc-navbar">
                    <ul>
                        <li>
                            <a href="#Information" class="nav-link">
                                <?php esc_html_e('Information', 'oersi-domain'); ?>
                            </a>
                        </li>
                        <li>
                            <a href="#StructureOfTranslation" class="nav-link">
                                <?php esc_html_e('Structure of Translation', 'oersi-domain'); ?>
                            </a>
                        </li>
                    </ul>
                </nav>
                <div class="container">
                    <section class="main-section" id="Information">
                        <header>
                            <h2><?php esc_html_e('Information', 'oersi-domain'); ?></h2>
                        </header>
                        <p>
                            <?php echo wp_kses( __('Translation is the process of adapting your website’s content into different languages. In <strong>OERSI</strong>, it is possible to use multiple languages based on the <strong>WordPress locale</strong>. To change the language of OERSI, select the preferred language in the <strong>WordPress settings</strong>.','oersi-domain'),['strong' => [],]); ?>
                        </p>
                    </section>
                    <hr />
                    <section class="main-section" id="StructureOfTranslation">
                        <header>
                            <h2><?php esc_html_e('Structure of Translation', 'oersi-domain'); ?></h2>
                        </header>
                        <p>
                        <?php echo wp_kses( __('For the translation to work, you need to have a structure like the one below. The example shows the structure used for the English language translation:','oersi-domain'),['strong' => [],]);?>
                        </p>
                        <div class="mt-8 mb-12">
                            <?php include_once plugin_dir_path(__FILE__) .  'codeStructureOfTranslation.php'; ?>
                        </div>
                        <h3><?php echo wp_kses( __('Example for Adding More Languages','oersi-domain'),['strong' => []]);?></h3>
                        <p>
                            <?php echo wp_kses( __('For translation, we use a <strong>JSON format (Key-Value)</strong>, where the <strong>Key</strong> is used internally by the application, and the <strong>Value</strong> is the text displayed to users. Some values also include variables, such as <strong>{{what}}</strong>, which the system will replace with specific values at runtime.','oersi-domain'),['strong' => []]);?>
                        </p>
                        <div class="mt-8 mb-12">
                            <?php include_once plugin_dir_path(__FILE__) .  'codeExample.php'; ?>
                        </div>
                        <div>
                        <button class="button button-primary button-hero" data-target="my-dialog" data-model-trigger aria-label="<?php echo esc_attr(__('Open settings modal and copy default translation for English and German to clipboard.', 'oersi-domain')); ?>">
                            <?php echo esc_html(__('Copy Default Translation for English and German', 'oersi-domain')); ?>
                        </button>
                            <dialog class="oersi-modal-dialog" data-name="my-dialog">
                                <header>
                                    <div class="info">
                                        <p class="lucky">
                                            <?php esc_html_e('The text has been copied to the clipboard.', 'oersi-domain'); ?>
                                        </p>
                                        <div class="dashicons dashicons-yes"></div>
                                    </div>
                                    <button class="btn-oersi btn-oersi-icon" data-target="my-dialog" data-model-trigger>
                                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                                            <path stroke-linecap="round" stroke-linejoin="round" d="M10 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2m7-2a9 9 0 11-18 0 9 9 0 0118 0z" />
                                        </svg>
                                    </button>
                                </header>
                                <main>
                                    <pre>
                                        <code id="textToCopy">
                                            <?php echo json_encode(Translation::$translations, JSON_PRETTY_PRINT);?>
                                        </code>
                                    </pre>
                                </main>
                                <footer>
                                    <button class="btn-oersi btn-oersi-icon" data-target="my-dialog" data-model-trigger>
                                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                                            <path stroke-linecap="round" stroke-linejoin="round" d="M10 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2m7-2a9 9 0 11-18 0 9 9 0 0118 0z" />
                                        </svg>
                                    </button>
                                </footer>
                            </dialog>
                        </div>
                    </section>
                </div>
        </div>

    </div>
</div>
<script>
    window.addEventListener("load", function() {
        const elements_json = document.querySelector("[data-editor='translation']");
        // get text area element by hidden attribute
        const textarea_json = document.getElementById("translation");
        // get value
        var value = textarea_json.value;
        // debugger;
        const jsEditor = window.codeEditor
            .setElement(elements_json)
            .setValue(value !== "" ? codeEditor.formatJson(value) : "{}")
            .setOption({
                autoScrollEditorIntoView: true,
                maxLines: 0,
                minLines: 30,

            }).show().getEditor();

        jsEditor.getSession().on('change', function(e) {
            // disable submit button if not valid json
            if (codeEditor.isJson(jsEditor.getValue())) {
                document.querySelector("[type='submit']").disabled = false;
                textarea_json.value = JSON.stringify(JSON.parse(jsEditor.getValue()));
            } else {
                document.querySelector("[type='submit']").disabled = true;
            }
        });
    });
</script>
