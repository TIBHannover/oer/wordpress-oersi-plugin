<div class="wrap oersi-wrap">
    <h1 class="screen-reader-text"><?php echo esc_html($_ENV['OERSI_PLUGIN_NAME']); ?></h1>
    <?php
    use Inc\Pages\Configuration\ConfigurationCallbacks;
    use Inc\Base\CustomSection;
    settings_errors();
    ?>

    <ul class="nav nav-tabs">
        <li class="active">
            <a href="#tab-1">
                <span class="tab-icon">
                    <img src="<?php echo esc_url(plugins_url('../../assets/images/cog-8-tooth.svg', __FILE__)); ?>" alt="" width="24"/>
                </span>
                <?php esc_html_e('Configuration', 'oersi-domain'); ?>
            </a>
        </li>
        <li>
            <a href="#tab-2">
                <span class="tab-icon">
                    <img src="<?php echo esc_url(plugins_url('../../assets/images/document-text.svg', __FILE__)); ?>" alt="" width="24"/>
                </span>
                <?php esc_html_e('Documentation', 'oersi-domain'); ?>
            </a>
        </li>
    </ul>

    <div class="tab-content">
        <div id="tab-1" class="tab-pane active oersi-ui-tab-pane">
            <form method="post" action="options.php">
                <?php
                settings_fields(ConfigurationCallbacks::$optionGroup);
                CustomSection::do_settings_sections(ConfigurationCallbacks::$optionName);
                submit_button(null, 'button-primary button-hero');
                ?>
            </form>
        </div>
        <div id="tab-2" class="tab-pane">
            <main id="oersi-doc-main-doc">
                <nav id="oersi-doc-navbar">
                    <ul>
                        <li>
                            <a href="#About" class="nav-link">
                                <?php esc_html_e('Information', 'oersi-domain'); ?>
                            </a>
                        </li>
                        <li>
                            <a href="#fieldsDescription" class="nav-link">
                                <?php esc_html_e('Field Descriptions', 'oersi-domain'); ?>
                            </a>
                        </li>
                    </ul>
                </nav>
                <div class="container">
                    <section class="main-section" id="About">
                        <header>
                            <h2><?php esc_html_e('Information', 'oersi-domain'); ?></h2>
                        </header>
                        <p>
                            <?php esc_html_e('In addition to using your project\'s environment variables for configuration, you can also use the admin panel within the project. All configurations made here will automatically apply to the frontend without requiring a rebuild of the project.', 'oersi-domain'); ?>
                        </p>
                        <h3><?php esc_html_e('Activate Search on a WordPress Page', 'oersi-domain'); ?></h3>
                        <p>
                        <?php echo wp_kses( __('To activate the search (frontend) on any page, simply use the WordPress <b>shortcode</b> and insert the search shortcode <code>[oersi_search]</code> into the shortcode input field.', 'oersi-domain'), array('b'    => array(),'code' => array())); ?>
                        </p>

                        <h4><?php esc_html_e('Important Information!', 'oersi-domain'); ?></h4>
                        <p class="mb-12"><?php esc_html_e('Some configurations need to be set for the frontend to work properly, so please ensure you add configurations for each part. In the documentation, you will find default configurations at the end of each section. Copy and paste these into the respective input fields, and then refresh the page.', 'oersi-domain'); ?></p>
                        <hr />
                    </section>
                    <section class="main-section mt-12" id="fieldsDescription">
                        <header>
                            <h2><?php esc_html_e('Field Descriptions', 'oersi-domain'); ?></h2>
                        </header>
                        <p>
                            <?php esc_html_e('Below is a description of all the fields you can configure in this panel.', 'oersi-domain'); ?>
                        </p>
                        <ol class="mt-12">
                            <li>
                                <div class="heading">
                                    <h3><?php esc_html_e('Default view type', 'oersi-domain'); ?></h3>
                                </div>
                                <p><?php esc_html_e('You can choose the default view type between List and Card on the frontend.', 'oersi-domain'); ?></p>
                                <div class="footer"></div>
                            </li>
                            <li>
                                <div class="heading">
                                    <h3><?php esc_html_e('Default number of cards', 'oersi-domain'); ?></h3> <i>(number)</i>
                                </div>
                                <p><?php esc_html_e('You can set the number of cards to be displayed on the frontend.', 'oersi-domain'); ?></p>
                                <strong><?php esc_html_e('Example', 'oersi-domain'); ?>:</strong>
                                <code><?php esc_html_e('12 (= 12 cards will be shown)', 'oersi-domain'); ?></code>
                                <div class="footer">
                                <p><b>"<?php esc_html_e('Default number of cards', 'oersi-domain'); ?>"</b>&nbsp;<?php esc_html_e('must correspond with one of the numbers in', 'oersi-domain'); ?>&nbsp;<b>"<?php esc_html_e('Card selection in dropdown', 'oersi-domain'); ?>"</b>.</p></div>
                            </li>
                            <li>
                                <div class="heading">
                                    <h3><?php esc_html_e('Card selection in dropdown', 'oersi-domain'); ?></h3> <i>(string)</i>
                                </div>
                                <p><?php echo wp_kses( __('Define the number of Cards to be displayed in the pagination dropdown. This allows the user to choose how many Cards should be visible per page. The numbers must be separated by <b>commas</b>.', 'oersi-domain'), array('b'    => array(),)); ?></p>
                                <strong><?php esc_html_e('Example', 'oersi-domain'); ?>:</strong>
                                <code><?php esc_html_e('12, 24, 36', 'oersi-domain'); ?></code>
                                <div class="footer"></div>
                            </li>
                            <li>
                                <div class="heading">
                                    <h3><?php esc_html_e('Show progress bar', 'oersi-domain'); ?></h3> <i>(boolean)</i>
                                </div>
                                <p><?php echo wp_kses( __('In the cards, we have the option to display more information about a material by hovering the mouse (long touching on mobile or tablet). <b>Show Progress Bar</b> adds a progress bar above the content when scrolling inside the content.', 'oersi-domain'), array('b'    => array(),)); ?></p>
                                <strong><?php esc_html_e('Example', 'oersi-domain'); ?>:</strong>
                                <code><?php esc_html_e('checked=true (activated)', 'oersi-domain'); ?></code>
                                <div class="footer"></div>
                            </li>
                            <li>
                                <div class="heading">
                                    <h3><?php esc_html_e('Implement HTML tags', 'oersi-domain'); ?></h3> <i>(boolean)</i>
                                </div>
                                <p><?php echo wp_kses( __('Some materials may contain HTML tags in their descriptions, which allows you to render them rather than displaying them as plain text. You can choose either to render them or to display them as a plain string.', 'oersi-domain'), array('b'    => array(),)); ?></p>
                                <strong><?php esc_html_e('Example', 'oersi-domain'); ?>:</strong>
                                <code><?php esc_html_e('checked=true (Render HTML tags)', 'oersi-domain'); ?></code>
                                <div class="footer"></div>
                            </li>
                            <li>
                                <div class="heading">
                                    <h3><?php esc_html_e('Max. number of text characters', 'oersi-domain'); ?></h3> <i>(number)</i>
                                </div>
                                <p><?php echo wp_kses( __('Some descriptions are relatively long. To limit the length of the descriptions displayed in search inputs and selected fields, you can set the length of the text to be displayed. This does not affect the description in the card content.', 'oersi-domain'), array('b'    => array(),)); ?></p>
                                <strong><?php esc_html_e('Example', 'oersi-domain'); ?>:</strong>
                                <code><?php esc_html_e('Default is 200', 'oersi-domain'); ?></code>
                                <div class="footer"></div>
                            </li>
                        </ol>
                    </section>
                </div>
            </main>
        </div>
    </div>
</div>
