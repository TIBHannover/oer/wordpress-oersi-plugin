<?php
if (!defined('ABSPATH')) {
    exit;
}

/* ElasticSearch Multilist Components */

require_once dirname(__FILE__) . '/../constants.php';

class MultiListRenderer {
    public static function renderDetails($title, $code, $description) {
        ob_start();
        ?>
        <details>
            <summary><h3><?php esc_html_e($title, 'oersi-domain'); ?></h3></summary>
            <div class="details-content">
                <code><?php echo esc_html($code); ?></code>
                <p><?php echo wp_kses_post(__($description, 'oersi-domain')); ?></p>
            </div>
        </details>
        <?php
        return ob_get_clean();
    }
}

$translations = [
    'aggregationSearchDebounce' => [
        'label' => TRANSLATION_LABEL_NUMBER,
        'description' => __('Milliseconds used to introduce a delay in every request.', 'oersi-domain'),
    ],
    'aggregationSearchMinLength' => [
        'label' => TRANSLATION_LABEL_NUMBER,
        'description' => __('Used to specify the min length of chars before the search will happen.', 'oersi-domain'),
    ],
    'className' => [
        'label' => TRANSLATION_LABEL_STRING,
        'description' => __('Add your custom class to the component to use it for styling.', 'oersi-domain'),
    ],
    'customQuery' => [
        'label' => TRANSLATION_LABEL_FUNCTION . ' ' . TRANSLATION_LABEL_OPTIONAL,
        'description' => __('Applies a custom query to the result component. This query will be run when no other components are being watched (via React prop), as well as in conjunction with the query generated from the React prop. The function should return a query. To add a custom query, you need to go to oer-elastics.js ==> oersi/assets/js/oer-elastics.js (you need a plugin editor for this) and add your function there, then add the name of the function to customQuery.', 'oersi-domain'),
    ],
    'dataField' => [
        'label' => TRANSLATION_LABEL_STRING,
        'description' => __('Data field to be connected to the component’s UI view. This field is used for doing an aggregation and returns the result. We’re using a .raw multifield here. You can use a field of type keyword or not_analyzed depending on your Elasticsearch cluster.', 'oersi-domain'),
    ],
    'defaultQuery' => [
    'label' => TRANSLATION_LABEL_FUNCTION . ' ' . TRANSLATION_LABEL_OPTIONAL,
    'description' => __('By default, the plugin uses "getPrefixAggregationQueryLicense" to handle license-based prefix aggregation. Alternatively, you can use "createFilteredQuery" function for custom filtering.<br><br><strong>IMPORTANT NOTE:</strong><br>You can only use either <a href="#defaultQuery2" onclick="document.querySelector(\'#defaultQuery2\').parentElement.open = true"><code>getPrefixAggregationQueryLicense</code></a> OR <a href="#defaultQuery3" onclick="document.querySelector(\'#defaultQuery3\').parentElement.open = true"><code>createFilteredQuery</code></a>, but not both simultaneously as this will override the filtering and return all results. When using <code>createFilteredQuery</code>, you must apply it consistently across all MultiList and the ResultList component in your JSON configuration to ensure proper filtering functionality.', 'oersi-domain'),
],
    'hierarchical_filter' => [
        'label' => TRANSLATION_LABEL_OBJECT . ' ' . TRANSLATION_LABEL_OPTIONAL,
        'description' => __('Give the URL for where to find the hierarchical_filter. See subject as an example.', 'oersi-domain'),
    ],
    'loader' => [
        'label' => TRANSLATION_LABEL_STRING . ' ' . TRANSLATION_LABEL_OPTIONAL,
        'description' => __('To display an optional loader while fetching the options.', 'oersi-domain'),
    ],
    'missingLabel' => [
        'label' => TRANSLATION_LABEL_STRING . ' ' . TRANSLATION_LABEL_OPTIONAL,
        'description' => __('Defaults to N/A. Specify a custom label to show when showMissing is set to true.', 'oersi-domain'),
    ],
    'reloadAggregationsOnSearch' => [
        'label' => TRANSLATION_LABEL_BOOLEAN,
        'description' => __('Use to make a request to the server when you search in the search input. Defaults to false which deactivates the request.', 'oersi-domain'),
    ],
    'selectAllLabel' => [
        'label' => TRANSLATION_LABEL_STRING . ' ' . TRANSLATION_LABEL_OPTIONAL,
        'description' => __('Add an extra Select all item to the list with the provided label string.', 'oersi-domain'),
    ],
    'showCount' => [
        'label' => TRANSLATION_LABEL_BOOLEAN . ' ' . TRANSLATION_LABEL_OPTIONAL,
        'description' => __('Show a count of the number of occurrences beside each list item. Defaults to true.', 'oersi-domain'),
    ],
    'showFilter' => [
        'label' => TRANSLATION_LABEL_BOOLEAN . ' ' . TRANSLATION_LABEL_OPTIONAL,
        'description' => __('Show as filter when a value is selected in a global selected filters view. Defaults to true.', 'oersi-domain'),
    ],
    'showLoadMore' => [
        'label' => TRANSLATION_LABEL_BOOLEAN . ' ' . TRANSLATION_LABEL_OPTIONAL,
        'description' => __('Defaults to false and works only with Elasticsearch >= 6 since it uses composite aggregations. This adds a “Load More” button to load the aggregations on demand combined with the size prop. Composite aggregations are in beta and this is an experimental API which might change in a future release. Note: Composite aggregations do not support sorting by count. Hence with showLoadMore, you can only sort by: asc or desc order. The sortBy prop defaults to asc when showLoadMore prop is used.', 'oersi-domain'),
    ],
    'size' => [
        'label' => TRANSLATION_LABEL_NUMBER . ' ' . TRANSLATION_LABEL_OPTIONAL,
        'description' => __('Number of list items to be displayed. Defaults to showing 100 items. Max value for this prop can be 1000.', 'oersi-domain'),
    ],
    'title' => [
        'label' => TRANSLATION_LABEL_STRING . ' ' . TRANSLATION_LABEL_OPTIONAL,
        'description' => __('Title of the component to be shown in the UI. Defaults to no title being shown. Can be Text or translation.RESOURCETYPE. If it starts with "translation.", it will be translated; otherwise, it will be displayed as a string.', 'oersi-domain'),
    ],
];

?>

<header>
    <h2><?php esc_html_e('MultiList Components', 'oersi-domain'); ?></h2>
</header>
<p>
    <a href="https://opensource.appbase.io/reactive-manual/list-components/multilist.html" target="_blank" rel="noopener">
        <?php esc_html_e('MultiList', 'oersi-domain'); ?>
    </a>
    <?php esc_html_e(' is useful for displaying a list of values where multiple items can be selected at a time, and the values are retrieved by a database query on the field specified in the dataField prop.', 'oersi-domain'); ?>
</p>

<?php
foreach ($translations as $translationKey => $translationValue) {
    echo MultiListRenderer::renderDetails($translationKey, $translationValue['label'], $translationValue['description']);
}
?>

<h3 style="font-size: 1rem;"><?php esc_html_e('Example:', 'oersi-domain'); ?></h3>
<details>
    <summary id="defaultQuery2"><h3><?php esc_html_e('MultiList Component Props', 'oersi-domain'); ?></h3></summary>
    <pre>
        <code>
        "multilist": [{
            "componentId": "license",
            "dataField": "license.id",
            "title": "translation.LICENSE",
            "placeholder": "translation.LICENSE",
            "filterLabel": "translation.LICENSE",
            "showMissing": true,
            "missingLabel": "N/A",
            "showFilter": true,
            "showSearch": false,
            "defaultQuery": "getPrefixAggregationQueryLicense",
            "customQuery": "customQueryLicense",
            "className": "license-card",
            "fontAwesome": "",
            "URLParams": true,
            "hierarchical_filter": {
                "schemaPath": "/locales/vocabularies_parents"
            },
            "and": ["author", "search", "provider", "results", "learningResourceType", "language", "about", "sourceOrganization"]
        }]
        </code>
    </pre>
</details>


<h3 style="font-size: 1rem;"><?php esc_html_e('Example:', 'oersi-domain'); ?></h3>
<details>
    <summary id="defaultQuery3">
        <h3><?php esc_html_e('Example showing how to restrict search results using defaultQuery conditions.', 'oersi-domain'); ?></h3>
    </summary>
    <pre>
        <code>
        "multilist": [{
            "componentId": "license",
            "dataField": "license.id",
            "title": "translation.LICENSE",
            "placeholder": "translation.LICENSE",
            "filterLabel": "translation.LICENSE",
            "showMissing": true,
            "missingLabel": "N/A",
            "showFilter": true,
            "showSearch": false,
            "defaultQuery": "createFilteredQuery({field: 'institutions.name', value: 'Universität Paderborn'}, {field: 'inLanguage', value: 'de'})",
            "customQuery": "customQueryLicense",
            "className": "license-card",
            "fontAwesome": "",
            "URLParams": true,
            "hierarchical_filter": {
                "schemaPath": "/locales/vocabularies_parents"
            },
            "and": ["author", "search", "provider", "results", "learningResourceType", "language", "about", "sourceOrganization"]
        }]
        </code>
    </pre>
</details>
