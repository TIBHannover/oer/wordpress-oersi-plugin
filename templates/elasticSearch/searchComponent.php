<?php
if (!defined('ABSPATH')) {
    exit;
}

/* ElasticSearch SearchComponent Components */

require_once dirname(__FILE__) . '/../constants.php';

class SearchComponentRenderer {
    public static function renderDetails($title, $code, $description) {
        ob_start();
        ?>
        <details>
            <summary><h3><?php esc_html_e($title, 'oersi-domain'); ?></h3></summary>
            <div class="details-content">
                <code><?php echo esc_html($code); ?></code>
                <p><?php echo wp_kses_post(__($description, 'oersi-domain')); ?></p>
            </div>
        </details>
        <?php
        return ob_get_clean();
    }
}

?>

<header>
    <h2><?php esc_html_e('SearchComponent Components', 'oersi-domain'); ?></h2>
</header>
<p>
    <a href="https://opensource.appbase.io/reactive-manual/search-components/datasearch.html" target="_blank" rel="noopener">
        <?php esc_html_e('SearchComponent', 'oersi-domain'); ?>
    </a>
    <?php esc_html_e(' is useful for displaying a list of values where multiple items can be selected at a time, and the values are retrieved by a database query based on the field specified in the dataField prop.', 'oersi-domain'); ?>
</p>

<?php
$translations = [
    'autosuggest' => [
        'label' => TRANSLATION_LABEL_BOOLEAN,
        'description' => __('Set whether the autosuggest functionality should be enabled or disabled. Defaults to false.<br><br><strong>IMPORTANT NOTE:</strong><br>The autosuggest feature is only fully functional when using an unrestricted default query. If the defaultQuery is limited using query, must, or match parameters, the autosuggest functionality will not work as expected.', 'oersi-domain'),
    ],
    'componentId' => [
        'label' => TRANSLATION_LABEL_STRING,
        'description' => __('The unique identifier of the component, which can be referenced in other components’ props.', 'oersi-domain'),
    ],
    'customHighlight' => [
        'label' => TRANSLATION_LABEL_OBJECT,
        'description' => __('When highlighting is enabled, this prop allows specifying the fields which should be returned with the matching highlights.', 'oersi-domain'),
    ],
    'dataField' => [
        'label' => TRANSLATION_LABEL_STRING . ' or ' . TRANSLATION_LABEL_ARRAY,
        'description' => __('Database field(s) to be connected to the component’s UI view. DataSearch accepts an Array in addition to String.', 'oersi-domain'),
    ],
    'debounce' => [
        'label' => TRANSLATION_LABEL_NUMBER,
        'description' => __('Sets the milliseconds to wait before executing the query. Defaults to 0, i.e., no debounce.', 'oersi-domain'),
    ],
    'defaultSuggestions' => [
        'label' => TRANSLATION_LABEL_ARRAY,
        'description' => __('Preset search suggestions to be shown on focus when the search box does not have any search query text set.', 'oersi-domain'),
    ],
    'executeSearch' => [
        'label' => TRANSLATION_LABEL_BOOLEAN,
        'description' => __('Controls when the search is executed. When set to true, search happens automatically as you type. When false, search only happens when pressing Enter or clicking the search button. Defaults to false.', 'oersi-domain'),
    ],
    'fuzziness' => [
        'label' => TRANSLATION_LABEL_STRING . ' or ' . TRANSLATION_LABEL_NUMBER,
        'description' => __('Sets a maximum edit distance on the search parameters, can be 0, 1, 2 or “AUTO”.', 'oersi-domain'),
    ],
    'fieldWeights' => [
        'label' => TRANSLATION_LABEL_ARRAY,
        'description' => __('Set the search weight for the database fields.', 'oersi-domain'),
    ],
    'highlight' => [
        'label' => TRANSLATION_LABEL_BOOLEAN,
        'description' => __('Whether highlighting should be enabled in the returned results.', 'oersi-domain'),
    ],
    'iconPosition' => [
        'label' => TRANSLATION_LABEL_STRING,
        'description' => __('Sets the position of the search icon. Can be left or right.', 'oersi-domain'),
    ],
    'placeholder' => [
        'label' => TRANSLATION_LABEL_STRING,
        'description' => __('Set the placeholder text to be displayed in the search box input field. Defaults to “Search”.', 'oersi-domain'),
    ],
    'queryFormat' => [
        'label' => TRANSLATION_LABEL_STRING,
        'description' => __('Sets the query format, can be or or and. Defaults to or.', 'oersi-domain'),
    ],
    'searchOperators' => [
        'label' => TRANSLATION_LABEL_BOOLEAN,
        'description' => __('If set to true, then you can use special characters in the search query to enable the advanced search.', 'oersi-domain'),
    ],
    'showButton' => [
        'label' => TRANSLATION_LABEL_BOOLEAN,
        'description' => __('Show or hide the search button. The default value is false.', 'oersi-domain'),
    ],
    'showClear' => [
        'label' => TRANSLATION_LABEL_BOOLEAN,
        'description' => __('Show a clear text icon. Defaults to true.', 'oersi-domain'),
    ],
    'showFilter' => [
        'label' => TRANSLATION_LABEL_BOOLEAN,
        'description' => __('Show as filter when a value is selected in a global selected filters view. Defaults to true.', 'oersi-domain'),
    ],
    'showIcon' => [
        'label' => TRANSLATION_LABEL_BOOLEAN,
        'description' => __('Whether to display a search or custom icon in the input box. Defaults to true.', 'oersi-domain'),
    ],
    'strictSelection' => [
        'label' => TRANSLATION_LABEL_BOOLEAN,
        'description' => __('When set to true, the component will only set its value if the value was selected from the suggestion.', 'oersi-domain'),
    ],
    'title' => [
        'label' => TRANSLATION_LABEL_STRING,
        'description' => __('Set the title of the component to be displayed in the UI.', 'oersi-domain'),
    ],
];

if (!empty($translations)) {
    foreach ($translations as $translationKey => $translationValue) {
        echo SearchComponentRenderer::renderDetails($translationKey, $translationValue['label'], $translationValue['description']);
    }
}
?>

<h3 style="font-size: 1rem;"><?php esc_html_e('Example:', 'oersi-domain'); ?></h3>
<details>
    <summary><h3><?php esc_html_e('SearchComponent Component Props', 'oersi-domain'); ?></h3></summary>
    <pre>
        <code>
        "searchComponent": {
            "showIcon": true,
            "showButton": true,
            "showClear": true,
            "translateText": "Search",
            "componentId": "search",
            "queryFormat": "and",
            "filterLabel": "translation.SEARCH_LABEL",
            "dataField": ["name", "creator.name", "description", "keywords"],
            "fieldWeights": [1, 3],
            "fuzziness": 0,
            "debounce": 100,
            "autosuggest": false,
            "highlight": true,
            "iconPosition": "right",
            "placeholder": "translation.SEARCH_PLACEHOLDER",
            "showFilter": true,
            "URLParams": true,
            "customHighlight": {
                "pre_tags": ["&lt;mark&gt;"],
                "post_tags": ["&lt;/mark&gt;"],
                "fields": {
                    "name": {},
                    "description": {}
                },
                "number_of_fragments": 0
            },
            "and": ["author", "results"]
        }
        </code>
    </pre>
</details>
