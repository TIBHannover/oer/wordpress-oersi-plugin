<?php
if (!defined('ABSPATH')) {
    exit;
}

/* Short script for <details>-tags */
echo '<script>
function openDetails(id) {
    document.querySelector(id).parentElement.open = true;
    return false;
}
</script>';

/* ElasticSearch ResultList Components */

require_once dirname(__FILE__) . '/../constants.php';

class ResultListRenderer {
    public static function renderDetails($title, $code, $description) {
        ob_start();
        ?>
        <details>
            <summary><h3><?php esc_html_e($title, 'oersi-domain'); ?></h3></summary>
            <div class="details-content">
                <code><?php echo esc_html($code); ?></code>
                <p><?php echo wp_kses_post(__($description, 'oersi-domain')); ?></p>
            </div>
        </details>
        <?php
        return ob_get_clean();
    }
}

// Übersetzungen definieren
$translations = [
    'componentId' => [
        'label' => TRANSLATION_LABEL_STRING,
        'description' => __('Unique identifier of the component, can be referenced in other components’ react prop.', 'oersi-domain'),
    ],
    'dataField' => [
        'label' => TRANSLATION_LABEL_STRING,
        'description' => __('Data field to be connected to the component’s UI view. It is useful for providing a sorting context.', 'oersi-domain'),
    ],
    'defaultQuery' => [
        'label' => TRANSLATION_LABEL_FUNCTION . ' ' . TRANSLATION_LABEL_OPTIONAL,
        'description' => __('The <strong>defaultQuery</strong> contains by default the parameter <code>track_total_hits: true,</code>&nbsp;which ensures that Elasticsearch returns the exact total number of matches for a search query. This is important for accurate result statistics and pagination. <br><br>The defaultQuery can also be used to predefine search restrictions. You can add the query with your specific conditions into the defaultQuery section. Multiple restrictions can be defined using key/value pairs in the must array. <a href="#defaultQuery1" onclick="document.querySelector(\'#defaultQuery1\').parentElement.open = true">The example below shows restrictions for <code>institutions.name</code> and <code>inLanguage</code></a>, but you can use any available fields to restrict your search results.', 'oersi-domain'),
    ],
    'defaultSortOption' => [
        'label' => TRANSLATION_LABEL_STRING . ' ' . TRANSLATION_LABEL_OPTIONAL,
        'description' => __('Accepts the <b>dataField</b> of the desired sort option to set the default sort value from the given <code>sortOptions</code> array.', 'oersi-domain'),
    ],
    'excludeFields' => [
        'label' => TRANSLATION_LABEL_ARRAY . ' ' . TRANSLATION_LABEL_OPTIONAL,
        'description' => __('Fields to be excluded in search results.', 'oersi-domain'),
    ],
    'includeFields' => [
        'label' => TRANSLATION_LABEL_ARRAY . ' ' . TRANSLATION_LABEL_OPTIONAL,
        'description' => __('Fields to be included in search results.', 'oersi-domain'),
    ],
    'pages' => [
        'label' => TRANSLATION_LABEL_NUMBER . ' ' . TRANSLATION_LABEL_OPTIONAL,
        'description' => __('Number of user-selectable pages to be displayed when pagination is enabled. Defaults to 5.', 'oersi-domain'),
    ],
    'pagination' => [
        'label' => TRANSLATION_LABEL_BOOLEAN . ' ' . TRANSLATION_LABEL_OPTIONAL,
        'description' => __('Defaults to <code>false</code>. When set to <code>true</code>, a pagination-based list view with page numbers will appear.', 'oersi-domain'),
    ],
    'paginationAt' => [
        'label' => TRANSLATION_LABEL_STRING . ' ' . TRANSLATION_LABEL_OPTIONAL,
        'description' => __('Determines the position where to show the pagination, only applicable when <strong>pagination</strong> prop is set to <code>true</code>. Accepts one of <code>top</code>, <code>bottom</code> or <code>both</code> as valid values. Defaults to <code>bottom</code>.', 'oersi-domain'),
    ],
    'react' => [
        'label' => TRANSLATION_LABEL_OBJECT . ' ' . TRANSLATION_LABEL_OPTIONAL,
        'description' => __('A dependency object defining how this component should react based on the state changes in the sensor components.', 'oersi-domain'),
    ],
    'scrollOnChange' => [
        'label' => TRANSLATION_LABEL_BOOLEAN . ' ' . TRANSLATION_LABEL_OPTIONAL,
        'description' => __('Enables you to customize the window scrolling experience on query change. Defaults to <code>true</code>. The window will scroll to top in case of the query change.', 'oersi-domain'),
    ],
    'showLoader' => [
        'label' => TRANSLATION_LABEL_BOOLEAN . ' ' . TRANSLATION_LABEL_OPTIONAL,
        'description' => __('Defaults to <code>true</code>, if set to <code>false</code>, the ReactiveList will not display the default loader.', 'oersi-domain'),
    ],
    'showResultStats' => [
        'label' => TRANSLATION_LABEL_BOOLEAN . ' ' . TRANSLATION_LABEL_OPTIONAL,
        'description' => __('Whether to show result stats in the form of results found and time taken. Defaults to <code>true</code>.', 'oersi-domain'),
    ],
    'showSortByOption' => [
        'label' => TRANSLATION_LABEL_BOOLEAN . ' ' . TRANSLATION_LABEL_OPTIONAL,
        'description' => __('Show or hide the sortBy property from the <code>sortOptions</code> array. Defaults to true, which will show the sortBy option; otherwise, it will hide it.', 'oersi-domain'),
    ],
    'size' => [
        'label' => TRANSLATION_LABEL_NUMBER . ' ' . TRANSLATION_LABEL_OPTIONAL,
        'description' => __('Number of results to show per view. Defaults to 10.', 'oersi-domain'),
    ],
    'sortBy' => [
        'label' => TRANSLATION_LABEL_STRING . ' ' . TRANSLATION_LABEL_OPTIONAL,
        'description' => __('Sort the results by either <code>asc</code> or <code>desc</code> order. It is an alternative to <code>sortOptions</code>, both can’t be used together.', 'oersi-domain'),
    ],
    'sortOptions' => [
        'label' => TRANSLATION_LABEL_OBJECT . ' ' . TRANSLATION_LABEL_ARRAY . ' ' . TRANSLATION_LABEL_OPTIONAL,
        'description' => __('An alternative to the <code>sortBy</code> prop, <code>sortOptions</code> creates a sorting view in the ReactiveList component’s UI. Each array element is an object that takes three keys: <code>label</code>, <code>dataField</code>, and <code>sortBy</code>.', 'oersi-domain'),
    ],
    'stream' => [
        'label' => TRANSLATION_LABEL_BOOLEAN . ' ' . TRANSLATION_LABEL_OPTIONAL,
        'description' => __('Whether to stream new result updates in the UI. Defaults to <code>false</code>.', 'oersi-domain'),
    ],
    'URLParams' => [
        'label' => TRANSLATION_LABEL_BOOLEAN . ' ' . TRANSLATION_LABEL_OPTIONAL,
        'description' => __('When set adds the current page number to the URL. Only works when <code>pagination</code> is enabled.', 'oersi-domain'),
    ],
];

// Rendern der Details
if (!empty($translations)) {
    foreach ($translations as $translationKey => $translationValue) {
        echo ResultListRenderer::renderDetails($translationKey, $translationValue['label'], $translationValue['description']);
    }
}
?>

<h3 style="font-size: 1rem;"><?php esc_html_e('Example:', 'oersi-domain'); ?></h3>
<details>
    <summary><h3><?php esc_html_e('ReactiveList Component Props', 'oersi-domain'); ?></h3></summary>
    <pre>
        <code>
        {
            "resultList": {
                "componentId": "results",
                "dataField": "name",
                "pagination": true,
                "stream": false,
                "showLoader": false,
                "renderError": true,
                "showResultStats": true,
                "paginationAt": "bottom",
                "sortBy": null,
                "pages": 5,
                "size": 12,
                "URLParams": true,
                "defaultQuery": {
                    "track_total_hits": true
                },
                "sortOptions": [{
                    "label": "translation.SORT_OPTION_LABEL_MODIFIED",
                    "dataField": "mainEntityOfPage.dateModified",
                    "sortBy": "desc"
                }, {
                    "label": "translation.SORT_OPTION_LABEL_MODIFIED",
                    "dataField": "mainEntityOfPage.dateModified",
                    "sortBy": "asc"
                }],
                "defaultSortOption": "mainEntityOfPage.dateModified",
                "showSortByOption": true,
                "showEndPage": true,
                "and": ["about"]
            }
        }
        </code>
    </pre>
</details>

<h3 style="font-size: 1rem;"><?php esc_html_e('Example:', 'oersi-domain'); ?></h3>
<details>
    <summary id="defaultQuery1">
        <h3><?php esc_html_e('Example showing how to restrict search results using defaultQuery conditions.', 'oersi-domain'); ?></h3>
    </summary>
    <pre>
        <code>
        {
            "resultList": {
                "componentId": "results",
                "dataField": "name",
                "pagination": true,
                "stream": false,
                "showLoader": false,
                "renderError": true,
                "showResultStats": true,
                "paginationAt": "bottom",
                "sortBy": null,
                "pages": 5,
                "size": 12,
                "URLParams": true,
                "defaultQuery": {
                    "track_total_hits": true,
                    "query": {
                        "bool": {
                            "must": [{
                                "match": {
                                    "institutions.name": "Universität Paderborn"
                                }
                            }, {
                                "match": {
                                    "inLanguage": "de"
                                }
                            }]
                        }
                    }
                },
                "sortOptions": [{
                    "label": "translation.SORT_OPTION_LABEL_MODIFIED",
                    "dataField": "mainEntityOfPage.dateModified",
                    "sortBy": "desc"
                }, {
                    "label": "translation.SORT_OPTION_LABEL_MODIFIED",
                    "dataField": "mainEntityOfPage.dateModified",
                    "sortBy": "asc"
                }],
                "defaultSortOption": "mainEntityOfPage.dateModified",
                "showSortByOption": true,
                "showEndPage": true,
                "and": ["about"]
            }
        }
        </code>
    </pre>
</details>
