<div class="wrap oersi-wrap">
    <h1 class="screen-reader-text"><?php echo esc_html($_ENV['OERSI_PLUGIN_NAME']); ?></h1>
    <?php
    use Inc\Pages\ElasticsSearch\ElasticsSearchConfigCallbacks;
    use Inc\Base\CustomSection;
    use Inc\DefaultData\ElasticsSearch;
    settings_errors();
    ?>

    <ul class="nav nav-tabs">
        <li class="active">
            <a href="#tab-1">
                <span class="tab-icon">
                    <img src="<?php echo esc_url(plugins_url('../../assets/images/cog-8-tooth.svg', __FILE__)); ?>" alt="" width="24"/>
                </span>
                <?php esc_html_e('Elasticsearch config', 'oersi-domain'); ?>
            </a>
        </li>
        <li>
            <a href="#tab-2">
                <span class="tab-icon">
                    <img src="<?php echo esc_url(plugins_url('../../assets/images/document-text.svg', __FILE__)); ?>" alt="" width="24"/>
                </span>
                <?php esc_html_e('Documentation', 'oersi-domain'); ?>
            </a>
        </li>
    </ul>

    <div class="tab-content">
        <div id="tab-1" class="tab-pane active oersi-ui-tab-pane">
            <form method="post" action="options.php">
                <?php
                settings_fields(ElasticsSearchConfigCallbacks::$optionGroup);
                CustomSection::do_settings_sections(ElasticsSearchConfigCallbacks::$optionName);
                ?>
                <div id="editor" data-editor="elastics_search" class="oersi-ui-elastics_search_fields"></div>
                <?php submit_button(null, 'button-primary button-hero'); ?>
            </form>
            <script>
                window.addEventListener("load", function() {
                    const elements_json = document.querySelector("[data-editor='elastics_search']");
                    // get text area element by hidden attribute
                    const textarea_json = document.getElementById("elastics_search");
                    // get value
                    var value = textarea_json.value;
                    // debugger;
                    const jsEditor = window.codeEditor
                        .setElement(elements_json)
                        .setValue(value !== "" ? codeEditor.formatJson(value) : "{}")
                        .setOption({
                            autoScrollEditorIntoView: true,
                            maxLines: 0,
                            minLines: 30,

                        }).show().getEditor();

                    jsEditor.getSession().on('change', function(e) {
                        // disable submit button if not valid json
                        if (codeEditor.isJson(jsEditor.getValue())) {
                            document.querySelector("[type='submit']").disabled = false;
                            textarea_json.value = JSON.stringify(JSON.parse(jsEditor.getValue()));
                        } else {
                            document.querySelector("[type='submit']").disabled = true;
                        }
                    });
                });
            </script>
        </div>
        <div id="tab-2" class="tab-pane">
            <main id="oersi-doc-main-doc">
                <nav id="oersi-doc-navbar">
                    <ul>
                        <li>
                            <a href="#Information" class="nav-link">
                                <?php esc_html_e('Information', 'oersi-domain'); ?>
                            </a>
                        </li>
                        <li>
                            <a href="#resultList" class="nav-link">
                                <?php esc_html_e('ResultList Components', 'oersi-domain'); ?>
                            </a>
                        </li>
                        <li>
                            <a href="#searchComponent" class="nav-link">
                                <?php esc_html_e('SearchComponent Components', 'oersi-domain'); ?>
                            </a>
                        </li>
                        <li>
                            <a href="#multiList" class="nav-link">
                                <?php esc_html_e('MultiList Components', 'oersi-domain'); ?>
                            </a>
                        </li>
                        <li>
                            <a href="#OERSI-Example" class="nav-link">
                                <?php esc_html_e('Configuration-Example', 'oersi-domain'); ?>
                            </a>
                        </li>
                    </ul>
                </nav>
                <div class="container">
                    <section class="main-section mb-12" id="Information">
                        <header>
                            <h2><?php esc_html_e('Information', 'oersi-domain'); ?></h2>
                        </header>
                        <p>
                            <?php echo wp_kses( __('<a href="https://opensource.appbase.io/reactive-manual/getting-started/componentsindex.html" target="_blank">ReactiveSearch</a> provides React UI components for Elasticsearch. This document explains the different types offered by the library and provides scenarios for when to use each.', 'oersi-domain'), array('a' => array('href' => array(), 'target' => array()))); ?>
                        </p>
                        <p>
                        <?php echo wp_kses( __('Not all the ReactiveSearch components are available in OERSI. Some are not accessible, so feel free to use those that are available. If you want to utilize components that are not included in OERSI, you are welcome to contribute to the library. You can find the <b>OERSI Plugin</b> and its documentation ', 'oersi-domain') . '<a href="' . esc_url($_ENV['OERSI_PLUGIN_REPOSITORY_URL']) . '" target="_blank" rel="noopener noreferrer">' . esc_html__('here', 'oersi-domain') . '</a>.', array('b' => array(), 'a' => array('href' => array(), 'target' => array(), 'rel' => array())));?>
                        </p>
                        <h3>
                            <?php esc_html_e('In OERSI, we include some of the following components:', 'oersi-domain'); ?>
                        </h3>
                        <p>
                            <?php esc_html_e('Every component has a link to the official documentation, where you can find all the information about it and its properties.', 'oersi-domain'); ?>
                        </p>
                        <ul>
                            <li>
                                <a href="#resultList"><?php esc_html_e('ReactiveList', 'oersi-domain'); ?></a>
                                <?php esc_html_e(' represents various list-related components that typically create term queries.', 'oersi-domain'); ?>
                            </li>
                            <li>
                                <a href="#searchComponent"><?php esc_html_e('SearchComponent', 'oersi-domain'); ?></a>
                                <?php esc_html_e(' is designed for numbers and dates, typically creating range-based queries.', 'oersi-domain'); ?>
                            </li>
                            <li>
                                <a href="#multiList"><?php esc_html_e('MultiList', 'oersi-domain'); ?></a>
                                <?php esc_html_e(' represents search bar components that typically perform searches on full-text data.', 'oersi-domain'); ?>
                            </li>
                        </ul>
                    </section>
                    <?php # resultList component ?>
                    <section class="main-section mt-20 mb-20" id="resultList">
                       <?php  include_once plugin_dir_path(__FILE__) .  'resultListComponent.php'; ?>
                    </section>
                    <hr />
                    <?php # searchComponent component ?>
                    <section class="main-section mb-20" id="searchComponent">
                        <?php  include_once plugin_dir_path(__FILE__) .  'searchComponent.php'; ?>
                    </section>
                    <hr />
                    <?php # searchComponent component ?>
                    <section class="main-section mb-20" id="multiList">
                        <?php  include_once plugin_dir_path(__FILE__) .  'multiListComponent.php'; ?>
                    </section>
                    <hr />
                    <section class="main-section" id="OERSI-Example">
                        <header>
                            <h2><?php esc_html_e('Configuration-Example', 'oersi-domain'); ?></h2>
                        </header>

                        <p>
                            <?php echo wp_kses(__('The following is an example of configuration for the plugin. For more information about the URL, App name, and Credentials, please refer to the <a href="https://docs.appbase.io/docs/reactivesearch/v3/overview/reactivebase/#props" target="_blank">ReactiveBase</a>.', 'oersi-domain'), array('a' => array('href' => array(), 'target' => array()))); ?>
                        </p>
                        <ol class="mt-12">
                            <li>
                                <div class="heading">
                                    <h3><?php esc_html_e('URL', 'oersi-domain'); ?></h3>
                                    <i>(string)</i>
                                </div>
                                <p style="max-width:45ch;">
                                    <?php esc_html_e('A field that contains the URL of the Elasticsearch instance, for example', 'oersi-domain'); ?> <code>http://localhost:9200</code>.
                                </p>
                            </li>
                            <li>
                                <div class="heading">
                                    <h3><?php esc_html_e('App Name', 'oersi-domain'); ?></h3>
                                    <i>(string)</i>
                                </div>
                                <p>
                                    <?php esc_html_e('Refers to an index if you\'re using your own Elasticsearch cluster, for example', 'oersi-domain'); ?> <code>oersi_internal</code>. <?php esc_html_e('Multiple indexes can be connected to by specifying comma-separated index names.', 'oersi-domain'); ?>
                                </p>
                            </li>
                            <li>
                                <div class="heading">
                                    <h3><?php esc_html_e('Credentials', 'oersi-domain'); ?></h3>
                                    <i>(string)</i>
                                </div>
                                <i>(<?php esc_html_e('optional', 'oersi-domain'); ?>)</i>
                                <p>
                                    <?php esc_html_e('App credentials as they appear on the dashboard. It should be a string of the format "username:password" and is used for authenticating the app.', 'oersi-domain'); ?> <?php esc_html_e('If you are not using an appbase.io app, credentials may not be necessary, although having an open-access Elasticsearch cluster is not recommended.', 'oersi-domain'); ?>
                                </p>
                            </li>
                            <li>
                                <div class="heading">
                                    <h3><?php esc_html_e('Provider Name', 'oersi-domain'); ?></h3>
                                    <i>(string)</i>
                                </div>
                                <i>(<?php esc_html_e('optional', 'oersi-domain'); ?>)</i>
                                <p>
                                    <?php esc_html_e('Accepts a string. When set, it activates provider links. This means that for the specified provider, all links will lead to the provider instead of their actual destinations.', 'oersi-domain'); ?>
                                </p>
                            </li>
                            <li>
                                <div class="heading">
                                    <h3><?php esc_html_e('Provider Image Link', 'oersi-domain'); ?></h3>
                                    <i>(string)</i>
                                </div>
                                <i>(<?php esc_html_e('optional', 'oersi-domain'); ?>)</i>
                                <p>
                                    <?php esc_html_e('Accepts an image link (URL). If set, the logo on the cards will be changed from the Oersi logo to the provider logo, but only if the', 'oersi-domain'); ?> <b><?php esc_html_e('Provider Name', 'oersi-domain'); ?></b> <?php esc_html_e('is also set.', 'oersi-domain'); ?>
                                </p>
                            </li>
                        </ol>

                        <div>
                            <button class="button button-primary button-hero" data-target="my-dialog" data-model-trigger id="openAndCopyButton" aria-label="<?php echo esc_attr(__('Open settings modal and copy default configuration to clipboard.', 'oersi-domain')); ?>">
                                <?php esc_html_e('Copy the default configuration', 'oersi-domain'); ?>
                            </button>
                            <dialog class="oersi-modal-dialog" data-name="my-dialog">
                                <header>
                                    <div class="info">
                                        <p class="lucky">
                                            <?php esc_html_e('The text has been copied to the clipboard.', 'oersi-domain'); ?>
                                        </p>
                                        <div class="dashicons dashicons-yes"></div>
                                    </div>
                                    <button class="btn-oersi btn-oersi-icon" data-target="my-dialog" data-model-trigger>
                                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                                            <path stroke-linecap="round" stroke-linejoin="round" d="M10 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2m7-2a9 9 0 11-18 0 9 9 0 0118 0z" />
                                        </svg>
                                    </button>
                                </header>
                                <main>
                                    <pre>
                                        <code id="textToCopy">
                                            <?php echo json_encode(ElasticsSearch::$fields, JSON_PRETTY_PRINT); ?>
                                        </code>
                                    </pre>
                                </main>
                                <footer>
                                    <button class="btn-oersi btn-oersi-icon" data-target="my-dialog" data-model-trigger>
                                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                                            <path stroke-linecap="round" stroke-linejoin="round" d="M10 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2m7-2a9 9 0 11-18 0 9 9 0 0118 0z" />
                                        </svg>
                                    </button>
                                </footer>
                            </dialog>
                        </div>
                    </section>
                </div>
            </main>
        </div>
    </div>
</div>
