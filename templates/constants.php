<?php
if (!defined('ABSPATH')) {
    exit;
}
if (!defined('TRANSLATION_LABEL_STRING')) {
    define('TRANSLATION_LABEL_STRING', 'String');
}
if (!defined('TRANSLATION_LABEL_NUMBER')) {
    define('TRANSLATION_LABEL_NUMBER', 'Number');
}
if (!defined('TRANSLATION_LABEL_BOOLEAN')) {
    define('TRANSLATION_LABEL_BOOLEAN', 'Boolean');
}
if (!defined('TRANSLATION_LABEL_ARRAY')) {
    define('TRANSLATION_LABEL_ARRAY', 'Array');
}
if (!defined('TRANSLATION_LABEL_OBJECT')) {
    define('TRANSLATION_LABEL_OBJECT', 'Object');
}
if (!defined('TRANSLATION_LABEL_FUNCTION')) {
    define('TRANSLATION_LABEL_FUNCTION', 'Function');
}
if (!defined('TRANSLATION_LABEL_OPTIONAL')) {
    define('TRANSLATION_LABEL_OPTIONAL', '(optional)');
}
